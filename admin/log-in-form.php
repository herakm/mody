<?php
/*
  (C) 2012 Mario Herak - GUNSOFT
  Autor: Mario Herak
  www.gunsoft.sk, gunsoft@gunsoft.sk
*/

// Set a little bit salt for new passwords
$salt_start = "D0n7 m35S W17h mY p@S5w0rD5 F1l7Hy m07h3R H@ck3r5";
$salt_end = "Y0u w1lL N3vEr g0nN@ g37 m3";

$login_error = FALSE; // Help variable to identify log-in proccess

// User authentification
if (isset($_POST["log-in"]) && isset($_POST["nick"]) && $_POST["nick"]
    && isset($_POST["password"]) && $_POST["password"]) {

    $menoUziv = $_POST["nick"];
    $hesloUziv = $_POST["password"];


    // Kontrola uzivatelom vlozenych udajov:

    /*
     * Ak je v PHP zapnuta funkcia Magic Quotes
     *  ( vklada / (lomitko) za ' (jedn. uv.), " (dvoj. uv.), \ (spatne lomitko) a NULL znaky ),
     * odstrania sa / vlozene funkciou Magic Quotes:
     * */
    if (get_magic_quotes_gpc()) {
        $menoUziv = stripslashes($menoUziv);
        $hesloUziv = stripslashes($hesloUziv);
    }


    /*
     * Osetri specialne znaky v retazci pre pouzitie s SQL:
     *  prida spatne lomitka pred: \x00, \n, \r, \, ', " a \x1a
     * */
    $menoUziv =  mysql_real_escape_string($menoUziv);
    $hesloUziv = mysql_real_escape_string($hesloUziv);


    $pswd = $salt_start . " - " . $hesloUziv . " - " . $salt_end; // Set new password

    $new_pswd = md5($pswd); // Calculate the md5 hash of new password

    $sql = "SELECT `id`, `admin` FROM `users` WHERE `publish`='1' AND `nick`='" . $menoUziv . "' AND `password`='" . $new_pswd . "'";
    $r = mysql_query($sql);
    $users = mysql_fetch_array($r);

    if ($users) {
        $_SESSION["UserID"] = $users["id"]; // Set user's id
        if ($users["admin"] == 1) $_SESSION["Admin"] = "TRUE"; // Set user as admin if it's true
    }
    else $login_error = TRUE; // Help variable to set that log-in proccess wasn't good
}


// Display log-in form
if (!isset($_POST["log-in"]) || isset($_POST["log-in"]) && (!isset($_POST["nick"])
    || (isset($_POST["nick"]) && !$_POST["nick"]) || !isset($_POST["password"])
    || (isset($_POST["password"]) && !$_POST["password"])) || $login_error) {

    if ($login_error) echo "<h2 class='center attention'>Prihlásenie neúspešné!</h2>
<p class='center'>
Prihlasovacia prezývka alebo heslo neboli v databáze nájdené. Skúste to ešte raz.<br />
<span class='tip'>
TIP: Skontrolujte či nemáte na svojej klávesnici zapnuté písanie veľkých písmen (kláves Caps Lock).
</span>
</p>\n\n";

    echo "<h1 class='center'>Administrácia</h1>
<form id='log-in' method='post' action='index.php'>
<label for='nick'>Prezývka:</label>
<input id='nick' class='text' type='text' name='nick' size='25' maxlength='255' /><br />\n";
    if (isset($_POST["log-in"]) && (!isset($_POST["nick"]) || isset($_POST["nick"]) && !$_POST["nick"])) echo "<span class='warning'>Nevložili ste žiadnu prezývku!</span><br />\n\n";

    echo "<label for='password'>Heslo:</label>
<input id='password' class='text' type='password' name='password' size='25' maxlength='255' /><br />\n";
    if (isset($_POST["log-in"]) && (!isset($_POST["password"]) || isset($_POST["password"]) && !$_POST["password"])) echo "<span class='warning'>Nevložili ste žiadne heslo!</span><br />\n\n";

    echo "<input type='submit' value='Prihlásiť' name='log-in' />
</form>\n";
}
?>