<?php
/*
(C) 2009 - 2012 Mario Herak - GUNSOFT
Autor: Mario Herak
www.gunsoft.sk, gunsoft@gunsoft.sk
*/


// Inicializacia
if (file_exists("init.php")) include_once("init.php");
else exit("<h1 align='center'>File &quot;init.php&quot; is missing in root directory!</h1>");


// Nacita udaje z URL
if (isset($_GET['table']) && $_GET['table']) $tabulka = $_GET['table']; // nazov tabulky
if (isset($_GET['id']) && $_GET['id']) $rID = $_GET['id']; // id riadku

// Po suhlaseni v JavaScripte nacita ci sa ma vymazat vybrany riadok z tabulky
if (isset($_GET['delete']) && $_GET['delete']) $vymaz = $_GET['delete'];

if (isset($_GET['edit']) && $_GET['edit']) $oprav = $_GET['edit']; // ci sa ma opravit vybrany riadok z tabulky

if (isset($_GET['page']) && $_GET['page']) $strana = $_GET['page']; // strana vypisu tabulky z databazy
if (isset($_GET['sort_by']) && $_GET['sort_by']) $radit_podla = $_GET['sort_by']; // podla coho sa ma tabulka radit
if (isset($_GET['order']) && $_GET['order']) $poradie = $_GET['order']; // poradie radenia
if (isset($_GET['rows']) && $_GET['rows']) $kolko = $_GET['rows']; // kolko zaznamov sa ma zobrazit na stranke


// Nacita udaje z FORMULAROV
if (isset($_POST['hladat'])) $hladat = $_POST['hladat']; // Zisti, ci bolo stlacene tlacidlo "Zoradit / Hladat"
if (isset($_POST['riadok'])) $riadok = $_POST['riadok']; // riadky vybrane zaskrtnutim pomocou checkbutton

/*
MS IE pri formularovom prvku <input type='image'> nepracuje s hodnotou uvedenou vo value='' !!!
Miesto toho si pozmeni meno uvedene v name='' tak, ze mu prida _x a _y !!!
Do tychto novych mien po kliknuti na taketo tlacidlo odovzdava x-ovu, resp. y-ovu hodnotu kurzoru mysi !!!
*/
if (isset($_POST['VybavViac_x'])) $VybavViac = $_POST['VybavViac_x'];
if (isset($_POST['VymazViac_x'])) $VymazViac = $_POST['VymazViac_x'];
if (isset($_POST['OpravViac_x'])) $OpravViac = $_POST['OpravViac_x'];
if (isset($_POST['VytlacViac_x'])) $VytlacViac = $_POST['VytlacViac_x'];
  
if (isset($_POST['potvrdene'])) $potvrdene = $_POST['potvrdene']; // potvrdenie vymazania viacerych riadkov z tabulky
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<?php
// meta tag pre copyright
if ($settings['meta_copyright']) {
	echo "<meta name='copyright' content='", $settings['meta_copyright'], "'>\n";
}

// meta tag pre description
if ($settings['meta_desc']) {
	echo "<meta name='description' content='", $settings['meta_desc'], "'>\n";
}

// meta_keywords
if ($settings['meta_keywords']) {
	echo "<meta name='keywords' content='", $settings['meta_keywords'], "'>\n";
}

// favicon subor
if ($settings['favicon']) echo "\n<link href='", $settings['favicon'], "' rel='shortcut icon'>\n";
?>
		
		<link media="screen" type="text/css" rel="stylesheet" href="css/reset.css">
		<link media="screen" type="text/css" rel="stylesheet" href="css/screen.css">
		<link media="screen" type="text/css" rel="stylesheet" href="css/basic.css">
		<link media="screen" type="text/css" rel="stylesheet" href="css/jquery.lightbox-0.5.css">
		
		<!-- IE6 "fix" for the close png image -->
		<!--[if lt IE 7]>
		<link media="screen" type="text/css" rel="stylesheet" href="css/basic_ie.css">
		<![endif]-->
		
<?php
echo "<title>Administrácia - ";
if (isset($tabulka)) {
	echo "Zobrazenie riadkov tabuľky &quot;", $tabulka, "&quot;";
}
else {
	echo "CHYBA! - Nebol určený názov tabuľky";
}
echo " | ", $settings['site_title'], "</title>\n";
?>
	</head>
	
	
	<body>
        <div id="hlavny">
            <?php
            // Zobrazi uvodnu stranu administracie prihlasenym uzivatelom
            if (isset($_SESSION['UserID']) && isset($_SESSION['Admin']) && $_SESSION['Admin'] == "TRUE") {

    // Ak bola urcena tabulka, zobrazia sa jej riadky
    if (isset($tabulka) && $tabulka) {
      $ZobrazTabulku = TRUE;
      
      
      // Vymazanie riadku z tabulky
      if (isset($vymaz) && $vymaz) {

        $ZobrazTabulku = FALSE;
        
        if ($tabulka == "files") {

          $sql = "SELECT `file`, `thumb_name` FROM `files` WHERE `id`='" . $rID . "'";
          $r = mysql_query($sql);
          $files = mysql_fetch_array($r);
          
          $err1 = @unlink("../upload/" . $files['file']); // Zmaze uploadnuty subor
          
          // Ak sa nepodarilo zmazat subor, vypise sa vystraha
          if (!$err1) {
            echo "<h1 class='warning center'>Zmazanie súboru &quot;", $files['file'], "&quot; v adresáry &quot;upload&quot; nebolo úspešné!</h1>
<p class='center'>Prosím, kontaktujte administrátora. Ďakujem.</p>\n\n";
          }
          // Ak sa podarilo zmazat subor, vypise sa oznam
          else {
            echo "<h1 class='notice center'>Zmazanie súboru &quot;", $files['file'], "&quot; v adresáry &quot;upload&quot; bolo úspešné!</h1>\n";
          }
          
          // Ak existuje nahladovy obr.,
          if ($files['thumb_name']) {

            $err2 = @unlink("../upload/thumbs/" . $files['thumb_name']); // Zmaze nahladovy obrazok
            
            // Ak sa nepodarilo zmazat subor, vypise sa vystraha
            if (!$err2) {
              echo "<h1 class='warning center'>Zmazanie náhľadového súboru &quot;", $files['thumb_name'], "&quot; v adresáry &quot;upload/thumbs&quot; nebolo úspešné!</h1>
<p class='center'>Prosím, kontaktujte administrátora. Ďakujem.</p>\n\n";
            }
            // Ak sa podarilo zmazat subor, vypise sa oznam
            else {
              echo "<h1 class='notice center'>Zmazanie náhľadového súboru &quot;", $files['thumb_name'], "&quot; v adresáry &quot;upload/thumbs&quot; bolo úspešné!</h1>\n";
            }
            
          }
          
        }
        
        //echo "\$tabulka = " . $tabulka . "<br>\$err1 = " . $err1 . "<br>\$files['thumb_name'] = " . $files['thumb_name'] . "<br>\$err2 = " . $err2;
        
        /*
        Vymazanie vybraneho riadku z tabulky
          Ak tabulka nie je 'files',
          alebo ak sa vola 'files' a podarilo sa zmazat uploadnuty subor a neexistuje nahladovy obr.,
          alebo ak sa vola 'files' a podarilo sa zmazat uploadnuty subor a existuje nahladovy obr. a podarilo sa ho vymazat,
        */
        if ($tabulka != "files" || ($tabulka == "files" && isset ($err1) && $err1 == 1 && !isset ($files['thumb_name'])) ||
          ($tabulka == "files" && isset ($err1) && $err1 == 1 && isset ($files['thumb_name']) && isset ($err2) && $err2 == 1)) {
          
          $sql = "DELETE FROM `" . $tabulka . "` WHERE `id`='" . $rID . "'";
          $r = @mysql_query($sql); // Vymaze sa riadok z tabulky
          
          // Ak sa nepodarilo zmazat riadok, vypise sa vystraha
          if (!$r) {

            echo "<h1 class='warning center'>Zmazanie riadku &quot;", $rID, "&quot; z databázy nebolo úspešné!</h1>
<p class='center'>\n";

            if ($tabulka == "files" && isset ($err1) && $err1) {

              echo "Súbor &quot;", $files['file'], "&quot; z riadku bol zmazaný, ale nepodarilo sa zmazať riadok v databáze!<br>\n";
              unset($files);
              
            }
            
            echo "Prosím, kontaktujte administrátora. Ďakujem.
</p>\n\n";

          }
          // Ak sa podarilo vymazat riadok, vypise sa oznamenie
          else {
            echo "<h1 class='notice center'>Zmazanie riadku &quot;", $rID, "&quot; z databázy bolo úspešné!</h1>\n";
          }
          
        }
        
      }
      
      
      // Oprava riadku z tabulky
      if (isset($oprav)) {
        $IDecka = $rID;
        $riadok[$rID] = "on";
        $ZobrazTabulku = FALSE;
        
        // Nacita opravu vybraneho riadku z tabulky
        if (file_exists("edit.php")) require("edit.php");
        else exit("<h1 class='center'>V adresári &quot;admin&quot; chýba súbor &quot;edit.php&quot;!</h1>");
      }
      
      
      if ((isset($VymazViac) || isset($OpravViac)) && !isset($riadok)) {
        echo "<h1 class='upozornenie'>Nevybrali ste žiadny riadok!</h1>\n";
      }
      
      
      if ((isset($VymazViac) || isset($OpravViac)) && isset($riadok)) {
        $pocet = count($riadok);
        $IDecka = "";
        $z = 0;
        
        foreach($riadok as $kluc => $hodnota) {
          $IDecka .= $kluc;
          $z++;
          if ($z < $pocet) $IDecka .= ", ";
        }
      }
      
      
      // Vymazanie viacerych riadkov z tabulky
      if (isset($VymazViac) && isset($riadok)) {
        $ZobrazTabulku = FALSE;

        echo ("<h1 class='hlavicka'>Zmazanie označených riadkov z tabuľky &quot;". $tabulka . "&quot;</h1>\n");

        if (!isset ($potvrdene)) { //Ak uzivatel este nestlacil tlacidlo "Ano", zobrazi sa potvrdzovaci formular pre zmazanie oznacenych riadkov
          echo "<p style='text-align: center'>
  Naozaj si prajete vymazať riadky s &quot;id&quot;<br />
  <b>" . $IDecka . "</b><br />\n?
</p>
<table align='center' cellspacing='0' id='tabulka2'>
  <tr>
    <td>
      <form method='post' action='browse.php?table=", $tabulka, "'>\n";

          foreach ($riadok as $kluc => $hodnota) echo ("<input type='hidden' name='riadok[" . $kluc . "]' value='" . $hodnota . "' />\n");

          echo ("<input type='hidden' value='" . $VymazViac . "' name='VymazViac_x' />
        <input type='submit' value='Áno' name='potvrdene' />
      </form>
    </td>
    <td>
      <form method='post' action='zobraz.php?tabulka=" . $tabulka . "'>
        <input type='submit' value='Nie' />
      </form>
    </td>
  </tr>
</table>\n");
        }
        else { //Ak uzivatel stlacil tlacidlo "Ano", vymazu sa oznacene riadky
          foreach ($riadok as $kluc => $hodnota) {
            //Ak sa mazu riadky tabulky "files", musia sa fyzicky zmazat aj subory z riadkov
            if ($tabulka == "files") {
              $sql = "SELECT file FROM files WHERE id=" . $kluc;
              $r = mysql_query ($sql);
              $files = mysql_fetch_array ($r);
              $err = @unlink ("../upload/" . $files['file']);
              if (!$err) { //Ak sa nepodarilo zmazat subor, vypise sa vystraha a nezmaze sa z tabulky riadok
                echo ("<h1 class='vystraha'>Zmazanie súboru &quot;" . $files['file'] . "&quot; v adresáry &quot;upload&quot; nebolo úspešné!</h1>
<p align='center'>Prosím, kontaktujte administrátora. Ďakujem.</p>\n");
              }
              else { //Ak sa podarilo zmazat subor, zmaze sa aj riadok z tabulky
                echo ("<h1 class='oznamenie'>Zmazanie súboru &quot;" . $files['file'] . "&quot; v adresáry &quot;upload&quot; bolo úspešné!</h1>\n");
                unset ($files);
              }
            }

            if ($tabulka != "files" || ($tabulka == "files" && isset ($err) && $err)) {
              $sql = "DELETE FROM " . $tabulka . " WHERE id=" . $kluc;
              $r = @mysql_query ($sql);

              if (!$r) { //Ak sa nepodarilo zmazat riadok, vypise sa vystraha
                echo ("<h1 class='vystraha'>Zmazanie riadku &quot;" . $kluc . "&quot; z databázy nebolo úspešné!</h1>
<p align='center'>\n");
                if ($tabulka == "files" && isset ($err) && $err) {
                  echo ("Súbor &quot;" . $files['file'] . "&quot; z riadku bol zmazaný, ale nepodarilo sa zmazať riadok v databáze!<br />\n");
                  unset ($files);
                }
                echo ("Prosím, kontaktujte administrátora. Ďakujem.
</p>\n");
              }
              else { //Ak sa podarilo vymazat riadok, vypise sa oznamenie
                echo ("<h1 class='oznamenie'>Zmazanie riadku &quot;" . $kluc . "&quot; z databázy bolo úspešné!</h1>\n");
              }
            }
          }
        }
      }
      
      
      // Opravenie viacerych riadkov z tabulky
      if (isset($OpravViac) && isset($riadok)) {
        $ZobrazTabulku = FALSE;
        
        // Nacita opravu vybraneho riadku z tabulky
        if (file_exists("edit.php")) require ("edit.php");
        else exit("<h1 class='center'>V adresári &quot;admin&quot; chýba súbor &quot;edit.php&quot;!</h1>");
      }
      
      
      // Zobrazenie tabulky zaznamov databazy
      if (isset($ZobrazTabulku) && $ZobrazTabulku) {
        if (isset($_POST['kolko'])) $kolko = $_POST['kolko'];
        if (isset($_GET['rows'])) $kolko = $_GET['rows'];
        if (!isset($kolko) || isset($kolko) && !$kolko) $kolko = 20; // Nastavi kolko riadkov sa ma zobrazit, ak uzivatel nezadal inak
        
        if (isset($_POST['cohladat'])) $CoHladat = $_POST['cohladat'];
        if (isset($_GET['search'])) $CoHladat = $_GET['search'];
        if (!isset($CoHladat) || isset($CoHladat) && !$CoHladat) $CoHladat = FALSE;
        
        if (isset($_POST['kdehladat'])) $KdeHladat = $_POST['kdehladat'];
        if (isset($_GET['search_at'])) $KdeHladat = $_GET['search_at'];
        if (!isset($KdeHladat) || isset($KdeHladat) && !$KdeHladat) $KdeHladat = FALSE;
        
        // Ak sa nepozna zobrazovana strana, zobrazi sa prva strana
        if (!isset($strana)) {
          $odkial = 0;
          $strana = 1;
        }
        else {
          $odkial = $kolko * ($strana - 1);
        }
        
        echo "<h1 class='hlavicka'>Zobrazenie údajov tabuľky &quot;", $tabulka, "&quot;</h1>\n\n";
        
        // Nacita nazvy stlpcov v tabulke
        $sql = "SELECT * FROM `" . $tabulka . "`";
        if (isset($KdeHladat) && $KdeHladat && $KdeHladat != "nehladat" && isset($CoHladat) && $CoHladat) $sql .= " WHERE `" . $KdeHladat . "` LIKE '%" . $CoHladat . "%'"; // Ak je urcene KDE a CO hladat, upravi sa MySQL dotaz
        if (isset($radit_podla) && $radit_podla) $sql .= " ORDER BY `" . $radit_podla . "`"; // Ak je urcene podla coho sa ma radit, upravi sa MySQL dotaz
        if (isset($radit_podla) && $radit_podla && isset($poradie) && $poradie) $sql .= " " . $poradie; // Ak je urcene podla coho a ako (A-Z/Z-A) sa ma radit, upravi sa MySQL dotaz
        
        $r = mysql_query($sql);
        $pocet_riadkov = mysql_num_rows($r); // Zisti sa pocet riadkov v tabulke
        $PocStr = Ceil($pocet_riadkov / $kolko); // Vypocita sa pocet stran = pocet riadkov / pocet zaznamov na stranu
        
        if ($pocet_riadkov) {

          // Pomocne premenne
          $pocet_stlpcov = 0;
          $name = "";
          
          while($pocet_stlpcov < mysql_num_fields($r)) {
            $meta = mysql_fetch_field($r, $pocet_stlpcov);
            $name .= $meta->name . ",";
            $pocet_stlpcov++;
          }
          
          $mena_stlpcov = split(",", $name);
          
          echo "<form id='formular' method='post' action='browse.php?table=", $tabulka, "'>

<p id='vyhladavanie_v_tabulke'>
<label for='kolko'>Počet záznamov na stranu:</label>
<input id='kolko' type='text' name='kolko'";
          if (isset($kolko) && $kolko) echo " value='", $kolko, "'";
          echo "size='3' maxlength='11'><br>
<label for='cohladat'>Hľadať:</label>
<input id='cohladat' type='text' name='cohladat'";
          if (isset($CoHladat) && $CoHladat) echo " value='", $CoHladat, "'";
          echo " size='50' maxlength='255'>
<label for='kdehladat'>v</label>
<select id='kdehladat' name='kdehladat'>
<option value='nehladat'";
          if (!isset($KdeHladat)) echo " selected='selected'";
          echo ">-- Nehladať --</option>\n";
          for($i = 0; $i < $pocet_stlpcov; $i++) {
            echo "<option value='", $mena_stlpcov[$i], "'";
            if (isset($KdeHladat) && $KdeHladat == $mena_stlpcov[$i]) echo " selected='selected'";
            echo ">", $mena_stlpcov[$i], "</option>\n";
          }
          echo "</select>
<input type='submit' name='hladat' value='Zoradiť / Hľadať'>
</p>

<p class='strany'>
<strong>Strany:</strong>&nbsp;\n";
          // Vypisanie navigacie
          for($z = 0; $z < $PocStr; $z++) {
            if (($z + 1) != $strana) {
              echo "<a href='browse.php?table=", $tabulka, "&amp;page=", ($z + 1);
              if (isset($radit_podla) && $radit_podla) echo "&amp;sort_by=", $radit_podla;
              if (isset($poradie) && $poradie) echo "&amp;order=", $poradie;
              if (isset($CoHladat) && $CoHladat && isset($KdeHladat) && $KdeHladat && $KdeHladat != "nehladat") echo "&amp;search=", $CoHladat, "&amp;search_at=", $KdeHladat;
              echo "&amp;rows=", $kolko, "' title='Prejsť na stranu ", ($z + 1), "'>";
            }
            if (($z + 1) == $strana) echo "<strong>";
            echo ($z + 1);
            if (($z + 1) == $strana) echo "</strong>";
            if (($z + 1) != $strana) echo "</a>";
            echo "\n ";
          }
          echo "</p>

<div id='admin-table'>
<table>
<!-- Hlavicka tabulky - START -->\n";
          if ($tabulka != "aktivity") {
            if ($tabulka == "settings") {
              echo "<tr>
<th>Akcia</th>\n";
            }
            if ($tabulka != "settings") {
              echo "<tr>
<th colspan='3'>Akcia</th>\n";
            }
          }
          for($i = 0; $i < $pocet_stlpcov; $i++) {
            echo "<th><a href='browse.php?table=", $tabulka, "&amp;sort_by=", $mena_stlpcov[$i], "&amp;order=";
            if (isset($poradie) && $poradie == "ASC") echo "DESC";
            else echo "ASC";
            if ($strana) echo "&amp;page=", $strana;
            if (isset($CoHladat) && $CoHladat && isset($KdeHladat) && $KdeHladat && $KdeHladat != "nehladat") echo "&amp;search=", $CoHladat, "&amp;search_at=", $KdeHladat;
            if (isset($kolko) && $kolko) echo "&amp;rows=", $kolko;
            echo "' title='Radiť podľa stĺpca &quot;", $mena_stlpcov[$i], "&quot;'>", $mena_stlpcov[$i];
            if (isset($radit_podla) && $radit_podla == $mena_stlpcov[$i]) {
              if (isset($poradie) && $poradie == "ASC") echo " <img src='images/order_asc.gif' width='16' height='16' alt='Poradie ASC' title='Zoradené od A po Z'>";
              else echo " <img src='images/order_desc.gif' width='16' height='16' alt='Poradie DESC' title='Zoradené od Z po A'>\n";
            }
            echo "</a></th>\n";
          }
          echo "</tr>
<!-- Hlavicka tabulky - KONIEC -->\n\n";
          
          $sql = "SELECT * FROM `" . $tabulka . "`";
          if (isset($KdeHladat) && $KdeHladat && $KdeHladat != "nehladat" && isset($CoHladat) && $CoHladat) $sql .= " WHERE `" . $KdeHladat . "` LIKE '%" . $CoHladat . "%'";
          if (isset($radit_podla) && $radit_podla) $sql .= " ORDER BY `" . $radit_podla . "`";
          if (isset($poradie) && $poradie) $sql .= " " . $poradie;
          $sql .= " LIMIT ". $odkial . ", " . $kolko;
          $r = mysql_query($sql);
          $riadok = 1;
          while($zaznam = mysql_fetch_array($r)) {
            echo "
<!-- ", $riadok, ". riadok tabulky - START -->
<tr>\n";
            if ($tabulka != "settings") echo "<td><input type='checkbox' name='riadok[", $zaznam['id'], "]' title='Označte pre spracovanie'></td>\n";
            
            if ($tabulka != "tabulka-ktora-sa-nesmie-opravovat") echo "<td><a href='browse.php?table=", $tabulka, "&amp;id=", $zaznam['id'], "&amp;edit=1' title='Opraviť riadok'><img src='images/edit.gif' width='24' height='24' alt='Opravit'></a></td>\n";
            
            if ($tabulka != "settings") echo "<td><a href='browse.php?table=", $tabulka, "&amp;id=", $zaznam['id'], "' onclick='return VymazRiadok(this, ", $zaznam['id'], ")' title='Zmazať riadok'><img src='images/delete.gif' width='17' height='24' alt='Zmazať'></a></td>\n";
            
            for($i = 0; $i < $pocet_stlpcov; $i++) {
              mb_internal_encoding("UTF-8"); // Set internal character encoding to UTF-8
              
              $raw = $zaznam[$mena_stlpcov[$i]]; // Hodnota priamo z databazy
              $noHTML = htmlspecialchars($raw, ENT_QUOTES); // Vypnu sa HTML znacky
              
              if (mb_strlen($raw) > 50) $cut = mb_substr($noHTML, 0, 50) . " ...";
              else $cut = $noHTML;
              
              echo "<td class='right middle";
              if ($mena_stlpcov[$i] == "teaser") echo " teaser";
              if ($mena_stlpcov[$i] == "content") {
                echo "' title='&quot;", $mena_stlpcov[$i], "&quot; = &quot;", $cut, "&quot;'>";
              }
              else {
                echo "' title='&quot;", $mena_stlpcov[$i], "&quot; = &quot;", $noHTML, "&quot;'>";
              }
              
              if (($mena_stlpcov[$i] == "header_pic" || $mena_stlpcov[$i] == "teaser_pic") && $zaznam[$mena_stlpcov[$i]]) {
                $suborObr = substr($zaznam[$mena_stlpcov[$i]], 8, strlen($zaznam[$mena_stlpcov[$i]]) - 8);
                $suborCely = "../upload/thumbs/tn_" . $suborObr;
                
                if (file_exists($suborCely)) {
                  $rozmery = getimagesize($suborCely); // Zisti sirku a vysku obrazku
                  $margTop = (128 - $rozmery[1]) / 2;
                  
                  echo "<a class='lightbox thumbnail' href='../upload/", $suborObr, "' title='", $suborObr, "'>
<img src='../upload/thumbs/tn_", $suborObr, "' style='margin-top: ", $margTop, "px;' alt='", $suborObr, "'></a>";
                }
                else {
                  echo "<span class='warning'>Obrázok neexistuje!</span>";
                }
              }
              else if ($mena_stlpcov[$i] == "content" && $zaznam[$mena_stlpcov[$i]]) {
                echo "<input type='button' name='basic' value='Zobraziť' class='basic' onclick=\"getModal('get-content.php', '", $zaznam['id'], "');\">";
              }
              else {
                echo $cut;
              }
              
              // Ak sa zobrazuje stlpec "author_id"
              if ($mena_stlpcov[$i] == "author_id") {
                $ha = FillTableArray("users", "id", $zaznam['author_id']);
                echo " (";
                if ($ha['first_name']) echo $ha['first_name'];
                if ($ha['first_name'] && $ha['surename']) echo " ";
                if ($ha['surename']) echo $ha['surename'];
                echo ")";
                unset($ha);
              }
              
              // Ak sa zobrazuje stlpec "menu_id"
              if ($mena_stlpcov[$i] == "menu_id") {
                $ha = FillTableArray("menu", "id", $zaznam['menu_id']);
                echo " (", $ha['item'], ")";
                unset($ha);
              }
              
              // Ak sa zobrazuje stlpec "submenu_id"
              if ($mena_stlpcov[$i] == "submenu_id") {
                $ha = FillTableArray("submenu", "id", $zaznam['submenu_id']);
                $hb = FillTableArray("menu", "id", $ha['item_id']);
                echo " [", $hb['item'], "] - (", $ha['item'], ")";
                unset($ha);
                unset($hb);
              }
              
              //Ak sa zobrazuje stlpec "continent_id"
              /*
              if ($mena_stlpcov[$i] == "continent_id") {
                $ha = FillTableArray ("continents", "id", $zaznam['continent_id']);
                echo (" (" . $ha['continent'] . ")");
                unset ($ha);
              }
              */
              
              // Ak sa zobrazuje stlpec "language_id"
              if ($mena_stlpcov[$i] == "language_id") {
                $ha = FillTableArray("languages", "id", $zaznam['language_id']);
                echo " (", $ha['language'], ")";
                unset($ha);
              }
              
              
              // Ak sa zobrazuje stlpec "expr2transl_id", "expr_as_transl_id"
              if ($mena_stlpcov[$i] == "expr2transl_id" || $mena_stlpcov[$i] == "expr_as_transl_id") {
                $ha = FillTableArray("expressions", "id", $zaznam[$mena_stlpcov[$i]]);
                echo " (", substr($ha['expression'], 0, 64);
                if (strlen($ha['expression']) > 64) echo " ...";
                echo ")";
                unset($ha);
              }
              
              
              echo "</td>\n";
            }
            
            echo "</tr>
<!-- ", $riadok, ". riadok tabulky - KONIEC -->\n\n";

            $riadok++;
          }
          
          echo "</table>
</div>\n\n";

          if ($tabulka != "settings") {
            echo "<p id='pata_uprava_tabulky'>
<a href='browse.php?table=", $tabulka, "' onclick=\"if (OznacVsetky('formular')) return false;\" title='Označiť všetky riadky'>Označiť</a>
/
<a href='browse.php?table=", $tabulka, "' onclick=\"if (OdznacVsetky('formular')) return false;\" title='Odznačiť všetky riadky'>Odznačiť</a> všetko&nbsp;&nbsp;\n";
            //if ($tabulka == "objednavky") echo ("<input type='image' name='VybavViac' title='Vybaviť označené obednávky' src='../images/admin/vybav_obj.gif' />\n");
            if ($tabulka != "files") echo "<input type='image' name='OpravViac' title='Opraviť označené riadky' src='images/edit.gif'>\n";
            echo "<input type='image' name='VymazViac' title='Zmazať označené riadky' src='images/delete.gif'>
</p>\n\n";
          }
          
          echo "</form>

<p class='strany'>
<strong>Strany:</strong>&nbsp;\n";
          // Vypisanie navigacie
          for($z = 0; $z < $PocStr; $z++) {

            if (($z + 1) != $strana) {
              echo "<a href='browse.php?table=", $tabulka, "&amp;page=", ($z + 1);
              if (isset($radit_podla) && $radit_podla) echo "&amp;sort_by=", $radit_podla;
              if (isset($poradie) && $poradie) echo "&amp;order=", $poradie;
              if (isset($CoHladat) && $CoHladat && isset($KdeHladat) && $KdeHladat && $KdeHladat != "nehladat") echo "&amp;search=", $CoHladat, "&amp;search_at=", $KdeHladat;
              echo "&amp;rows=", $kolko, "'>";
            }
            
            if (($z + 1) == $strana) echo "<strong>";
            echo ($z + 1);
            if (($z + 1) == $strana) echo "</strong>";
            if (($z + 1) != $strana) echo "</a>";
            echo "\n ";
            
          }
          echo "</p>\n\n";
          
        }
        else {
          echo "<h1 class='caution center'>Tabuľka neobsahuje žiadne riadky (záznamy)!</h1>\n";
        }
        
      }
      
      
      echo "<p class='odkazy'>
<a href='index.php'>Návrat na úvodnú stránku Administrácie</a>\n";

      if ($ZobrazTabulku && ($pocet_riadkov > 0)) {

        echo "|
<a href='browse.php?table=", $tabulka;
        if (isset($radit_podla) && $radit_podla) echo "&amp;sort_by=", $radit_podla;
        if (isset($poradie) && $poradie) echo "&amp;order=", $poradie;
        if (isset($strana) && $strana) echo "&amp;page=", $strana;
        if (isset($CoHladat) && $CoHladat && isset($KdeHladat) && $KdeHladat && $KdeHladat != "nehladat") echo "&amp;search=", $CoHladat, "&amp;search_at=", $KdeHladat;
        if (isset($kolko) && $kolko) echo "&amp;rows=", $kolko;
        echo "'>Obnoviť stránku</a>\n";
        
      }
      
      if ($tabulka != "settings") {
        echo "|
<a href='add.php?table=", $tabulka, "'>Vložiť nový riadok</a>\n";
      }
      
      if (!$ZobrazTabulku) echo "|
<a href='browse.php?table=", $tabulka, "'>Zobraziť tabuľku &quot;", $tabulka, "&quot;</a>\n";

      echo "</p>\n\n";
      
    }
    // Ak nebol urceny nazov tabulky, zobrazi sa chyba
    else {
      echo "<h1 class='warning center'>CHYBA! - Nebol určený názov tabuľky!</h1>
<p class='odkazy'>
<a href='index.php'>Návrat na úvodnú stránku Administrácie</a>
</p>\n\n";
    }
    
            } else {
                // Nepovoleny vstup na stranku
                echo "<h1 class='warning center'>Nepovolený vstup na administrátorskú stránku!</h1>
<p class='center'>Vstup na túto stránku je povolený iba <strong>administrátorom</strong>!<br /><br /></p>
<p class='odkazy'><a href='index.php'>Návrat na úvodnú stránku Administrácie</a></p>\n\n";
            }

            unset($nastavenia);

            mysql_close($link); // Prerusi spojenie s databazou
            ?>
		</div>
		
		<!-- modal content -->
		<div id="basic-modal-content">
			<img src="images/ajax_loading.gif" width="143" height="93" alt="Loading content" class="loading" />
		</div>
		
		<!-- preload the images -->
		<div style="display:none">
			<img src="images/x.png" width="25" height="29" alt="Zatvoriť" />
		</div>
		
		<!-- Load jQuery, SimpleModal and Basic JS files -->
		<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
		<script type="text/javascript" src="js/jquery.lightbox-0.5.js"></script>
		<script type="text/javascript" src="js/jquery.simplemodal.js"></script>
		<script type="text/javascript" src="js/basic.js"></script>
		
		<!-- jQuery LightBox setting  -->
		<script type="text/javascript">
		  $(function() {
			// Select all links that contains lightbox in the attribute rel
			$('a.lightbox').lightBox();
		  });
		</script>
		
		<!-- Moj vlastny JavaScript -->
		<script src="js/script.js" type="text/javascript"></script>
		
		<!-- JavaScripty pre WYSIWYG editor TinyMCE -->
		<script src="js/tiny_mce/tiny_mce.js" type="text/javascript"></script>
		<script type="text/javascript">
			tinyMCE.init({
				// General options
				mode : "specific_textareas",
				editor_selector : "mceEditor",
				language : "sk",
				theme : "advanced",
				plugins : "advhr,advimage,advlink,autoresize,autosave,contextmenu,emotions,fullscreen,iespell,inlinepopups,insertdatetime,media,nonbreaking,paste,preview,safari,searchreplace,table,xhtmlxtras,wordcount",
				entity_encoding : "raw",
				
				// Theme options
				theme_advanced_buttons1 : "undo,redo,|,cut,copy,paste,pastetext,pasteword,|,search,replace,|,insertdate,inserttime,|,removeformat,code,preview,fullscreen,nonbreaking,|,link,unlink,anchor",
				theme_advanced_buttons2 : "formatselect,fontsizeselect,forecolor,backcolor,|,bold,italic,underline,strikethrough,sub,sup,|,justifyleft,justifycenter,justifyright,justifyfull,bullist,numlist,outdent,indent,blockquote,|,cite,abbr,acronym,del,ins",
				theme_advanced_buttons3 : "tablecontrols,|,hr,advhr,|,image,media,emotions,charmap,iespells",
				theme_advanced_toolbar_location : "top",
				theme_advanced_toolbar_align : "left",
				theme_advanced_statusbar_location : "bottom",
				theme_advanced_resizing : true
			});
		</script>
		<script type="text/javascript">
			function toggleEditor(id) {
				if (!tinyMCE.get(id)) tinyMCE.execCommand('mceAddControl', false, id);
				else tinyMCE.execCommand('mceRemoveControl', false, id);
			}
		</script>
	</body>
</html>