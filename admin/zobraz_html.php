<?php
  // (C) 2009 - 2010 Mario Herak - GUNSOFT
  // Autor: Mario Herak
  // www.gunsoft.sk, gunsoft@gunsoft.sk
  // Posledná úprava: 06.02.2010

  //Nacitanie a inicializacia premennych session
  session_register("Admin");

  //Pripojenie sa na databazu
  if (file_exists ("../prip_na_db.php")) require ("../prip_na_db.php");
  else exit ("<h1 align='center'>V koreňovom adresári chýba súbor &quot;prip_na_db.php&quot;!</h1>");

  //Nacita z tabuky "nastavenia" databazy vsetky nastavenia stranok
  $sql = "SELECT * FROM nastavenia WHERE id=1";
  $r = mysql_query ($sql);
  $nastavenia = mysql_fetch_array ($r);

  $lang = "sk"; //Nastavi jazyk na Slovensky

  if (isset ($_GET['id'])) $HTMLID = $_GET['id']; //Nacita id clanku
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<?php 
  //Nacitanie meta udajov zo suboru 'meta.php'
  if (file_exists ("../meta.php")) require ("../meta.php"); //Ak v korenovom adr. existuje subor 'meta.php', nacita sa jeho obsah
  else exit ("<h1>V koreňovom adr. chýba súbor \"meta.php\"!</h1>"); //Inak sa vypise chyba a ukonci kod
?>
    <link href="../favicon.ico" rel="shortcut icon">
    <link media="screen" type="text/css" href="../css/admin.css" rel="stylesheet">
<?php
  echo ("<title>Administrácia - ");
  if (isset ($HTMLID) && $HTMLID) echo ("Náhľad obsahu článku s id &quot;" . $HTMLID . "&quot;");
  else echo ("CHYBA! - Nebolo určené id článku obsahu");
  echo (" | " . $nastavenia['titulka_stranok_' . $lang] . "</title>\n");
?>
  </head>
  <body>    
    <div id="hlavny">
<?php
  if ($_SESSION["Admin"] == "TRUE") {
    if (isset ($HTMLID) && $HTMLID) { //Ak je urcene id clanku, zobrazi sa clanok
      //Nacitanie informacii o HTML stranke
      $sql = "SELECT titulka, obsah FROM html_stranky WHERE id=" . $HTMLID;
      $r = mysql_query ($sql);
      $obsah = mysql_fetch_array ($r);

      //Zobrazenie "html_stranky"
      echo ("<h1 class='hlavicka'>Náhľad článku</h1>
<div class='html_preview'>
        <h2 class='polozka_siva'>" . $obsah['titulka'] . "</h2>\n" . $obsah['obsah'] . "\n</div>\n");
      unset ($obsah);
    }
    else { //Ak nie je urcene id oclanku, zobrazi sa upozornenie
      echo ("<h1 class='vystraha'>CHYBA! - Nebolo určené id článku obsahu!</h1>\n");
    }
  }
  else { //Nepovoleny vstup na stranku
    echo ("<h1 class='vystraha'>Nepovolený vstup na administrátorskú stránku!</h1>
<p style='text-align: center'>
  Vstup na túto stránku je povolený iba <b>administrátorom</b>! Prosím zatvorte toto okno. Ďakujem.
</p>\n");
  }

  unset ($nastavenia);
  mysql_close ($link);
?>
    </div>
  </body>
</html>