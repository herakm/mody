<?php
/*
(C) 2009 - 2012 Mario Herak - GUNSOFT
Autor: Mario Herak
www.gunsoft.sk, gunsoft@gunsoft.sk
*/


// Inicializacia
if (file_exists("init.php")) include_once("init.php");
else exit("<h1 align='center'>File &quot;init.php&quot; is missing in root directory!</h1>");


// Nacita nazov tabulky z URL
if (isset($_GET['table'])) $tabulka = $_GET['table'];

// Nacita hodnotu tlacidla "Odoslat"
if (isset($_POST['odoslat'])) $odoslat = $_POST['odoslat'];
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<?php
// meta tag pre copyright
if ($settings['meta_copyright']) {
	echo "<meta name='copyright' content='", $settings['meta_copyright'], "'>\n";
}

// meta tag pre description
if ($settings['meta_desc']) {
	echo "<meta name='description' content='", $settings['meta_desc'], "'>\n";
}

// meta_keywords
if ($settings['meta_keywords']) {
	echo "<meta name='keywords' content='", $settings['meta_keywords'], "'>\n";
}

// favicon subor
if ($settings['favicon']) echo "\n<link href='", $settings['favicon'], "' rel='shortcut icon'>\n";
?>
		
		<link media="screen" type="text/css" rel="stylesheet" href="css/reset.css">
		<link media="screen" type="text/css" rel="stylesheet" href="css/screen.css">
		<link media="screen" type="text/css" rel="stylesheet" href="css/jquery.lightbox-0.5.css">
		
<?php
echo "<title>Administrácia - ";

if (isset($tabulka)) {
	echo "Vkladanie nového riadku do tabuľky &quot;", $tabulka, "&quot;";
}
else {
	echo "CHYBA! - Nebol určený názov tabuľky";
}

echo " | ", $settings['site_title'], "</title>\n";
?>
	</head>
	
	
	<body>
		<div id="hlavny">
<?php
if (isset($_SESSION['UserID']) && isset($_SESSION['Admin']) && $_SESSION['Admin'] == "TRUE") {
	
	// Ak bola urcena tabulka, zobrazia sa jej riadky
	if (isset($tabulka) && $tabulka) {
		echo "<h1 class='hlavicka'>Vkladanie nového riadku do tabuľky &quot;", $tabulka, "&quot;</h1>\n\n";
		
		
		// Nacita komentare jedn. stlpcov tabulky
		$komentare = Array();
		$i = 0;
		
		$sql ="SHOW FULL COLUMNS FROM `" . $tabulka . "`";
		$r = mysql_query($sql);
		while($comment = mysql_fetch_array($r)) {
			$komentare[$i] = $comment['Comment'];
			$i++;
		}
		unset($comment);
		
		
		// Nacita nazvy stlpcov v tabulke
		$sql = "SELECT * FROM `" . $tabulka . "`";
		$r = mysql_query($sql);
		
		// Pomocne premenne
		$pocet_stlpcov = 0;
		$name = "";
		$type = "";
		
		while($pocet_stlpcov < mysql_num_fields($r)) {
			$meta = mysql_fetch_field($r, $pocet_stlpcov);
			$name .= $meta->name . ",";
			$type .= $meta->type . ",";
			
			/*
			echo "<pre>
			blob:			$meta->blob
			max_length:		$meta->max_length
			multiple_key:	$meta->multiple_key
			name:			$meta->name
			not_null:		$meta->not_null
			numeric:		$meta->numeric
			primary_key:	$meta->primary_key
			table:			$meta->table
			type:			$meta->type
			default:		$meta->def
			unique_key:		$meta->unique_key
			unsigned:		$meta->unsigned
			zerofill:		$meta->zerofill
			</pre>";
			*/
			
			$pocet_stlpcov++;
		}
		
		$mena_stlpcov = split(",", $name);
		$typy_stlpcov = split(",", $type);
		
		
		// Ak uzivatel stlacil tlacidlo "Odoslat",
		if (isset($odoslat) && $odoslat) {
			// Ak sa vklada novy riadok do tabulky "files"
			if ($tabulka == "files") {
				// Upload suboru z formulara do pamate
				
				// Zdrojovy subor z formulara
				$UploadZdrojSubor = $_FILES['file']['tmp_name'];
				
				// Samotne meno suboru z formulara
				$UploadSubor = $_FILES['file']['name'];
				
				// Chyba uploadu
				$UploadChyba = $_FILES['file']['error'];
				
				// Typ suboru
				$UploadSuborTyp = $_FILES['file']['type'];
				
				// Velkost suboru
				$UploadSuborVelkost = $_FILES['file']['size'];
				
				// Kompletne URL
				$KompletneURL = "upload/" . $UploadSubor;
				
				// Ak upload suboru z formulara do pamate prebehol uspesne,
				if ($UploadChyba == 0) {
					// vycisti meno uploadovaneho suboru od nezelanych znakov
					$meno_suboru = ereg_replace("[^a-z0-9._-]", "",
						// nahradi medzeru a %20 v nazve suboru za podtrznik
						// a na konci konvertuje vsetky znaky na male
						str_replace (" ", "_",
							str_replace ("%20", "_",
								strtolower ($UploadSubor)
							)
						)
					);
					
					// Upload suboru do adresara "upload"
					@copy($UploadZdrojSubor, "../upload/" . $meno_suboru)
					or die("<h1 class='warning center'>Upload súboru nebol úspešný!</h1>

<p class='center'>
	Kopírovanie súboru &quot; $UploadSubor &quot; do adresára &quot;upload&quot; nebolo úspešné!
	Prosím kontaktujte administrátora! Ďakujem.
</p>

<p class='odkazy'>
	<a href='index.php'>Návrat na úvodnú stránku Administrácie</a>
</p>\n");
					
					// Ak je uploadovany subor typu gif, jpg alebo png, vytvori sa nahladovy obrazok do ../upload/thumbs/
					if ($UploadSuborTyp == "image/gif" || $UploadSuborTyp == "image/jpeg"
						|| $UploadSuborTyp == "image/png") {
						// vytvori nahl. obr. do podadr. 'thumbs' a vrati jeho meno
						$thumb_name = createThumb("../upload/" .  $meno_suboru, 128);
					}
					
					// Zmazanie docasneho suboru z pamate
					unlink($UploadZdrojSubor);
					
					echo "<h1 class='center notice'>Upload súboru bol úspešný!</h1>\n";
				}
				else if ($UploadChyba == 2) {
					die("<h1 class='warning center'>Upload súboru nebol úspešný!</h1>

<p class='center'>Veľkosť súboru presiahla maximálnu určenú hodnotu (1 MB)!</p>

<p class='odkazy'>
	<a href='index.php'>Návrat na úvodnú stránku Administrácie</a>
</p>\n");
				}
			}
			
			
			if ($tabulka != "files" || ($tabulka == "files" && isset($UploadChyba) && $UploadChyba == 0)) {
				// Nacita vlozene udaje z formular
				for($i = 0; $i < $pocet_stlpcov; $i++) {
					if (isset($_POST[$mena_stlpcov[$i]])) {
						// Ak nie je zapnute magic quotes gpc, prida lomitka pred znaky ('), ("), (\) a NUL (NULL byte)
						if (!get_magic_quotes_gpc()) {
							$form[$mena_stlpcov[$i]] = addslashes($_POST[$mena_stlpcov[$i]]);
						}
						else {
							$form[$mena_stlpcov[$i]] = $_POST[$mena_stlpcov[$i]];
						}
					}
				}
				
				// Vytvorenie SQL dotazu
				$sql = "INSERT INTO `" . $tabulka . "` (";
				for($i = 1; $i < $pocet_stlpcov; $i++) {
					$sql .= "`" . $mena_stlpcov[$i] . "`";
					if ($i < ($pocet_stlpcov - 1)) $sql .= ", ";
				}
				$sql .= ") VALUES (";
				for($i = 1; $i < $pocet_stlpcov; $i++) {
					// Ak sa vklada do tabulky "files" a meno stlpca je "file"
					if ($tabulka == "files" &&
						($mena_stlpcov[$i] == "file" || $mena_stlpcov[$i] == "thumb_name"
						 || $mena_stlpcov[$i] == "type")) {
						if ($mena_stlpcov[$i] == "file") $sql .= "'" . $meno_suboru . "'";
						
						// Urci meno suboru nahl. obr.
						if ($mena_stlpcov[$i] == "thumb_name") {
							// ak sa pozna meno nahl. obr, ulozi sa do databazy v tabulke 'files', stlpec 'thumb_name'
							if (isset($thumb_name)) {
								$sql .= "'" . $thumb_name . "'";
							}
							else {
								// ak sa nepozna, ulozi sa prazdna hodnota
								$sql .= "''";
							}
						}
						
						// Urci typ uploadovaneho suboru pre ulozenie do databazy
						if ($mena_stlpcov[$i] == "type") {
							$sql .= "'" . $UploadSuborTyp . "'";
						}
					}
					else {
						// Vkladanie hodnot pre ostatne tabulky
						$sql .= "'" . $form[$mena_stlpcov[$i]] . "'";
					}
					
					if ($i < ($pocet_stlpcov - 1)) {
						$sql .= ", ";
					}
				}
				$sql .= ")";
				$res = @mysql_query($sql);
				
				// Ak sa podarilo vlozit udaje do tabulky, vypise SQL kod
				if ($res) {
					echo "<h3 class='sql'>SQL kód:</h3>

<p class='sql_kod'>\n", htmlspecialchars($sql), "\n</p>

<h3 class='notice center'>Údaje z formulára boli úspešne vložené do tabuľky &quot;", $tabulka, "&quot;.</h3>\n";
				}
				else {
					// Ak sa nepodarilo vlozit udaje do tabulky
					echo "<h1 class='warning center'>Pri vkladaní údajov do databázy nastala chyba!</h1>
					
<p class='center'>Prosím, zatvorte toto okno a kontaktujte administrátora. Ďakujem.</p>

<h3 class='sql'>SQL kód:</h3>

<p class='sql_kod'>\n", htmlspecialchars($sql), "</p>\n";
				}
			}
		}
		else {
			// Ak uzivatel este nestlacil tlacidlo "Odoslat"
			echo "<form name='post' action='add.php?table=", $tabulka, "' method='post' onsubmit='return OverFormular();'";
			if ($tabulka == "files") {
				echo " enctype='multipart/form-data'";
			}
			echo ">\n\n";
			
			/*
			  Urci max. velkost uploadovaneho suboru na 30 MB
			  Max. velkost uploadovaneho suboru v jednotkach 'byte' = 1 MB, 1024 kB
			*/
			if ($tabulka == "files") {
				echo "<input type='hidden' name='MAX_FILE_SIZE' value='31457280'>\n\n";
			}
			
			echo "<table>\n\n";
			
			for($i = 0; $i < $pocet_stlpcov; $i++) {
				echo "<tr";
				if ($komentare[$i]) {
					echo " title='", $komentare[$i], "'";
				}
				echo ">
	<td class='right top'>\n";
				if ($komentare[$i]) {
					echo "<span class='column_comment' title='", $komentare[$i], "'>";
				}
				echo "<strong>", $mena_stlpcov[$i], ":</strong>";
				if ($komentare[$i]) {
					echo "</span>";
				}
				echo "\n</td>
	<td>";
				
				/* DEFINOVANIE TYPOV STLPCOV - ZACIATOK */
				
				// Ak je stlpec typu retazec
				if (($typy_stlpcov[$i] == "string")
					&& ($tabulka != "files"
						|| ($tabulka == "files"
							&& $mena_stlpcov[$i] != "file"
							&& $mena_stlpcov[$i] != "type"
							&& $mena_stlpcov[$i] != "thumb_name"))
					&& ($tabulka != "content"
						|| ($tabulka == "content"
							&& $mena_stlpcov[$i] != "header_pic"
							&& $mena_stlpcov[$i] != "teaser_pic"
							&& $mena_stlpcov[$i] != "extrnal_window"))) {
					echo "<input type='text' name='", $mena_stlpcov[$i], "' size='50' maxlength='255'>";
				}
				
				// Ak je stlpec typu text
				if ($typy_stlpcov[$i] == "blob") {
					echo "\n<textarea name='", $mena_stlpcov[$i], "' rows='10' cols='80'";
					
					if ($mena_stlpcov[$i] != "expression"
						&& $mena_stlpcov[$i] != "meta_keywords"
						&& $mena_stlpcov[$i] != "meta_desc"
						&& $mena_stlpcov[$i] != "plus"
						&& $mena_stlpcov[$i] != "minus"
						&& $mena_stlpcov[$i] != "result"
						&& $mena_stlpcov[$i] != "links") {
						// prida triedu pre TinyMC editor
						echo " class='mceEditor'";
					}
					
					echo "></textarea>\n";
					
					if ($mena_stlpcov[$i] != "expression"
						&& $mena_stlpcov[$i] != "meta_keywords"
						&& $mena_stlpcov[$i] != "meta_desc"
						&& $mena_stlpcov[$i] != "plus"
						&& $mena_stlpcov[$i] != "minus"
						&& $mena_stlpcov[$i] != "result"
						&& $mena_stlpcov[$i] != "links") {
						// link na skryvanie/zobrazovanie TinyMC editor
						echo "<br>
<a href=\"javascript:toggleEditor('", $mena_stlpcov[$i], "');\">Ukáž/Schovaj editor</a>\n";
					}
				}
				
				// Ak je stlpec typu cislo
				if ($typy_stlpcov[$i] == "int" || $typy_stlpcov[$i] == "real") {
					if ($mena_stlpcov[$i] != "id"
						&& $mena_stlpcov[$i] != "admin"
						&& $mena_stlpcov[$i] != "published"
						&& $mena_stlpcov[$i] != "publish"
						&& $mena_stlpcov[$i] != "continent_id"
						&& $mena_stlpcov[$i] != "language_id"
						&& $mena_stlpcov[$i] != "lang_id"
						&& $mena_stlpcov[$i] != "author_id"
						&& $mena_stlpcov[$i] != "menu_id"
						&& $mena_stlpcov[$i] != "submenu_id"
						&& $mena_stlpcov[$i] != "submenu_item"
						&& $mena_stlpcov[$i] != "is_video"
						&& $mena_stlpcov[$i] != "is_external"
						&& $mena_stlpcov[$i] != "expr2transl_id"
						&& $mena_stlpcov[$i] != "expr_as_transl_id"
						&& $mena_stlpcov[$i] != "grp1"
						&& $mena_stlpcov[$i] != "grp2"
						&& $mena_stlpcov[$i] != "grp3"
						&& $mena_stlpcov[$i] != "grp4"
						&& $mena_stlpcov[$i] != "times_dwnl"
						&& $mena_stlpcov[$i] != "rating"
						&& $mena_stlpcov[$i] != "users_rating"
						&& $mena_stlpcov[$i] != "model_id"
						&& $mena_stlpcov[$i] != "item_id"
						&& $mena_stlpcov[$i] != "viewed") {
						echo "<input type='text' name='", $mena_stlpcov[$i], "' size='20' maxlength='30'>";
					}
				}
				
				// Ak je stlpec typu datum
				if ($typy_stlpcov[$i] == "date") {
					$datum = date("Y-m-d");
					echo "<input type='text' name='", $mena_stlpcov[$i], "' value='", $datum,
					"' size='10' maxlength='10' readonly='readonly'>";
				}
				
				// Ak je stlpec typu cas
				if ($typy_stlpcov[$i] == "time") {
					$cas = date("H:i:s");
					echo "<input type='text' name='", $mena_stlpcov[$i], "' value='", $cas,
					"' size='8' maxlength='8' readonly='readonly'>";
				}
				
				/* DEFINOVANIE TYPOV STLPCOV - KONIEC */
				
				
				/* DEFINOVANIE SPECIALNYCH STLPCOV - ZACIATOK */
				
				// Ak sa stlpec vola "viewed", "users_rating"
				if ($typy_stlpcov[$i] == "viewed"
					|| $typy_stlpcov[$i] == "users_rating") {
					echo "<input type='text' name='", $mena_stlpcov[$i], "' value='' size='8' readonly='readonly'>";
				}
				
				// Ak sa stlpec vola "author_id"
				if ($mena_stlpcov[$i] == "author_id") {
					$ha = FillTableArray ("users", "id", $_SESSION["UserID"]);
					
					echo "\n<input type='hidden' name='", $mena_stlpcov[$i], "' value='", $_SESSION["UserID"], "'>
<input type='text' value='";
					if ($ha['first_name']) echo $ha['first_name'];
					if ($ha['first_name'] && $ha['surename']) echo " ";
					if ($ha['surename']) echo $ha['surename'];
					echo "' size='50' maxlength='255' readonly='readonly'>\n";
					unset($ha);
				}
				
				// Ak sa stlpec vola "admin", "published", 'submenu_item', 'is_video', 'is_external'
				if ($mena_stlpcov[$i] == "admin"
					|| $mena_stlpcov[$i] == "published"
					|| $mena_stlpcov[$i] == "publish"
					|| $mena_stlpcov[$i] == "submenu_item"
					|| $mena_stlpcov[$i] == "is_video"
					|| $mena_stlpcov[$i] == "is_external") {
					echo "\n<select size='1' name='", $mena_stlpcov[$i], "'>
	<option value='1'";
					if ($mena_stlpcov[$i] == "published") {
						echo " selected='selected'";
					}
					echo ">Áno</option>
	<option value='0'";
					if ($mena_stlpcov[$i] == "admin" || $mena_stlpcov[$i] == "submenu_item"
						|| $mena_stlpcov[$i] == "is_video" || $mena_stlpcov[$i] == "is_external") {
						echo " selected='selected'";
					}
					echo ">Nie</option>
</select>\n";
				}
				
				// Ak sa stlpec vola "language_id", "lang_id"
				if ($mena_stlpcov[$i] == "language_id" || $mena_stlpcov[$i] == "lang_id") {
					echo "\n<select size='1' name='" . $mena_stlpcov[$i] . "'>
	<option value='0'>- Default -</option>\n";
					
					$sql = "SELECT * FROM `languages` ORDER BY `language`";
					$r = mysql_query($sql);
					while($lang = mysql_fetch_array($r)) {
						echo "<option value='", $lang['id'], "'>", $lang['language'], "</option>\n";
					}
					unset($lang);
					
					echo "</select>\n";
				}
				
				// Ak sa stlpec vola "continent_id"
				if ($mena_stlpcov[$i] == "continent_id") {
					echo "\n<select size='1' name='", $mena_stlpcov[$i], "'>\n";
					
					$sql = "SELECT * FROM `continents` ORDER BY `continent`";
					$r = mysql_query($sql);
					while($cont = mysql_fetch_array($r)) {
						echo "<option value='", $cont['id'], "'>", $cont['continent'], "</option>\n";
					}
					unset($cont);
					
					echo "</select>\n";
				}
				
				// Ak sa stlpec vola "menu_id"
				if ($mena_stlpcov[$i] == "menu_id") {
					echo "\n<select size='1' name='", $mena_stlpcov[$i], "'>
	<option value='' selected='selected'";
					echo ">-----</option>\n";
					
					$sql = "SELECT * FROM `menu` WHERE `published`='1' ORDER BY `order`";
					$r = mysql_query($sql);
					while($menu = mysql_fetch_array($r)) {
						echo "<option value='", $menu['id'], "' title='", $menu['title'], "'>", $menu['item'], "</option>\n";
					}
					unset($menu);
					
					echo "</select>\n";
				}
				
				// Ak sa stlpec vola "submenu_id"
				if ($mena_stlpcov[$i] == "submenu_id") {
					echo "\n<select size='1' name='", $mena_stlpcov[$i], "'>
	<option value='' selected='selected'";
					echo ">-----</option>\n";
					
					$sql = "SELECT * FROM `submenu` WHERE `published`='1' ORDER BY `item_id`, `order`";
					$r = mysql_query($sql);
					while ($submenu = mysql_fetch_array($r)) {
						$ha = FillTableArray("menu", "id", $submenu["item_id"]);
						echo "<option value='", $submenu['id'], "' title='", $submenu['title'], "'>[", $ha['item'], "] - ", $submenu['item'], "</option>\n";
						unset ($ha);
					}
					unset ($submenu);
					
					echo "</select>\n";
				}
				
				// Ak sa stlpec vola "file" (tab. "files")
				if ($mena_stlpcov[$i] == "file") {
					echo "<input type='file' name='", $mena_stlpcov[$i], "' size='50'>\n";
				}
				
				// Ak sa stlpec vola 'header_pic' (tab. "content"), alebo 'teaser_pic'
				if ($mena_stlpcov[$i] == "header_pic" || $mena_stlpcov[$i] == "teaser_pic") {
					echo "\n<input type='hidden' checked='checked' name='", $mena_stlpcov[$i], "' value=''>

<div class='thumb_view'>\n";
					
					$sql = "SELECT `file`, `desc`, `thumb_name` FROM `files` WHERE `type` LIKE 'image%' AND `published` = 1 ORDER BY `file` ASC";
					$r = mysql_query($sql);
					while ($files = mysql_fetch_array($r)) {
						// Ziskanie info o nahl. obr.
						$thumb_filename = "../upload/thumbs/" . $files['thumb_name'];
						$thumb_info = getimagesize($thumb_filename); // Zisti info o nahl. obr.
						$thumb_width = $thumb_info[0]; // Zisti sirku nahl. obr.
						$thumb_height = $thumb_info[1]; // Zisti vysku nahl. obr.
						
						// Vycentrovanie nahl. obr.
						$thumb_marg_left = (128 - $thumb_width) / 2; // Urci odsadenie nahl. obr. zlava
						$thumb_marg_top = (128 - $thumb_height) / 2; // Urci odsadenie nahl. obr. zlava
						
						// Ziskanie info o obr.
						$img_filename = "../upload/" . $files['file'];
						$img_info = getimagesize ($img_filename); // Zisti info o obr.
						$img_width = $img_info[0]; // Zisti sirku obr.
						$img_height = $img_info[1]; // Zisti vysku obr.
						
						echo "<div class='thumb_frame'>
	<div class='thumb_pic'>
		<a href='", $img_filename, "' title='", $files['file'], "' class='lightbox'>
			<img src='", $thumb_filename, "' ", $thumb_info[3], " alt='", $files['thumb_name'], "' style='margin-top: ", $thumb_marg_top, "px; margin-left: ", $thumb_marg_left, "px;'>
		</a>
	</div>
	
	<p class='thumb_desc'>
		<input type='radio' name='", $mena_stlpcov[$i], "' value='/upload/", $files['file'], "'>", $img_width, "x", $img_height, " - ", $files['file'];
						if ($files['desc']) {
							echo "\n- ", $files['desc'];
						}
						
						echo "</p>
</div>\n\n";
					}
					unset ($files);
					
					echo "</div>\n";
				}
				
				// Ak sa stlpec vola "extrnal_window"
				if ($mena_stlpcov[$i] == "extrnal_window") {
					echo "\n<select size='1' name='", $mena_stlpcov[$i], "'>
	<option value='' selected='selected'>-----</option>
	<option value='_self'>_self (same window)</option>
	<option value='_blank'>_blank (new window)</option>
</select>\n";
				}
				
				// Ak sa stlpec vola "expr2transl_id", "expr_as_transl_id"
				if ($mena_stlpcov[$i] == "expr2transl_id" || $mena_stlpcov[$i] == "expr_as_transl_id") {
					echo "\n<select size='1' name='", $mena_stlpcov[$i], "'>\n";
					
					$sql = "SELECT * FROM `expressions` ORDER BY `expression`";
					$r = mysql_query($sql);
					while ($expr = mysql_fetch_array($r)) {
						echo "<option value='", $expr['id'], "'>", substr($expr['expression'], 0, 128);
						if (strlen($expr['expression']) > 128) echo " ...";
						echo "</option>\n";
					}
					unset($expr);
					
					echo "</select>\n";
				}
				
				// Ak sa stlpec vola "grp1"
				if ($mena_stlpcov[$i] == "grp1") {
					echo "\n<select class='ajax' id='", $mena_stlpcov[$i], "' size='1' name='", $mena_stlpcov[$i], "' _colname='", $mena_stlpcov[$i], "' _table='groups_2' _next='grp2'>
	<option value=''>Vyberte z možností</options>\n";
					
					$query = "SELECT * FROM `groups_1` WHERE `publish`='1' ORDER BY `name` ASC";
					$rows = mysql_query($query);
					while ($result = mysql_fetch_array($rows)) {
						echo "<option value='", $result['id'], "'>", $result['name'], "</option>\n";
					}
					unset($expr);
					
					echo "</select>\n";
				}
				
				// Ak sa stlpec vola "grp2"
				if ($mena_stlpcov[$i] == "grp2") {
					echo "\n<select class='ajax' id='", $mena_stlpcov[$i], "' size='1' name='", $mena_stlpcov[$i], "' _colname='", $mena_stlpcov[$i], "' _table='groups_3' _next='grp3'>\n";
					echo "</select>\n";
				}
				
				// Ak sa stlpec vola "grp3"
				if ($mena_stlpcov[$i] == "grp3") {
					echo "\n<select class='ajax' id='", $mena_stlpcov[$i], "' size='1' name='", $mena_stlpcov[$i], "' _colname='", $mena_stlpcov[$i], "' _table='groups_4' _next='grp4'>\n";
					echo "</select>\n";
				}
				
				// Ak sa stlpec vola "grp4"
				if ($mena_stlpcov[$i] == "grp4") {
					echo "\n<select id='", $mena_stlpcov[$i], "' size='1' name='", $mena_stlpcov[$i], "' _colname='", $mena_stlpcov[$i], "'>\n";
					echo "</select>\n";
				}
				
				// Ak sa stlpec vola "rating"
				if ($mena_stlpcov[$i] == "rating") {
					echo "\n<select id=", $mena_stlpcov[$i], " size='1' name='", $mena_stlpcov[$i], "'>\n";
					
					for ($z = 1; $z <= 10; $z++) {
						echo "<option value='", $z, "'>", $z, "</option>\n";
					}
					
					echo "</select>/10\n";
				}
				
				// Ak sa stlpec vola 'model_id'
				if ($mena_stlpcov[$i] == "model_id") {
					echo "\n<select size='1' name='", $mena_stlpcov[$i], "'>\n";
					echo "<option value=''>Vyberte z možností</options>\n";
					
					$query = "SELECT * FROM `models` WHERE `publish`='1' ORDER BY `model_nr` ASC";
					$rows = mysql_query($query);
					while ($result = mysql_fetch_array($rows)) {
						echo "<option value='", $result['id'], "'";
						echo ">(", $result['model_nr'], ") ", $result['name'], "</option>\n";
					}
					unset ($result);
					
					echo "</select>\n";
				}
				
				// Ak sa stlpec vola 'item_id'
				if ($mena_stlpcov[$i] == "item_id") {
					if ($tabulka == "reviews") {
						echo "\n<select size='1' name='", $mena_stlpcov[$i], "'>\n";
						echo "<option value=''>Vyberte z možností</options>\n";
						
						$query = "SELECT `id`, `name` FROM `items` WHERE `publish`='1' ORDER BY `date` DESC, `time` DESC";
						$rows = mysql_query($query);
						while ($result = mysql_fetch_array($rows)) {
							echo "<option value='", $result['id'], "'";
							echo ">", $result['name'], "</option>\n";
						}
						unset ($result);
						
						echo "</select>\n";
					}
				}
				
				/* DEFINOVANIE SPECIALNYCH STLPCOV - KONIEC */
				
				
				echo "</td>
</tr>\n\n";
			}
			
			
			echo "<tr>
	<td colspan='2' class='center'>
		<input type='submit' name='odoslat' value='Odoslať'>
	</td>
</tr>
</table>
</form>\n\n";
		}
		
		
		echo "<p class='odkazy'>
	<a href='index.php'>Návrat na úvodnú stránku Administrácie</a>
	|
	<a href='browse.php?table=", $tabulka, "'>Zobraziť tabuľku &quot;", $tabulka, "&quot;</a>\n";
		if (isset($odoslat)) {
			echo "|
	<a href='add.php?table=", $tabulka, "'>Vložiť ďalší riadok do tabuľky &quot;", $tabulka, "&quot;</a>\n";
		}
		echo "</p>\n\n";      
	}
	else {
		// Ak nebol urceny nazov tabulky, zobrazi sa chyba
		echo "<h1 class='warning center'>CHYBA! - Nebol určený názov tabuľky!</h1>

<p class='odkazy'>
	<a href='index.php'>Návrat na úvodnú stránku Administrácie</a>
</p>\n";
	}
}
else {
	// Nepovoleny vstup na stranku
	echo "<h1 class='warning center'>Nepovolený vstup na administrátorskú stránku!</h1>
	
<p class='center'>
	Vstup na túto stránku je povolený iba <strong>administrátorom</strong>! Prosím zatvorte toto okno. Ďakujem.
</p>\n\n";
}

unset ($nastavenia);
mysql_close($link);
?>
		</div>
		
		<!-- Load jQuery, SimpleModal and Basic JS files -->
		<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
		<script type="text/javascript" src="js/jquery.lightbox-0.5.js"></script>
		<script type="text/javascript" src="js/jquery.simplemodal.js"></script>
		<script type="text/javascript" src="js/basic.js"></script>
		
		<!-- jQuery LightBox setting  -->
		<script type="text/javascript">
		  $(function() {
			// Select all links that contains lightbox in the attribute rel
			$('a.lightbox').lightBox();
		  });
		</script>
		
		<!-- Moj vlastny JavaScript -->
		<script src="js/script.js" type="text/javascript"></script>
		
		<!-- JavaScripty pre WYSIWYG editor TinyMCE -->
		<script src="js/tiny_mce/tiny_mce.js" type="text/javascript"></script>
		<script type="text/javascript">
			tinyMCE.init({
				// General options
				mode : "specific_textareas",
				editor_selector : "mceEditor",
				language : "sk",
				theme : "advanced",
				plugins : "advhr,advimage,advlink,autoresize,autosave,contextmenu,emotions,fullscreen,iespell,inlinepopups,insertdatetime,media,nonbreaking,paste,preview,safari,searchreplace,table,xhtmlxtras,wordcount",
				entity_encoding : "raw",
				
				// Theme options
				theme_advanced_buttons1 : "undo,redo,|,cut,copy,paste,pastetext,pasteword,|,search,replace,|,insertdate,inserttime,|,removeformat,code,preview,fullscreen,nonbreaking,|,link,unlink,anchor",
				theme_advanced_buttons2 : "formatselect,fontsizeselect,forecolor,backcolor,|,bold,italic,underline,strikethrough,sub,sup,|,justifyleft,justifycenter,justifyright,justifyfull,bullist,numlist,outdent,indent,blockquote,|,cite,abbr,acronym,del,ins",
				theme_advanced_buttons3 : "tablecontrols,|,hr,advhr,|,image,media,emotions,charmap,iespells",
				theme_advanced_toolbar_location : "top",
				theme_advanced_toolbar_align : "left",
				theme_advanced_statusbar_location : "bottom",
				theme_advanced_resizing : true
			});
		</script>
		<script type="text/javascript">
			function toggleEditor(id) {
				if (!tinyMCE.get(id)) tinyMCE.execCommand('mceAddControl', false, id);
				else tinyMCE.execCommand('mceRemoveControl', false, id);
			}
		</script>
	</body>
</html>