<?php
/*
  (C) 2009 - 2012 Mario Herak - GUNSOFT
  Autor: Mario Herak
  www.gunsoft.sk, gunsoft@gunsoft.sk
*/

// Inicializacia
if (file_exists("init.php")) include_once("init.php");
else exit("<h1 align='center'>File &quot;init.php&quot; is missing in root directory!</h1>");


if (isset($_GET['table']) && $_GET['table']) $tabulka = $_GET['table']; // Nacita nazov tabulky z URL
if (isset($_GET['action']) && $_GET['action']) $akcia = $_GET['action']; // Nacita akciu ktoru treba previest


// Dump databazy
function ZalohaDatabazy($db_name) {
    $tab_status = mysql_query("SHOW TABLE STATUS");
    while ($all = mysql_fetch_assoc($tab_status)) $tbl_stat[$all['Name']] = $all['Auto_increment'];
    unset($backup);
    $backup = "";
    $tables = mysql_list_tables($db_name);
    while ($tabs = mysql_fetch_row($tables)):
        $backup .= "--\n
-- Struktura tabulky pre `$tabs[0]`\n
--\n
\n
\n
DROP TABLE IF EXISTS `$tabs[0]`;\n
CREATE TABLE IF NOT EXISTS `$tabs[0]` (";
        $res = mysql_query("SHOW CREATE TABLE $tabs[0]");
        while ($all = mysql_fetch_assoc($res)):
            $str = str_replace("CREATE TABLE `$tabs[0]` (", "", $all['Create Table']);
            $str = str_replace(",", ",", $str);
            $str2 = str_replace("`) ) TYPE=MyISAM ", "`)\n ) TYPE=MyISAM ", $str);
            $backup .= $str2 . " AUTO_INCREMENT=" . $tbl_stat[$tabs[0]] . ";\n\n";
        endwhile;
        $backup .= "--\n
-- Data to be executed for table `$tabs[0]`\n
--\n
\n";
        $data = mysql_query("SELECT * FROM $tabs[0]");
        while ($dt = mysql_fetch_row($data)):
            $backup .= "INSERT INTO `$tabs[0]` VALUES('$dt[0]'";
            for ($i = 1; $i < sizeof($dt); $i++) $backup .= ", '$dt[$i]'";
            $backup .= ");\n";
        endwhile;
        $backup .= "\n-- --------------------------------------------------------\n\n";
    endwhile;
    $fName = "zaloha_" . date("Y-m-d") . "_" . date("H-i-s") . ".sql";
    $fp = fopen("../backup/" . $fName, "w");
    fwrite($fp, $backup);
    echo ("<h2 class='oznamenie'>Záloha databázy prebehla úspešne!</h2>\n");
}

// User log-out
if (isset($akcia) && $akcia == "log-out") {
    unset($_SESSION['UserID']);
    unset($_SESSION['Admin']);

    session_unset();
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <?php
        if ($settings['meta_copyright']) echo "<meta name='copyright' content='", $settings['meta_copyright'], "'>\n"; // meta tag pre copyright
        if ($settings['meta_desc']) echo "<meta name='description' content='", $settings['meta_desc'], "'>\n"; // meta tag pre description
        if ($settings['meta_keywords']) echo "<meta name='keywords' content='", $settings['meta_keywords'], "'>\n"; // meta_keywords
        if ($settings['favicon']) echo "<link href='", $settings['favicon'], "' rel='shortcut icon'>\n"; // favicon subor
        ?>

        <link media="screen" type="text/css" rel="stylesheet" href="css/reset.css">
        <link media="screen" type="text/css" rel="stylesheet" href="css/screen.css">

        <script src="js/script.js" type="text/javascript"></script>
        <script src="js/jquery-1.7.2.min.js" type="text/javascript"></script>

        <?php
        echo "<title>Administrácia - ";
        if (!isset($akcia)) echo "Úvodná stránka";
        else {
            if ($akcia == "db_dump") echo "Záloha databázy";
            if ($akcia == "vyprazdni_tabulku") {
                echo "Vyprázdnenie tabuľky";
                if (isset($tabulka)) echo " &quot;", $tabulka, "&quot;";
            }
            if($akcia == "log-out") {
                echo "Odhlásenie";
            }
        }
        echo " | ", $settings['site_title'], "</title>\n";
        ?>
    </head>

    <body>
        <div id="hlavny">
            <?php
            // Display log-in form for non-logged users
            if (!isset($_SESSION['UserID'])) {
                // Ak sa uzivatel odhlasil, zobrazi sa oznamenie
                if($akcia == "log-out") {
                    echo "<h3 class='center caution'>Boli ste úspešne odhlásený.</h3>\n\n";
                }

                // Include log-in form
                if (file_exists("log-in-form.php")) require("log-in-form.php");
                else exit("<h1>V adresári chýba súbor <u>log-in-form.php</u>!</h1>\n");
            }


            // Zobrazi uvodnu stranu administracie prihlasenym uzivatelom
            if (isset($_SESSION['UserID']) && isset($_SESSION['Admin']) && $_SESSION['Admin'] == "TRUE") {

                echo "<h1 class='hlavicka'>Administrácia</h1>\n\n";


                if (isset($akcia) && $akcia == "db_dump") ZalohaDatabazy($db); // Zaloha databazy


                // Vyprazdnenie tabulky
                if (isset($akcia) && $akcia == "vyprazdni_tabulku" && isset($tabulka)) {
                    $sql = "SELECT id FROM " . $tabulka;
                    $r = mysql_query($sql);
                    $prt = mysql_num_rows($r); //$prt = Pocet Riadkov Tabulky ;-)

                    if ($prt) { //Ak sa nasiel aspon jeden riadok v danej tabulke, tabulka sa vyprazdni
                        //Pri tabulke "subory" sa vymazu aj fyzicke subory
                        if ($tabulka == "subory") {
                            $sql = "SELECT subor FROM subory";
                            $r = mysql_query($sql);
                            while ($subory = mysql_fetch_array($r)) {
                                $subor = "../upload/" . $subory['subor'];
                                $err = @unlink($subor); //Vymaze subor
                                if (!$err) echo ("<h2 class='vystraha'>Zmazanie súboru " . $subor . " bolo neúspešné!</h2>
          <p style='margin: 0; text-align: center;'>Prosím, kontaktujte <b>administrátora</b>. Ďakujem.</p>\n");
                                else echo ("<h2 class='oznamenie'>Zmazanie súboru " . $subor . " bolo úspešné!</h2>\n");
                            }
                        }

                        $sql = "TRUNCATE TABLE " . $tabulka;
                        $r = @mysql_query($sql);
                        if (!$r) echo ("<h2 class='vystraha'>Vyprázdnenie tabuľky &quot;" . $tabulka . "&quot; bolo neúspešné!</h2>
          <p style='margin: 0; text-align: center;'>Prosím kontaktujte <b>administrátora</b>. Ďakujem.</p>\n");
                        else echo ("<h2 class='oznamenie'>Tabuľka &quot;" . $tabulka . "&quot; bola vyprázdnená!</h2>\n");
                    }
                    else { //Ak sa nenasiel v danej tabulke ani jeden riadok, vypise sa chyba
                        echo ("<h2 class='upozornenie'>V tabuľke &quot;" . $tabulka . "&quot; nie je ani jeden riadok!</h2>\n");
                    }
                }


                // Ak nie je zadana ziadna akcia, zobrazia sa tabulky databazy
                if (!isset($akcia)) {
                    $sql = "SHOW TABLE STATUS FROM `" . $db . "`";
                    $r = mysql_query($sql);
                    while ($zaznam = mysql_fetch_array($r)) {
                        echo "<div class='tabulka'>
<h4 class='hlavicka_2'>", $zaznam['Name'], "</h4>
<p class='hlavicka_2_comment'>";
                        if (!$zaznam['Comment']) echo "&nbsp;";
                        else echo $zaznam['Comment'];
                        echo "</p>
<p class='obsah'>
<a href='browse.php?table=", $zaznam['Name'], "' title='Prechádzať tabuľku &quot;", $zaznam['Name'], "&quot;'>Prechádzať</a><br />\n";

                        if ($zaznam['Name'] != "settings") {
                            echo "<a href='add.php?table=", $zaznam['Name'], "' title='Vložiť riadok do tabuľky &quot;", $zaznam['Name'], "&quot;'>Vložiť</a><br />\n";
                        }

                        if ($zaznam['Name'] != "settings") echo "<a href='index.php' onclick='return VyprazdniTabulku(this, &quot;", $zaznam['Name'], "&quot;)' title='Vyprázdniť tabuľku &quot;", $zaznam['Name'], "&quot;'>Vyprázdniť</a>\n";
                        else echo "&nbsp;";

                        echo "</p>
</div>\n\n";
                    }

                    // Ziska udaje o prihlasenom uzivatelovi
                    $sql = "SELECT `surname`, `forename` FROM `users` WHERE `id`='" . $_SESSION['UserID'] . "'";
                    $r = mysql_query($sql);
                    $user_details = mysql_fetch_array($r);

                    echo "<div id='zalohovat_databazu' class='clear'>
<img src='images/backup.jpg' alt='Zálohovať' width='14' height='14' />
<a href='index.php?action=db_dump' title='Zálohovať databázu'>Zálohovať databázu</a>
</div>

<div id='manual'>
<img src='images/manual.gif' alt='Manuál' width='14' height='14' />
<a href='manual.html' target='_blank' title='Zobraziť manuál administrácie'>Manuál</a>
</div>

<div id='user-info'>
Prihlásený už.: <strong>", $user_details['forename'], " ", $user_details['surname'], "</strong> -
<a href='index.php?action=log-out' title='Odhlásiť'>Odhlásiť</a>
</div>\n\n";
                }
                else echo "<p class='odkazy'>
<a href='index.php' title='Návrat na úvodnú stránku Administrácie'>Návrat na úvodnú stránku Administrácie</a>
</p>\n";
            }

            unset($nastavenia);

            mysql_close($link); // Prerusi spojenie s databazou
            ?>
        </div>

        <script type="text/javascript">
            $(document).ready(function(){
                $('#nick').focus();
            });
        </script>
    </body>
</html>