<?php
  // (C) 2009 - 2010 Mario Herak - GUNSOFT
  // Autor: Mario Herak
  // www.gunsoft.sk, gunsoft@gunsoft.sk
  // Posledná úprava: 06.02.2010

  //Nacitanie a inicializacia premennych session
  session_register("Admin");

  //Pripojenie sa na databazu
  if (file_exists ("../prip_na_db.php")) require ("../prip_na_db.php");
  else exit ("<h1 align='center'>V koreňovom adresári chýba súbor &quot;prip_na_db.php&quot;!</h1>");

  //Nacita preklad
  if (file_exists ("../preklad.php")) require ("../preklad.php");
  else exit ("<h1 align='center'>V koreňovom adresári chýba súbor &quot;preklad.php&quot;!</h1>");

  //Nacita z tabuky "nastavenia" databazy vsetky nastavenia stranok
  $sql = "SELECT * FROM nastavenia WHERE id=1";
  $r = mysql_query ($sql);
  $nastavenia = mysql_fetch_array ($r);

  $lang = "sk"; //Nastavi jazyk na Slovensky

  if (isset ($_GET['id'])) $ID = $_GET['id'];

  $konv_kurz = $nastavenia['konverzny_kurz']; //Urci konverzny kurz
  $zobraz_dual = $nastavenia['dualne_zobrazovanie']; //Urci ci sa ma dualne zobrazovat cena: 1 = ano, 0 = nie

  $itts_nazov = $nastavenia['itts_nazov'];
  $itts_http = $nastavenia['itts_http']; //Musi koncit lomitkom "/" !!!
  $itts_adresa = $nastavenia['itts_adresa'];
  $itts_ico = $nastavenia['itts_ico'];
  $itts_dic = $nastavenia['itts_dic'];
  $itts_telefon = $nastavenia['itts_telefon'];
  $itts_mobil_domi = $nastavenia['itts_mobil_1'];
  $itts_mobil_quido = $nastavenia['itts_mobil_2'];
  $itts_fax = $nastavenia['itts_fax'];
  $itts_email = $nastavenia['itts_email'];
  $itts_cislo_uctu = $nastavenia['itts_cislo_uctu'];
  $itts_iban = $nastavenia['itts_iban'];
  $itts_konst_symb = $nastavenia['itts_konst_symb'];
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<?php 
  //Nacitanie meta udajov zo suboru 'meta.php'
  if (file_exists ("../meta.php")) require ("../meta.php"); //Ak v korenovom adr. existuje subor 'meta.php', nacita sa jeho obsah
  else exit ("<h1>V koreňovom adr. chýba súbor \"meta.php\"!</h1>"); //Inak sa vypise chyba a ukonci kod
?>
    <link href="../favicon.ico" rel="shortcut icon">
    <link media="screen" type="text/css" href="../css/admin.css" rel="stylesheet">
<?php
  echo ("<title>Administrácia - ");
  if (isset ($ID) && $ID) echo ("Vytlačenie objednávky číslo &quot;" . $ID . "&quot;");
  else echo ("CHYBA! - Nebolo určené id objednávky");
  echo (" | " . $nastavenia['titulka_stranok_' . $lang] . "</title>\n");
?>
  </head>
  <body class="tlac_faktury">
<?php
  if ($_SESSION["Admin"] == "TRUE") {
    if (isset ($ID) && $ID) { //Ak je urcene id objednavky, zobrazi sa

      //Nacitanie informacii o objednavke
      $sql = "SELECT * FROM objednavky WHERE id=" . $ID;
      $r = mysql_query ($sql);
      $Obj = mysql_fetch_array ($r);

      $sprava = "<table cellspacing='0' style='width: 100%; font-size: 0.8em;'>
  <tr>
    <td><b>" . $itts_nazov . "</b></td>
    <td align='right'>
      <b>" . $z_objednavka[$lang] . " " . $z_cislo[$lang] . " <big>" . $ID . "</big></b>
    </td>
  </tr>
</table>
<table border='1' cellspacing='0' style='margin: 0.25em auto; width: 100%; border-collapse: collapse; border: 1px solid #000; font-size: 0.8em;'>
  <tr>
    <td valign='top' style='padding: 0.25em' width='50%'>
      <font color='#0000ff'>
        <i>" . $z_dodavatel[$lang] . ":</i>
      </font><br />
      <br />
      <img src='../images/main/itts_foods_logo.gif' height='49' width='150' alt='" . $itts_nazov . "' title='" . $itts_nazov . "' />
      <div style='font-weight: bold'>\n" . $itts_adresa . "\n</div>
      <p style='color: #0000ff'>\n";
      $sprava .= $z_ico[$lang] . ": " . $itts_ico . "<br />\n";
      $sprava .= $z_dic[$lang] . ": " . $itts_dic . "<br />\n";
      $sprava .= $z_ic_dph[$lang] . ": SK" . $itts_dic . "\n</p>
<p style='font-weight: bold'>\n";
      $sprava .= $z_telefon[$lang] . ": " . $itts_telefon . "<br />\n";
      $sprava .= $z_fax[$lang] . ": " . $itts_fax ."<br />\n";
      $sprava .= $z_MobilnyTelefon[$lang] . ":<br />" . $itts_mobil_domi . "<br />" . $itts_mobil_quido . "<br />\n";
      $sprava .= $z_email[$lang] . ": " . $itts_email . "\n</p>
<p>\n";
      $sprava .= $z_CisloUctu[$lang] . ": <big><b>" . $itts_cislo_uctu . "</b></big><br />\n";
      $sprava .= "IBAN: <big><b>" . $itts_iban . "</b></big><br />\n";
      $sprava .= $z_VariabSymbol[$lang] . ": <big><b>" . $ID . "</b></big><br />\n";
      $sprava .= $z_KonstSymbol[$lang] . ": <big><b>" . $itts_konst_symb . "</b></big>
</p>
<p>\n";
      $sprava .= $z_DatumVytObj[$lang] . ": <b>" . $Obj['datum'] . "</b><br />\n";
      $sprava .= $z_CasVytObj[$lang] . ": <b>" . $Obj['cas'] . "</b>
  </p>
</td>
<td valign='top' style='padding: 0.25em; width: 50%;'>
  <font color='#0000ff'>
    <i>" . $z_odberatel[$lang] . ":</i>
  </font><br />
  <br />
  <table width='100%' border='0' cellpadding='0' cellspacing='2' style='border: 1px solid #000000; font-size: 10pt;'>
    <tr>
      <td colspan='2'>
        <hr style='width: 95%; border: 1px solid;'>
      </td>
    </tr>
    <tr>
      <td align='right'><i><b>" . $z_firma[$lang] . ":</b></i></td>
      <td>" . $Obj['firma'] . "</td>
    </tr>
    <tr>
      <td align='right'><i><b>" . $z_meno[$lang] . ":</b></i></td>
      <td>" . $Obj['meno'] . "</td>
    </tr>
    <tr>
      <td align='right'><i><b>" . $z_ico[$lang] . ":</b></i></td>
      <td>" . $Obj['ico'] . "</td>
    </tr>
    <tr>
      <td align='right'><i><b>" . $z_UlACisDomu[$lang] . ":</b></i></td>
      <td>" . $Obj['ulica'] . "&nbsp;" . $Obj['c_domu'] . "</td>
    </tr>
    <tr>
      <td align='right'><i><b>" . $z_mesto[$lang] . ":</b></i></td>
      <td>" . $Obj['mesto'] . "</td>
    </tr>
    <tr>
      <td align='right'><i><b>" . $z_psc[$lang] . ":</b></i></td>
      <td>" . $Obj['psc'] . "</td>
    </tr>
    <tr>
      <td align='right'><i><b>" . $z_krajina[$lang] . ":</b></i></td>
      <td>";
      if ($Obj['krajina'] == "sk") $sprava .= $z_slovensko[$lang];
      if ($Obj['krajina'] == "bg") $sprava .= $z_bulharsko[$lang];
      $sprava .= "</td>
      </tr>
      <tr>
        <td colspan='2'>
          <hr style='width: 95%; border: 1px solid;'>
        </td>
      </tr>
      <tr>
        <td align='right'><i><b>" . $z_telefon[$lang] . ":</b></i></td>
        <td>" . $Obj['telefon'] . "</td>
      </tr>
      <tr>
        <td align='right'><i><b>" . $z_fax[$lang] . ":</b></i></td>
        <td>" . $Obj['fax'] . "</td>
      </tr>
      <tr>
        <td align='right'><i><b>" . $z_MobilnyTelefon[$lang] . ":</b></i></td>
        <td>" . $Obj['mobil'] . "</td>
      </tr>
      <tr>
        <td align='right'><i><b>" . $z_email[$lang] . ":</b></i></td>
        <td>" . $Obj['email'] . "</td>
      </tr>
      <tr>
        <td align='right'><i><b>www:</b></i></td>
        <td>" . $Obj['web'] . "</td>
      </tr>
      <tr>
        <td align='right' valign='top'><i><b>" . $z_poznamka[$lang] . ":</b></i></td>
        <td>\n" . nl2br ($Obj['poznamka']) . "\n</td>
      </tr>
      <tr>
        <td colspan='2'>
          <hr style='width: 95%; border: 1px solid;'>
        </td>
      </tr>
    </table>
  </td>
</tr>
<tr>
  <td valign='top' colspan='2' style='padding: 0.25em'>
    <table border='1' cellpadding='2' cellspacing='2' style='width: 100%;
  border-collapse: collapse;
  border: 1px solid #000;
  font-size: 0.85em;'>
      <tr>
        <th>" . $z_c[$lang] . "</th>
        <th>" . $z_kod[$lang] . "</th>
        <th>" . $z_nazov[$lang] . "</th>
        <th>" . $z_cena[$lang] . " " . $z_za[$lang] . " " . $z_m_j[$lang] . "</th>
        <th>" . $z_mnozstvo[$lang] . "</th>
        <th>" . $z_spolu[$lang] . "</th>
      </tr>";

      //Vytvori pole z udaju v databaze
      $ceny = explode (",", $Obj['ceny']);
      $kody = explode (",", $Obj['kody']);
      $kusy = explode (",", $Obj['kusy']);

      $pocet = 1; //Docasna premenna pre ocislovanie jedn. riadkov tovaru v tabulke
      $celkovo = 0; //Cena k uhrade
      $z = 0; //Urci docasnu premennu ako pocitadlo kluca v poli

      foreach ($kody as $kluc) {
        $sql = "SELECT * FROM tovar WHERE id=" . $kluc;
        $r = mysql_query ($sql);
        $tovar = mysql_fetch_array ($r);

        $sprava .= "<tr>
  <td align='right' valign='top'>" . $pocet . "</td>
  <td valign='top' align='center'>" . $tovar['kod'] . "</td>
  <td valign='top'>";

        if ($tovar['akcia']) $sprava .= "*" . $z_AKCIA[$lang] . "* "; //Ak je tovar v akcii

        $sprava .= $tovar['nazov_' . $lang] . "</td>\n";

        $cena_sk = round ($ceny[$z] * $konv_kurz, 2); //Vypocita sa cena v Sk vynasobenim konv. kurzom a zaokruhli sa na dve des. miesta

        $sprava .= "<td align='right' valign='top' style='white-space: nowrap'>
  <b>" . $ceny[$z] . "</b> &euro;";
        if (isset ($zobraz_dual)) $sprava .= "<br />" . $cena_sk . " Sk";
        $sprava .= "\n</td>
<td align='right' valign='top'>" . $kusy[$z] . "</td>\n";

        $spolu = $ceny[$z] * $kusy[$z]; //Vypocita cenu spolu za vsetok tovar v akt. riadku
        $spolu_sk = round ($spolu * $konv_kurz, 2); //Vypocita a zaokruhli cenu v Sk

        $sprava .= "<td align='right' valign='top' style='white-space: nowrap'>
  <b>" . $spolu . "</b> &euro;";
        if (isset ($zobraz_dual)) $sprava .= "<br />" . $spolu_sk . " Sk";
        $sprava .= "\n</td>
</tr>\n";

        $pocet++;
        $z++;
        $celkovo += $spolu;
        unset ($tovar);
      }

      $sprava .= "<tr>
  <td align='right'>" . $pocet . "</td>
  <td colspan='4'>" . $z_Dovoz[$lang] . " - " . $Obj['dovoz'] . "</td>
  <td align='right' style='white-space: nowrap'>
    <b>" . $Obj['poplatok'] . "</b> &euro;";
      if (isset ($zobraz_dual)) $sprava .= "<br />" . round ($Obj['poplatok'] * $konv_kurz, 2) . " Sk";
      $sprava .= "\n</td>
</tr>\n";

      $celkovo += $Obj['poplatok']; //K celkovej sume sa prirata poplatok za rozvoz
      $celkovo_sk = round ($celkovo * $konv_kurz, 2); //Vypocita sa cena v Sk

      $sprava .= "<tr>
  <th colspan='5' align='left'>" . $z_CenaKUhrade[$lang] . "</th>
  <th align='right' style='white-space: nowrap'>\n" . round ($celkovo, 2) . " &euro;";
      if (isset ($zobraz_dual)) $sprava .= "<br />" . $celkovo_sk . " Sk";
      $sprava .= "\n</th>
  </tr>
</table>\n";

      //Ak je zapnute dualne zobrazovanie, vypise sa text o pouzitom konverznom kurze
      if (isset ($zobraz_dual)) $sprava .= "<p align='center'>" . $z_text0038[$lang] . " 1 &euro; = " . $konv_kurz . " Sk</p>\n";

      $sprava .= "<p style='text-align: center'>\n" . $z_text0016[$lang] . "</p>
      <p style='font-weight: bolder; text-align: center;'>" . $z_text0023[$lang] . " " . $itts_nazov . "</p>
      <p style='color: #0000ff; text-align: center;'>\n" . $z_text0017[$lang] . " " . $itts_http . ".</p>
      <h1 align='center'>&copy; 2009 - 2010 " . $itts_nazov . "</h1>
    </td>
  </tr>
</table>\n";

      echo ($sprava);
      unset ($Obj);
    }
    else { //Ak nie je urcene id objednavky, zobrazi sa upozornenie
      echo ("<h1 class='vystraha'>CHYBA! - Nebolo určené id objednávky!</h1>\n");
    }
  }
  else { //Nepovoleny vstup na stranku
    echo ("<h1 class='vystraha'>Nepovolený vstup na administrátorskú stránku!</h1>
<p style='text-align: center'>
  Vstup na túto stránku je povolený iba <b>administrátorom</b>! Prosím zatvorte toto okno. Ďakujem.
</p>\n");
  }

  unset ($nastavenia);
  mysql_close ($link);
?>