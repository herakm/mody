/*
(C) 2012 Mario Herak - GUNSOFT
Autor: Mario Herak
www.gunsoft.sk, gunsoft@gunsoft.sk
*/

function VymazHodnotu (FormMeno, FormPole) {
	document.forms[FormMeno].elements[FormPole].value = "";
}


function VyprazdniTabulku(odkaz, tabulka) {
	var otazka = window.confirm ("Naozaj chcete vyprázdniť tabuľku '" + tabulka + "' ?");
	
	if (otazka) {
		if (typeof(odkaz.href) != 'undefined') {
		  odkaz.href += '?action=vyprazdni_tabulku&tabulka=' + tabulka;
		}
	}
	
	return otazka;
}


function VymazRiadok(odkaz, RiadokID) {
	var otazka = window.confirm("Naozaj chcete vymazať riadok s id '" + RiadokID + "' ?");
	
	if (otazka) {
		if (typeof(odkaz.href) != 'undefined') {
			odkaz.href += '&delete=1';
		}
	}
	
	return otazka;
}


function OznacVsetky( container_id ) {
	var rows = document.getElementById(container_id).getElementsByTagName('tr');
	var unique_id;
	var checkbox;
	
	for (var i = 0; i < rows.length; i++) {
		checkbox = rows[i].getElementsByTagName( 'input' )[0];
		
		if (checkbox && checkbox.type == 'checkbox') {
			unique_id = checkbox.name + checkbox.value;
			
			if (checkbox.disabled == false) checkbox.checked = true;
		}
	}
	
	return true;
}


function OdznacVsetky( container_id ) {
	var rows = document.getElementById(container_id).getElementsByTagName('tr');
	var unique_id;
	var checkbox;
	
	for (var i = 0; i < rows.length; i++) {
		checkbox = rows[i].getElementsByTagName( 'input' )[0];
		
		if (checkbox && checkbox.type == 'checkbox') {
			unique_id = checkbox.name + checkbox.value;
			checkbox.checked = false;
		}
	}
	
	return true;
}


function ZmenAkciu (pridavok) {
	post.action += "#riadok_" + pridavok;
	post.submit();
}


// Overovanie spravnosti vlozenych hodnot niektorych prvkov formulara pred odoslanim na server
function OverFormular() {
	if (document.post.email && document.post.email.value.indexOf("@") == -1) { // Overi, ci hodnota form. prvku e-mail obsahuje znak @
		window.alert("Nezadali ste správny formát e-Mailovej adresy!");
		
		return false;
	}
	else return true;
}


function getModal(url, id) {
	$.get(url, { id: id },
		function(data){
			$('#basic-modal-content').html(data);
		}
	);
}


$(document).ready(function(){
	$(".ajax").change(function() {
		// Zisti nazov stlpca tabulky ktory sa bude porovnavat
		var colName = $(this).attr("_colname");
		
		// Zisti nazov tabulky z ktorej sa budu tahat udaje
		var tableName = $(this).attr("_table");
		
		// Zisti id zvolene uzivatelom
		var itemID = $(this).val();
		
		// Zisti id html elementu ktoremu sa priradi vysledok
		var nextElem = $(this).attr("_next");
		
		$.ajax({
			type:	"GET",
			url:	"show_table_items.php",
			data:	{ table: tableName, colname : colName, id: itemID }
		}).done(function(data) {
			$("#" + nextElem).html(data).find("option")[0].attr("selected", "selected");
			$("#" + nextElem).trigger("change");
		});
	});
}); 