<?php
/*
(C) 2009 - 2012 Mario Herak - GUNSOFT
Autor: Mario Herak
www.gunsoft.sk, gunsoft@gunsoft.sk
*/


// Nacita hodnotu tlacidla "Odoslat"
if (isset($_POST['odoslat'])) {
	$odoslat = $_POST['odoslat'];
}


echo "<h1 class='hlavicka'>Oprava riadku(ov) v tabuľke &quot;", $tabulka, "&quot;</h1>\n\n";


// Nacita komentare jedn. stlpcov tabulky
$komentare = array();
$i = 0;

$sql = "SHOW FULL COLUMNS FROM `" . $tabulka . "`";
$r = mysql_query($sql);
while ($comment = mysql_fetch_array($r)) {
	$komentare[$i] = $comment['Comment'];
	$i++;
}
unset ($comment);


$pocet_stlpcov = 0;
$name = "";
$type = "";

// Nacita nazvy stlpcov v tabulke
$sql = "SELECT * FROM `" . $tabulka . "`";
$r = mysql_query($sql);
while ($pocet_stlpcov < mysql_num_fields($r)) {
	$meta = mysql_fetch_field($r, $pocet_stlpcov);
	$name .= $meta->name . ",";
	$type .= $meta->type . ",";
	
//	echo "<pre>
//blob:			$meta->blob
//max_length:		$meta->max_length
//multiple_key:	$meta->multiple_key
//name:			$meta->name
//not_null:		$meta->not_null
//numeric:		$meta->numeric
//primary_key:	$meta->primary_key
//table:			$meta->table
//type:			$meta->type
//default:		$meta->def
//unique_key:		$meta->unique_key
//unsigned:		$meta->unsigned
//zerofill:		$meta->zerofill
//</pre>";
	
	$pocet_stlpcov++;
}

$mena_stlpcov = split(",", $name);
$typy_stlpcov = split(",", $type);


// Ak uzivatel stlacil tlacidlo "Opravit", opravi(a) sa riadok(ky) v tabulke
if (isset($odoslat)) {
	// Nacita vlozene udaje z formularu
	for ($i = 0; $i < $pocet_stlpcov; $i++) {
		if (isset($_POST[$mena_stlpcov[$i]])) {
			$form[$mena_stlpcov[$i]] = $_POST[$mena_stlpcov[$i]];
		}
	}
	
	// Zacne listovat vo vybranych riadkoch tabulky
	foreach ($riadok as $kluc => $hodnota) {
		// Vytvorenie SQL dotazu - zaciatok
		$sql = "UPDATE `" . $tabulka . "` SET ";
		
		// Zacne listovat vo vsetkych stlpcoch daneho riadku
		for ($i = 1; $i < $pocet_stlpcov; $i++) {
			// Ak nie je zapnute magic quotes gpc, prida lomitka pred znaky ('), ("), (\) a NUL (NULL byte)
			if (!get_magic_quotes_gpc()) {
				$sql .= "`" . $mena_stlpcov[$i] . "`='" . addslashes($form[$mena_stlpcov[$i]][$kluc]) . "'";
			}
			else {
				$sql .= "`" . $mena_stlpcov[$i] . "`='" . $form[$mena_stlpcov[$i]][$kluc] . "'";
			}
			
			// Ak to nie je posledny stlpec tabulky, prida sa do MySQL dotazu ciarka pre dalsi stlpec
			if ($i < ($pocet_stlpcov - 1)) {
				$sql .= ", ";
			}
		}
		
		// urci sa, ze MySQL dotaz plati len pre riadok s id podla premennej $kluc danej tabulky
		$sql .= " WHERE id='" . $kluc . "'";
		$res = @mysql_query($sql);
		
		// Ak sa podarilo vlozit udaje do tabulky, vypise SQL kod
		if ($res) {
			echo "<h3 class='sql'>SQL kód:</h3>
			
<p class='sql_kod'>", htmlspecialchars($sql), "</p>

<h3 class='oznamenie'>Riadok &quot;", $kluc, "&quot; v tabuľke &quot;", $tabulka, "&quot; bol úspešne opravený.</h3>\n\n";
		}
		else {
			//Ak sa nepodarilo vlozit udaje do tabulky, vypise sa chyba
			echo "<h3 class='sql'>SQL kód:</h3>
			
<p class='sql_kod'>", htmlspecialchars($sql), "</p>

<h3 class='vystraha'>Pri oprave údajov riadku &quot;", $kluc, "&quot; v tabuľke &quot;", $tabulka, "&quot; nastala chyba!</h3>

<p class='center'>Prosím, kontaktujte administrátora. Ďakujem.</p>\n\n";
		}
	}
}
else {
	// Ak este uzivatel nestlacil tlacidlo "Opravit", zobrazia sa udaje vybraneho(nych) riadka(kov)
	echo "<form id='post' name='post' action='browse.php?table=", $tabulka;
	if (isset($oprav)) echo "&amp;id=", $rID, "&amp;edit=1";
	echo "' method='post'>\n\n";
	
	if (isset($OpravViac)) {
		foreach ($riadok as $kluc => $hodnota) {
			echo "<input type='hidden' name='riadok[", $kluc, "]' value='", $hodnota, "'>
<input type='hidden' name='OpravViac_x' value='", $OpravViac, "'>\n";
		}
	}
	
	echo "<table align='center' cellspacing='2'>\n\n";
	
	foreach ($riadok as $kluc => $hodnota) {
		$tbl_sql = "SELECT * FROM `" . $tabulka . "` WHERE id='" . $kluc . "'";
		$tbl_r = mysql_query($tbl_sql);
		$tbl_zaznam = mysql_fetch_array($tbl_r);
		
		echo "<!-- Zaciatok riadku ", $kluc, " tabulky ", $tabulka, " -->
<tr>
	<td colspan='2'>
		<a id='riadok_", $kluc, "'></a>
		<h2 class='hlavicka'>Riadok ", $kluc, "</h2>
	</td>
</tr>\n\n";
		
		for ($i = 0; $i < $pocet_stlpcov; $i++) {
			echo "<tr";
			if ($komentare[$i]) echo " title='", $komentare[$i], "'";
			echo ">
	<td class='right top'>\n";
			if ($komentare[$i]) echo "<span class='column_comment' title='", $komentare[$i], "'>\n";
			echo "<strong>", $mena_stlpcov[$i], ":</strong>\n";
			if ($komentare[$i]) echo "</span>\n";
			echo "</td>
	<td>";
			
			/* DEFINOVANIE TYPOV STLPCOV - ZACIATOK */
			
			// Ak je stlpec typu string
			if ($typy_stlpcov[$i] == "string") {
				if ($mena_stlpcov[$i] != "extrnal_window"
					&& $mena_stlpcov[$i] != "header_pic"
					&& $mena_stlpcov[$i] != "teaser_pic"
					&& $mena_stlpcov[$i] != "opravil"
					&& $mena_stlpcov[$i] != "submenu_pic") {
					echo "<input type='text' name='", $mena_stlpcov[$i], "[", $kluc, "]'";
					if ($tabulka == "html_stranky" && $mena_stlpcov[$i] == "titulka") $titulka = $tbl_zaznam[$mena_stlpcov[$i]];
					echo " value='", $tbl_zaznam[$mena_stlpcov[$i]], "' size='50' maxlength='255'>";
				}
			}
			
			// Ak je stlpec typu text
			if ($typy_stlpcov[$i] == "blob") {
				echo "\n<textarea name='", $mena_stlpcov[$i], "[", $kluc, "]' rows='10' cols='80'";
				
				if ($mena_stlpcov[$i] != "meta_copyright"
					&& $mena_stlpcov[$i] != "meta_desc"
					&& $mena_stlpcov[$i] != "meta_keywords"
					&& $mena_stlpcov[$i] != "teaser"
					&& $mena_stlpcov[$i] != "err404"
					&& $mena_stlpcov[$i] != "plus"
					&& $mena_stlpcov[$i] != "minus"
					&& $mena_stlpcov[$i] != "result"
					&& $mena_stlpcov[$i] != "links") {
					echo " class='mceEditor'";
				}
				
				/*
				Convert all applicable characters to HTML entities
				ENT_QUOTES Will convert both double and single quotes.
				Optional third argument charset defines character set used in conversion.
				*/
				$vystup = htmlentities($tbl_zaznam[$mena_stlpcov[$i]], ENT_QUOTES, "UTF-8");
				
				echo ">", $vystup, "</textarea>\n";
				
				if ($mena_stlpcov[$i] != "meta_copyright"
					&& $mena_stlpcov[$i] != "meta_desc"
					&& $mena_stlpcov[$i] != "meta_keywords"
					&& $mena_stlpcov[$i] != "teaser"
					&& $mena_stlpcov[$i] != "err404"
					&& $mena_stlpcov[$i] != "plus"
					&& $mena_stlpcov[$i] != "minus"
					&& $mena_stlpcov[$i] != "result"
					&& $mena_stlpcov[$i] != "links") {
					echo "<br>
<a href=\"javascript:toggleEditor('", $mena_stlpcov[$i], "[", $kluc, "]');\">Ukáž/Schovaj editor</a>\n";
				}
			}
			
			// Ak je stlpec typu cislo
			if ($typy_stlpcov[$i] == "int" || $typy_stlpcov[$i] == "real") {
				if ($mena_stlpcov[$i] != "id"
					&& $mena_stlpcov[$i] != "admin"
					&& $mena_stlpcov[$i] != "published"
					&& $mena_stlpcov[$i] != "publish"
					&& $mena_stlpcov[$i] != "language_id"
					&& $mena_stlpcov[$i] != "author_id"
					&& $mena_stlpcov[$i] != "continent_id"
					&& $mena_stlpcov[$i] != "menu_id"
					&& $mena_stlpcov[$i] != "use_rss"
					&& $mena_stlpcov[$i] != "def_lang_id"
					&& $mena_stlpcov[$i] != "submenu_id"
					&& $mena_stlpcov[$i] != "submenu_item"
					&& $mena_stlpcov[$i] != "is_video"
					&& $mena_stlpcov[$i] != "is_external"
					&& $mena_stlpcov[$i] != "model_id"
					&& $mena_stlpcov[$i] != "times_dwnl"
					&& $mena_stlpcov[$i] != "users_rating"
					&& $mena_stlpcov[$i] != "rating"
					&& $mena_stlpcov[$i] != "grp1"
					&& $mena_stlpcov[$i] != "grp2"
					&& $mena_stlpcov[$i] != "grp3"
					&& $mena_stlpcov[$i] != "grp4"
					&& $mena_stlpcov[$i] != "viewed"
					&& $mena_stlpcov[$i] != "item_id") {
					echo "<input type='text' name='", $mena_stlpcov[$i], "[", $kluc, "]'
					value='", $tbl_zaznam[$mena_stlpcov[$i]], "' size='20' maxlength='30'>";
				}
			}
			
			// Ak je stlpec typu datum alebo cas
			if ($typy_stlpcov[$i] == "date" || $typy_stlpcov[$i] == "time") {
				echo "<input type='text' name='", $mena_stlpcov[$i], "[", $kluc, "]'
				value='", $tbl_zaznam[$mena_stlpcov[$i]], "' size='10' readonly='readonly'>";
			}
			
			/* DEFINOVANIE TYPOV STLPCOV - KONIEC */
			
			
			/* DEFINOVANIE SPECIALNYCH STLPCOV - ZACIATOK */
			
			// Ak sa stlpec vola "continent_id"
			if ($mena_stlpcov[$i] == "continent_id") {
				echo "\n<select size='1' name='", $mena_stlpcov[$i], "[", $kluc, "]'>\n";
				
				$sql = "SELECT * FROM `continents` ORDER BY `continent`";
				$r = mysql_query($sql);
				while ($cont = mysql_fetch_array($r)) {
					echo "<option value='", $cont['id'], "'";
					if ($tbl_zaznam[$mena_stlpcov[$i]] == $cont['id']) echo " selected='selected'";
					echo ">", $cont['continent'], "</option>\n";
				}
				unset ($cont);
				
				echo "</select>\n";
			}
			
			// Ak sa stlpec vola "id", "times_dwnl", "users_rating"
			if ($mena_stlpcov[$i] == "id"
				|| $mena_stlpcov[$i] == "times_dwnl"
				|| $mena_stlpcov[$i] == "users_rating"
				|| $mena_stlpcov[$i] == "viewed") {
				echo "<input type='text' name='", $mena_stlpcov[$i], "[", $kluc, "]'
				value='", $tbl_zaznam[$mena_stlpcov[$i]], "' size='10' readonly='readonly'>";
			}
			
			// Ak sa stlpec vola 'extrnal_window'
			if ($mena_stlpcov[$i] == "extrnal_window") {
				echo "\n<select size='1' name='", $mena_stlpcov[$i], "[", $kluc, "]'>
	<option value=''";
				if (!$tbl_zaznam[$mena_stlpcov[$i]]) echo " selected='selected'";
				echo ">Vyberte z možností</option>
	<option value='_self'";
				if ($tbl_zaznam[$mena_stlpcov[$i]] == "_self") echo " selected='selected'";
				echo ">_self (same window)</option>
	<option value='_blank'";
				if ($tbl_zaznam[$mena_stlpcov[$i]] == "_blank") echo " selected='selected'";
				echo ">_blank (new window)</option>
</select>\n";
			}
			
			// Ak sa stlpec vola "admin", "published", ...
			if ($mena_stlpcov[$i] == "admin"
				|| $mena_stlpcov[$i] == "published"
				|| $mena_stlpcov[$i] == "publish"
				|| $mena_stlpcov[$i] == "use_rss"
				|| $mena_stlpcov[$i] == "is_external"
				|| $mena_stlpcov[$i] == "is_video"
				|| $mena_stlpcov[$i] == "submenu_item") {
				echo "\n<select size='1' name='", $mena_stlpcov[$i], "[", $kluc, "]'>
	<option value='1'";
				if ($tbl_zaznam[$mena_stlpcov[$i]] == 1) echo " selected='selected'";
				echo ">Áno</option>
	<option value='0'";
				if ($tbl_zaznam[$mena_stlpcov[$i]] == 0) echo " selected='selected'";
				echo ">Nie</option>
</select>\n";
			}
			
			// Ak sa stlpec vola 'language_id', 'def_lang_id'
			if ($mena_stlpcov[$i] == "language_id" || $mena_stlpcov[$i] == "def_lang_id") {
				echo "\n<select size='1' name='", $mena_stlpcov[$i], "[", $kluc, "]'>
	<option value=''";
				if (!$tbl_zaznam[$mena_stlpcov[$i]] || $tbl_zaznam[$mena_stlpcov[$i]] == "0") echo " selected='selected'";
				echo ">- Default -</option>\n";
				
				$sql = "SELECT * FROM `languages` ORDER BY `language`";
				$r = mysql_query($sql);
				while ($lang = mysql_fetch_array($r)) {
					echo "<option value='", $lang['id'], "'";
					if ($tbl_zaznam[$mena_stlpcov[$i]] == $lang['id']) echo " selected='selected'";
					echo ">", $lang['language'], "</option>\n";
				}
				unset ($lang);
				
				echo "</select>\n";
			}
			
			// Ak sa stlpec vola "author_id"
			if ($mena_stlpcov[$i] == "author_id") {
				$ha = FillTableArray("users", "id", $_SESSION["UserID"]);
				
				echo "\n<input type='hidden' name='", $mena_stlpcov[$i], "[", $kluc, "]'
				value='", $_SESSION["UserID"], "'>
<input type='text' value='";
				if ($ha['first_name']) echo $ha['first_name'];
				if ($ha['first_name'] && $ha['surename']) echo " ";
				if ($ha['surename']) echo $ha['surename'];
				echo "' size='50' maxlength='255' readonly='readonly'>\n";
				
				unset($ha);
			}
			
			// Ak sa stlpec vola "menu_id"
			if ($mena_stlpcov[$i] == "menu_id") {
				echo "\n<select size='1' name='", $mena_stlpcov[$i], "[", $kluc, "]'>
	<option value=''";
				if (!$tbl_zaznam[$mena_stlpcov[$i]]) {
					echo " selected='selected'";
				}
				echo ">Vyberte z možností</option>\n";
				
				$sql = "SELECT * FROM `menu` ORDER BY `order`";
				$r = mysql_query($sql);
				while ($menu = mysql_fetch_array($r)) {
					echo "<option value='", $menu['id'], "'";
					if ($tbl_zaznam[$mena_stlpcov[$i]] == $menu['id']) {
						echo " selected='selected'";
					}
					if ($menu['title']) {
						echo " title='", $menu['title'], "'";
					}
					echo ">", $menu['item'], "</option>\n";
				}
				unset ($menu);
				
				echo "</select>\n";
			}
			
			
			// Ak sa stlpec vola "submenu_id"
			if ($mena_stlpcov[$i] == "submenu_id") {
				echo "\n<select size='1' name='", $mena_stlpcov[$i], "[", $kluc, "]'>
	<option value=''";
				if (!$tbl_zaznam[$mena_stlpcov[$i]]) {
					echo " selected='selected'";
				}
				echo ">Vyberte z možností</option>\n";
				
				$sql = "SELECT * FROM `submenu` WHERE `published`='1' ORDER BY `item_id`, `order`";
				$r = mysql_query($sql);
				while ($submenu = mysql_fetch_array($r)) {
					$ha = FillTableArray("menu", "id", $submenu["item_id"]);
					
					echo "<option value='", $submenu['id'], "' title='", $submenu['title'], "'";
					if ($tbl_zaznam[$mena_stlpcov[$i]] == $submenu['id']) {
						echo " selected='selected'";
					}
					echo ">[", $ha['item'], "] - ", $submenu['item'], "</option>\n";
					
					unset($ha);
				}
				unset ($submenu);
				
				echo "</select>\n";
			}
			
			
			// Columns named 'header_pic', 'teaser_pic', 'submenu_pic'
			if ($mena_stlpcov[$i] == "header_pic"
				|| $mena_stlpcov[$i] == "teaser_pic"
				|| $mena_stlpcov[$i] == "submenu_pic") {
				echo "<div class='thumb_view'>
	<div class='thumb_frame'>
		<div class='thumb_pic'>
			<img src='images/no-image.gif' width='128' height='128' alt='NO PICTURE'>
		</div>
		
		<p class='thumb_desc'>
			<input type='radio'";
				if (!$tbl_zaznam[$mena_stlpcov[$i]]) {
					echo " checked='checked'";
				}
				echo " name='", $mena_stlpcov[$i], "[", $kluc, "]' value=''>
		</p>
	</div>\n\n";
				
				// Nastavi docasnu premennu oznacujucu ci sa nasiel medzi uploadnutymi subormi subor obrazku
				$image_file_found = FALSE;
				
				$sql = "SELECT `file`, `desc`, `thumb_name` FROM `files` WHERE `type` LIKE 'image%' AND `published` = 1 ORDER BY `file` ASC";
				$r = mysql_query($sql);
				while ($files = mysql_fetch_array($r)) {
					// Ziskanie info o nahl. obr.
					$thumb_filename = "../upload/thumbs/" . $files['thumb_name'];
					$thumb_info = getimagesize($thumb_filename); // Zisti info o nahl. obr.
					$thumb_width = $thumb_info[0]; // Zisti sirku nahl. obr.
					$thumb_height = $thumb_info[1]; // Zisti vysku nahl. obr.
					// $thumb_info[3] = directly image width and height
					
					// Vycentrovanie nahl. obr.
					$thumb_marg_left = (128 - $thumb_width) / 2; // Urci odsadenie nahl. obr. zlava
					$thumb_marg_top = (128 - $thumb_height) / 2; // Urci odsadenie nahl. obr. zlava
					
					// Ziskanie info o obr.
					$img_filename = "../upload/" . $files['file'];
					$img_info = getimagesize($img_filename); // Zisti info o obr.
					$img_width = $img_info[0]; // Zisti sirku obr.
					$img_height = $img_info[1]; // Zisti vysku obr.
					
					echo "<div class='thumb_frame'>
	<div class='thumb_pic'>
		<a href='", $img_filename, "' title='", $files['file'], "' class='lightbox'>
			<img src='", $thumb_filename, "' ", $thumb_info[3], " alt='", $files['thumb_name'], "'
			style='margin-top: ", $thumb_marg_top, "px; margin-left: ", $thumb_marg_left, "px;'>
		</a>
	</div>
	<p class='thumb_desc'>
		<input type='radio'";
					if ($tbl_zaznam[$mena_stlpcov[$i]] == "/upload/" . $files['file']) {
						// Medzi uploadnutymi subormi sa nasiel subor obrazku
						$image_file_found = TRUE;
						
						echo " checked='checked'";
					}
					echo " name='", $mena_stlpcov[$i], "[", $kluc, "]' value='/upload/", $files['file'], "'> ",
						$img_width, "x", $img_height, " - ", $files['file'];
					if ($files['desc']) {
						echo " - ", $files['desc'];
					}
					echo "</p>
</div>\n\n";
				}
				unset ($files);
				
				if ($tbl_zaznam[$mena_stlpcov[$i]] && !$image_file_found) {
					// Ziskanie info o obr.
					$img_filename = $tbl_zaznam[$mena_stlpcov[$i]];
					$img_info = getimagesize(".." . $img_filename); // Zisti info o obr.
					$img_width = $img_info[0]; // Zisti sirku obr.
					$img_height = $img_info[1]; // Zisti vysku obr.
					
					echo "<div class='thumb_frame'>
	<div class='thumb_pic'>
		<img src='images/other-image.gif' width='128' height='128' alt='OTHER PICTURE'>
	</div>
	
	<p class='thumb_desc'>
		<input type='radio' checked='checked' name='", $mena_stlpcov[$i], "[", $kluc, "]'
		value='", $img_filename, "' />\n", $img_width, "x", $img_height, " - ", $img_filename, "
	</p>
</div>\n\n";
				}
				
				echo "</div>\n";
			}
			
			// Ak sa stlpec vola 'model_id'
			if ($mena_stlpcov[$i] == "model_id") {
				echo "\n<select size='1' name='", $mena_stlpcov[$i], "[", $kluc, "]'>\n";
				
				$query = "SELECT * FROM `models` WHERE `publish`='1' ORDER BY `model_nr` ASC";
				$rows = mysql_query($query);
				while ($result = mysql_fetch_array($rows)) {
					echo "<option value='", $result['id'], "'";
					if ($tbl_zaznam[$mena_stlpcov[$i]] == $result['id']) echo " selected='selected'";
					echo ">(", $result['model_nr'], ") ", $result['name'], "</option>\n";
				}
				unset ($result);
				
				echo "</select>\n";
			}
			
			// Ak sa stlpec vola 'rating'
			if ($mena_stlpcov[$i] == "rating") {
				echo "\n<select size='1' name='", $mena_stlpcov[$i], "[", $kluc, "]'>\n";
				
				for ($z = 1; $z <= 10; $z++) {
					echo "<option value='", $z, "'";
					if ($tbl_zaznam[$mena_stlpcov[$i]] == $z) {
						echo " selected='selected'";
					}
					echo ">", $z, "</option>\n";
				}
				unset ($result);
				
				echo "</select>&nbsp;/10\n";
			}
			
			// Ak sa stlpec vola 'grp1'
			if ($mena_stlpcov[$i] == "grp1") {
				echo "\n<select class='ajax' id='", $mena_stlpcov[$i], "' size='1' name='", $mena_stlpcov[$i], "[", $kluc, "]' _colname='", $mena_stlpcov[$i], "' _table='groups_2' _next='grp2'>\n";
				
				$query = "SELECT * FROM `groups_1` WHERE `publish`='1' ORDER BY `name` ASC";
				$rows = mysql_query($query);
				while ($result = mysql_fetch_array($rows)) {
					echo "<option value='", $result['id'], "'";
					if ($tbl_zaznam[$mena_stlpcov[$i]] == $result['id']) echo " selected='selected'";
					echo ">", $result['name'], "</option>\n";
				}
				unset ($result);
				
				echo "</select>\n";
			}
			
			// Ak sa stlpec vola 'grp2'
			if ($mena_stlpcov[$i] == "grp2") {
				echo "\n<select class='ajax' id='", $mena_stlpcov[$i], "' size='1' name='", $mena_stlpcov[$i], "[", $kluc, "]' _colname='", $mena_stlpcov[$i], "' _table='groups_3' _next='grp3'>\n";
				
				$query = "SELECT * FROM `groups_2` WHERE `grp1`='" . $tbl_zaznam['grp1'] . "' AND `publish`='1' ORDER BY `name` ASC";
				$rows = mysql_query($query);
				while ($result = mysql_fetch_array($rows)) {
					echo "<option value='", $result['id'], "'";
					if ($tbl_zaznam[$mena_stlpcov[$i]] == $result['id']) echo " selected='selected'";
					echo ">", $result['name'], "</option>\n";
				}
				unset ($result);
				
				echo "</select>\n";
			}
			
			// Ak sa stlpec vola 'grp3'
			if ($mena_stlpcov[$i] == "grp3") {
				echo "\n<select class='ajax' id='", $mena_stlpcov[$i], "'size='1' name='", $mena_stlpcov[$i], "[", $kluc, "]' _colname='", $mena_stlpcov[$i], "' _table='groups_4' _next='grp4'>\n";
				
				$query = "SELECT * FROM `groups_3` WHERE `grp2`='" . $tbl_zaznam['grp2'] . "' AND `publish`='1' ORDER BY `name` ASC";
				$rows = mysql_query($query);
				while ($result = mysql_fetch_array($rows)) {
					echo "<option value='", $result['id'], "'";
					if ($tbl_zaznam[$mena_stlpcov[$i]] == $result['id']) echo " selected='selected'";
					echo ">", $result['name'], "</option>\n";
				}
				unset ($result);
				
				echo "</select>\n";
			}
			
			// Ak sa stlpec vola 'grp4'
			if ($mena_stlpcov[$i] == "grp4") {
				echo "\n<select size='1' id='", $mena_stlpcov[$i], "' name='", $mena_stlpcov[$i], "[", $kluc, "]' _colname='", $mena_stlpcov[$i], "'>\n";
				
				$query = "SELECT * FROM `groups_4` WHERE `grp3`='" . $tbl_zaznam['grp3'] . "' AND `publish`='1' ORDER BY `name` ASC";
				$rows = mysql_query($query);
				while ($result = mysql_fetch_array($rows)) {
					echo "<option value='", $result['id'], "'";
					if ($tbl_zaznam[$mena_stlpcov[$i]] == $result['id']) echo " selected='selected'";
					echo ">", $result['name'], "</option>\n";
				}
				unset ($result);
				
				echo "</select>\n";
			}
			
			// Ak sa stlpec vola 'item_id'
			if ($mena_stlpcov[$i] == "item_id") {
				echo "\n<select size='1' name='", $mena_stlpcov[$i], "[", $kluc, "]'>\n";
				
				$query = "SELECT `id`, `name` FROM `items` WHERE `publish`='1' ORDER BY `date` DESC, `time` DESC";
				$rows = mysql_query($query);
				while ($result = mysql_fetch_array($rows)) {
					echo "<option value='", $result['id'], "'";
					if ($tbl_zaznam[$mena_stlpcov[$i]] == $result['id']) echo " selected='selected'";
					echo ">", $result['name'], "</option>\n";
				}
				unset ($result);
				
				echo "</select>\n";
			}
			
			/* DEFINOVANIE SPECIALNYCH STLPCOV - KONIEC */
			
			
			echo "</td>
</tr>\n\n";
		}
		
		
		echo "<tr>
	<td colspan='2' class='center'>
		<input type='submit' name='odoslat' value='Opraviť'>
	</td>
</tr>\n";
	}
	
	
	echo "</table>
</form>\n";
}
?>