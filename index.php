<?php
/*
(C) 2008 - 2012 Mário Herák - GUNSOFT
Author: Mário Herák (herakm@gmail.com, gunsoft@gunsoft.sk)
*/


// Include functions
if (file_exists("include/functions.php")) {
	require("include/functions.php");
}
else {
	exit("V adresári [include] chýba súbor <u>functions.php</u>! / File <u>functions.php</u> is missing in directory [include]!");
}

// Include database connection
if (file_exists("include/db-connect.php")) {
	require("include/db-connect.php");
}
else {
	$error = error_title("V adresári [include] chýba súbor <u>db-connect.php</u>!", "File <u>db-connect.php</u> is missing in directory [include]!");
	exit($error);
}

/*
// Include reCaptcha library
if (file_exists("include/recaptchalib.php")) {
	require_once("include/recaptchalib.php");
	// Get a key from https://www.google.com/recaptcha/admin/create
	$publickey = "6LeYP8ESAAAAADjY3LSnxAdjfufhlzaZGyEvAYtn";
	$privatekey = "6LeYP8ESAAAAACXZetXpzpXGMM2ol0MC03-ET4iU";
}
else {
	$error = error_title("V adresári [include] chýba súbor <u>recaptchalib.php</u>!", "File <u>recaptchalib.php</u> is missing in directory [include]!");
	exit($error);
}
*/

// Include bad words ;)
if (file_exists("include/bad-words.php")) {
	require("include/bad-words.php");
}
else {
	$error = error_title("V adresári [include] chýba súbor <u>bad-words.php</u>!", "File <u>bad-words.php</u> is missing in directory [include]!");
	exit($error);
}

// Inicalization of values
// URL Values for selecting menu items
if (isset($_GET['grp1'])) $group1 = $_GET['grp1'];
if (isset($_GET['grp2'])) $group2 = $_GET['grp2'];
if (isset($_GET['grp3'])) $group3 = $_GET['grp3'];
if (isset($_GET['grp4'])) $group4 = $_GET['grp4'];

if (isset($_GET['item'])) $item = $_GET['item']; // Get selected item id from URL
if (isset($_GET['action'])) $akcia = $_GET['action']; //Get action to do from URL
if (isset($_GET['vyhladavanie'])) $vyhladavanie = $_GET['vyhladavanie'];
if (isset($_GET['kniha_hosti'])) $kniha_hosti = $_GET['kniha_hosti'];

// Form values for sending message from users to guestbook or to mod's messages
if (isset($_POST["user-nick"])) $user_nick = $_POST["user-nick"]; // Get value from input field "Prezyvka"
if (isset($_POST["email"])) $email = $_POST["email"]; // Get value from input field "E-mail"
if (isset($_POST["user-message"])) $user_message = $_POST["user-message"]; // Get value from input field "Odkaz"

// Get emoticon value from button
if (isset($_POST["emo"]) && $_POST["emo"]) {
	$emo = $_POST["emo"];
	
	// Add code to message, if there was a click on a smiley
	if (isset($emo) && $emo) {
		$user_message .= $emo;
	}
}

if (isset($_POST["valid-check"])) $validCheck = $_POST["valid-check"];
if (isset($_POST["valid-check-id"])) $validCheckId = $_POST["valid-check-id"];

// Get button "Poslat odkaz" state
if (isset($_POST["send-message"])) {
	$btn_send_message = $_POST["send-message"];
}

//$tl_fulltext = $_POST["vyhladat_fulltext"];
//if ($tl_fulltext == TRUE) $fulltext = $_POST["fulltext"];


/*
Define emoticons arrays
Format: file name (without extansion, eg. ".gif") is array key; value is title
---------------------------------------------------
--- Must be placed in this file (index.php) !!! ---
---------------------------------------------------
*/
$Emoticons = Array(
	"hanba" => array ("Hanbím sa", ":shame:"),
	"he" => array ("He", ":he:"),
	"hnev" => array ("Hnev", ":x"),
	"myslenie" => array ("Premýšlam", ":think:"),
	"nedovera" => array ("Neverím", ":no:"),
	"pohoda" => array ("Pohoda", ":cool:"),
	"sir_usm" => array ("Široký úsmev", ":D"),
	"sklamanie" => array ("Sklamanie", ":diss:"),
	"spanok" => array ("Spánok", ":sleep:"),
	"srdce" => array ("Srdce", ":heart:"),
	"ticho" => array ("Som ticho", ":silence:"),
	"usmev" => array ("Úsmev", ":smile:"),
	"vyplaz" => array ("Vyplazovanie", ":loll:"));


// Otazky pre kontrolu ci formular v knihe hosti vyplna clovek
$otazka = array("Koľko je päť plus dva?",
				"Koľko je jeden plus jeden?",
				"Koľko je štyri krát dva?",
				"Koľko je jeden krát dva?",
				"Napíšte slovo 'autobus' odzadu.",
				"Napíšte slovo 'harmonika' odzadu.",
				"Napíšte slovo 'sobota' odzadu.",
				"Napíšte slovo 'sokol' odzadu.",
				"Napíšte názov ľavého stĺpca.",
				"Napíšte názov tohto formulára.");

$odpoved = array("sedem",
				 "dva",
				 "osem",
				 "dva",
				 "subotua",
				 "akinomrah",
				 "atobos",
				 "lokos",
				 "menu",
				 "odkaz");


// Function to highlight expression
function zvyrazni($vyraz, $text) {
  $dt = strlen($text);
  $dv = strlen($vyraz);
  $vysledok = "";
  for ($z = 0; $z != $dt; $z++) {
	if (substr($text, $z, $dv) == $vyraz) {
	  $vysledok = $vysledok . "<big><strong>" . substr($text, $z, $dv) . "</strong></big>";
	  $z = $z + ($dv - 1);
	}
	else {
	  $vysledok = $vysledok . substr($text, $z, 1);
	}
  }
  return $vysledok;
}

// Censore all bad words
function cenzura($nadavky, $retazec) {
  foreach ($nadavky as $kluc => $pole) {
	foreach ($pole as $hodnota) {
	  $nahrada = " :CENZUROVANÉ: ";
	  $retazec = eregi_replace($hodnota, $nahrada, $retazec);
	}
  }
  return $retazec;
}

// Remove diacritic
function bezdiakritiky($retazec) {
  $zamena = Array (
	"a" => Array ("á", "Á", "ä", "Ä"),
	"c" => Array ("č", "Č"),
	"d" => Array ("ď", "Ď"),
	"e" => Array ("é", "É", "ě", "Ě"),
	"i" => Array ("í", "Í"),
	"l" => Array ("ľ", "Ľ", "ĺ", "Ĺ"),
	"n" => Array ("ň", "Ň"),
	"o" => Array ("ó", "Ó", "ô", "Ô"),
	"r" => Array ("ř", "Ř", "ŕ", "Ŕ"),
	"s" => Array ("š", "Š"),
	"t" => Array ("ť", "Ť"),
	"u" => Array ("ú", "Ú", "ů", "Ů"),
	"y" => Array ("ý", "Ý"),
	"z" => Array ("ž", "Ž"));

  foreach ($zamena as $kluc => $pole) {
	foreach ($pole as $hodnota) {
	  $retazec = str_replace($hodnota, $kluc, $retazec);
	}
  }
  $retazec = strtolower($retazec);
  return $retazec;
}

// Convert capitals with diacritic by lowercase with diacritic
function dia_velke_na_male($retazec) {
  $zamena = Array ( "á" => "Á", "ä" => "Ä", "č" => "Č", "ď" => "Ď", "é" => "É", "ě" => "Ě", "í" => "Í", "ľ" => "Ľ", "ĺ" => "Ĺ", "ň" => "Ň",
					"ó" => "Ó", "ô" => "Ô", "ř" => "Ř", "ŕ" => "Ŕ", "š" => "Š", "ť" => "Ť", "ú" => "Ú", "ů" => "Ů", "ý" => "Ý", "ž" => "Ž");

  foreach ($zamena as $kluc => $hodnota) {
	$retazec = str_replace($hodnota, $kluc, $retazec);
  }
  return $retazec;
}

function DoplnStiahnute() {
  $sql = "SELECT `subor` FROM `polozky` ORDER BY `id` ASC";
  $r = mysql_query($sql);
  while ($zaznam = mysql_fetch_array($r)) {
	echo ("Zisťujem počet stiahnutí pre súbor " . $zaznam["subor"] . "...<br>");
	$sql = "SELECT `id` FROM `stiahnute` WHERE `subor`='" . $zaznam["subor"] . "' AND `zobrazit`='1'";
	$r2 = mysql_query($sql);
	$pocet = mysql_num_rows($r2);
	echo ("Počet stiahnutí súbora " . $zaznam["subor"] . " je " . $pocet . "...<br>");
	$sql = "UPDATE `polozky` SET `stiahnute`='" . $pocet . "' WHERE `subor`='" . $zaznam["subor"] . "'";
	$r3 = mysql_query($sql);
	echo ("Updatujem počet stiahnutí v tabuľke <strong>polozky</strong>...<br><br>");
  }
  echo ("Počet stiahnutí súborov bol v tabulke <strong>polozky</strong> zapísaný. Funkcia skončila...<br><br>");
}

//Aktivovat funkciu len ak je treba doplnit do tabulky "polozky"  pocet stiahnuti
//DoplnStiahnute(); //Doplni v databaze do tabulky "polozky" do stlpca "stiahnute" pocty stiahnuti suboru
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width">
		
		<meta name="author" content="Mário Herák; e-mail: herakm@gmail.com">
		<meta name="copyright" content="(C) 2008 - 2011 Mário Herák GUNSOFT">
		<meta name="robots" content="index,follow">
		<meta name="description" content="Stránky venované detailnejším popisom modov do hier. Zamerané najmä na GTA: San Andreas a hernú sériu Grand Theft Auto.">
		<meta name="keywords" content="GTA, Grand Theft Auto, San Andreas, GTA: SA, mody, GUNSOFT, Mário Herák">
		
		<title>Mody</title>
		
		<link rel="icon" type="image/ico" href="favicon.ico">
		
		<link rel="stylesheet" type="text/css" href="css/reset.css">
		<link rel="stylesheet" type="text/css" href="css/screen.css">
		<link rel="stylesheet" type="text/css" href="css/responsive.css">
		<link rel="stylesheet" type="text/css" href="css/print.css" media="print">
		<link rel="stylesheet" type="text/css" media="screen" href="css/lightbox.css">
		
		<script type="text/javascript" src="js/prototype.js"></script>
		<script type="text/javascript" src="js/scriptaculous.js?load=effects,builder"></script>
		<script type="text/javascript" src="js/lightbox.js"></script>
	</head>
<?php
echo "<body";

if (isset($akcia) && $akcia == "guestbook") {
	echo " onload='usernick.focus();'";
	
	if (isset($user_message) && $user_message) {
		echo " onload='usermessage.focus();'";
	}
}

if (isset($akcia) && $akcia == "search") echo " onload='search_text.focus();'";

echo ">\n";
?>
		<div id="background">
			<img class="header_image" src="images/main/hlavicka.jpg" alt="Mody">
			
			<ul id="menu_pod_hlavickou">
<!--
        <li class="first"><a href="admin/default.php" target="_blank">Administrácia</a></li>
        <li>|</li>
-->
        <li class="first"><a href="index.php">Úvodná stránka</a></li>
        <li>|</li>
        <li><a href="index.php?action=search">Vyhľadávanie</a></li>
        <li>|</li>
        <li><a href="index.php?action=guestbook">Kniha hostí</a></li>
        <li>|</li>
        <li><a href="mailto:herakm@gmail.com?subject=Dotaz%20zo%20stranky%20Mody" target="_blank">Webmaster</a></li>
      </ul>
<!--
<div id="fulltext_vyhladavanie_pod_hlavickou">
<form method="POST" action="index.php">
<input type="text" size="20" name="fulltext" value="fulltext vyhľadávanie" class="form_fulltext">
<input type="submit" value="Vyhľadať" name="vyhladat_fulltext" class="form_fulltext_submit">
</form>
</div>
-->
      <div id="menu">
        <h1>Menu</h1>
<?php
  // Include left menu
  if (file_exists("include/left-menu.php")) require_once("include/left-menu.php");
  else {
    $error = error_title("V adresári [include] chýba súbor <u>left-menu.php</u>!", "File <u>left-menu.php</u> is missing in directory [include]!");
    exit($error);
  }
?>
        <h2>Počítadlo</h2>
        <a href="http://www.toplist.sk/" target="_blank">
          <img src="http://toplist.sk/count.asp?id=1096463&amp;logo=mc" alt="TOPlist" width="88" height="60"></a>
        <h2>Moja ikonka</h2>
        <img src="images/main/web_ikonka.jpg" alt="www.gunsoft.sk/mody" height="32" width="89">
        <h2>Priatelia</h2>
        <ul class="friends-icons">
          <li>
            <a href="http://www.trencan.xf.cz/" target="_blank">
              <img src="images/main/iko_www_trencan_xf_cz.gif" alt="www.trencan.xf.cz" height="32" width="89"></a>
          </li>
          <li>
            <a href="http://www.gred-modspage.wbs.cz/" target="_blank">
              <img src="images/main/gred.gif" alt="GRED modspage" height="31" width="88"></a>
          </li>
          <li>
            <a href="http://gred-mafiamods.wbs.cz/" target="_blank">
              <img src="images/main/gredmm.gif" alt="GREDovi mafia módy" height="31" width="84"></a>
          </li>
        </ul>
      </div>
      <div id="main">
<?php
  if (isset($item)) { // If we know the item number,
    // Include item displaying code
    if (file_exists("include/display-item.php")) require_once("include/display-item.php");
    else {
      $error = error_title("V adresári [include] chýba súbor <u>display-item.php</u>!", "File <u>display-item.php</u> is missing in directory [include]!");
      exit($error);
    }

    // Display messages from users which belongs to selected mod
    if (file_exists("include/messages.php")) require_once("include/messages.php");
    else {
      $error = error_title("V adresári [include] chýba súbor <u>messages.php</u>!", "File <u>messages.php</u> is missing in directory [include]!");
      exit($error);
    }
  }

  // Display group items according selected group(s)
  if (!isset($item) && isset($group1)) {
    $show_welcome = FALSE;

    // Load name of Group 1
    $sql = "SELECT `name` FROM `groups_1` WHERE `id`='$group1'";
    $r2 = mysql_query($sql);
    $groups_1 = mysql_fetch_array($r2);
    $Grp1_Name = $groups_1["name"]; // Select name of Group 1

    if (isset($group2)) {
      // Load name of Group 2
      $sql = "SELECT `name` FROM `groups_2` WHERE `id`='$group2'";
      $r2 = mysql_query($sql);
      $groups_2 = mysql_fetch_array($r2);
      $Grp2_Name = $groups_2["name"]; // Select name of Group 2
    }

    if (isset($group3)) {
      // Load name of Group 3
      $sql = "SELECT `name` FROM `groups_3` WHERE `id`='$group3'";
      $r2 = mysql_query($sql);
      $groups_3 = mysql_fetch_array($r2);
      $Grp3_Name = $groups_3["name"]; // Select name of Group 3
    }

    if (isset($group4)) {
      // Load name of Group 4
      $sql = "SELECT `name` FROM `groups_4` WHERE `id`='$group4'";
      $r2 = mysql_query($sql);
      $groups_4 = mysql_fetch_array($r2);
      $Grp4_Name = $groups_4["name"]; // Select name of Group 4
    }

    // Display bread crumbs
    if (isset($group1)) {
      echo("<p class='bread-crumbs'>
  <a href='index.php?grp1=$group1'>$Grp1_Name</a>\n");
      if (isset($group2)) {
        echo(" \\ <a href='index.php?grp1=$group1&amp;grp2=$group2'>$Grp2_Name</a>\n");
        if (isset($group3)) {
          echo(" \\ <a href='index.php?grp1=$group1&amp;grp2=$group2&amp;grp3=$group3'>$Grp3_Name</a>\n");
          if (isset($group4)) echo(" \\ <a href='index.php?grp1=$group1&amp;grp2=$group2&amp;grp3=$group3&amp;grp4=$group4'>$Grp4_Name</a>\n");
        }
      }
      echo("</p>\n");
    }

    // Display items from group 2
    if (isset($group1) && !isset($group2)) {
      $sql = "SELECT * FROM `groups_2` WHERE `grp1`='$group1' AND `publish`='1' ORDER BY `name` ASC";
      $r2 = mysql_query($sql);
      $grp2_count = mysql_num_rows($r2); // Find the number of Group 2 items belongs to Group 1 item
      if ($grp2_count) {
        echo("<ul class='grp4'>\n");
        while($groups_2 = mysql_fetch_array($r2)) {
          echo("<li>
  <a href='index.php?grp1=$group1&amp;grp2=$groups_2[id]'>");
          if ($groups_2["img_name"]) echo("<img src='images/groups_2/$groups_2[img_name].jpg' alt='$groups_2[name]'><br>");
          echo("$groups_2[name]</a>
</li>\n");
        }
        echo("</ul>\n");
      }
    }

    // Display items from group 3
    if (isset($group1) && isset($group2) && !isset($group3)) {
      $sql = "SELECT * FROM `groups_3` WHERE `grp2`='$group2' AND `publish`='1' ORDER BY `name` ASC";
      $r2 = mysql_query($sql);
      $grp3_count = mysql_num_rows($r2); // Find the number of Group 3 items belongs to Group 2 item
      if ($grp3_count) {
        echo("<ul class='grp4'>\n");
        while($groups_3 = mysql_fetch_array($r2)) {
          echo("<li>
  <a href='index.php?grp1=$group1&amp;grp2=$group2&amp;grp3=$groups_3[id]'>");
          if ($groups_3["img_name"]) echo("<img src='images/groups_3/$groups_3[img_name].jpg' alt='$groups_3[name]'><br>");
          echo("$groups_3[name]</a>
</li>\n");
        }
        echo("</ul>\n");
      }
    }

    // Display items from group 4
    if (isset($group1) && isset($group2) && isset($group3) && !isset($group4)) {
      $sql = "SELECT * FROM `groups_4` WHERE `grp3`='$group3' AND `publish`='1' ORDER BY `name` ASC";
      $r2 = mysql_query($sql);
      $grp4_count = mysql_num_rows($r2); // Find the number of Group 4 items belongs to Group 3 item
      if ($grp4_count > 0) {
        echo("<ul class='grp4'>\n");
        while($groups_4 = mysql_fetch_array($r2)) {
          echo("<li>
  <a href='index.php?grp1=$group1&amp;grp2=$group2&amp;grp3=$group3&amp;grp4=$groups_4[id]'>");
          if ($groups_4["img_name"]) echo("<img src='images/groups_4/" . $groups_4["img_name"] . ".jpg' alt='$groups_4[name]'><br>");
          echo("$groups_4[name]</a>
</li>\n");
        }
        echo("</ul>\n");
      }
    }

    // Display review names from items belongs to grp1 - gpr 4
    if (isset($group1) && isset($group2) && isset($group3) && isset($group4)) {
      $sql = "SELECT `id`, `name`, `img_name`, `num_of_pics` FROM `items` WHERE `publish`='1' AND `grp1`='$group1' AND `grp2`='$group2' AND `grp3`='$group3' AND `grp4`='$group4'";
      $r2 = mysql_query($sql);
      $items_count = mysql_num_rows($r2); // Find the number of items belongs to selected groups
      if ($items_count > 0) {
        echo("<ul class='grp4'>\n");
        while($items = mysql_fetch_array($r2)) {
          $IFCH = substr($items["img_name"], 0, 1); // Get Image name First Character
          $RI = rand(1, $items["num_of_pics"]); // Randomly set image
          $Img_File = "images/mods/" . $IFCH . "/thumbnails/" . $items["img_name"] . "_" . $RI . ".jpg"; // Set image file name
          $Img_Alt = $Grp4_Name . " " . $items["name"] . " - náhľ. obr. č. " . $RI; // Set image alternative text
          echo("<li>
  <a href='index.php?item=$items[id]'>");
          if (file_exists($Img_File)) echo("<img src='$Img_File' alt='$Img_Alt'><br>");
          echo("$items[name]</a>
</li>\n");
        }
        echo("</ul>\n");
      }
    }
  }

if (isset($akcia)) {
	if ($akcia == "search") {
		// Include searching code
		if (file_exists("include/search.php")) {
			require("include/search.php");
		}
		else {
			$error = error_title("V adresári [include] chýba súbor <u>search.php</u>!", "File <u>search.php</u> is missing in directory [include]!");
			exit($error);
		}
	}
	
	if ($akcia == "guestbook") {
		// Include guest book code
		if (file_exists("include/guestbook.php")) {
			require("include/guestbook.php");
		}
		else {
			$error = error_title("V adresári [include] chýba súbor <u>guestbook.php</u>!", "File <u>guestbook.php</u> is missing in directory [include]!");
			exit($error);
		}
	}
}

/*
  if ($tl_fulltext) {
    //Vytvorenie vyhladavacieho retazca MySQL
    $sql = "SELECT * FROM polozky WHERE zobrazit=1";

    $sql .= " AND nazov LIKE '%" . $fulltext . "%' OR autor LIKE '%" . $fulltext . "%'";
    $sql .= " OR plusy LIKE '%" . $fulltext . "%' OR minusy LIKE '%" . $fulltext . "%'";
    $sql .= " OR zaver LIKE '%" . $fulltext . "%' ORDER BY nazov";

    $r = mysql_query($sql);
    $pocet = mysql_num_rows($r);

    echo ("<center><big>:: Výsledok vyhľadávania ::</big></center><br>");

    if ($pocet > 0) {
      echo ("&nbsp;&nbsp;V databáze bolo nájdených <big><b>" . $pocet . "</b></big> záznamov podobných vyhľadávanému slovu!<br><br>");

      //Vypisanie najdenych hier
      $cislo = 1;

      echo ("<table align='center' border='0' cellpadding='0' cellspacing='0' class='tab1'>");

      while ($zaznam = mysql_fetch_array($r)) {
          echo ("<tr>");
            echo ("<td align='right'>&nbsp;" . $cislo . "&nbsp;");
            echo ("<td>&nbsp;<a href='zobraz_hru.php?typ=" . $zaznam["typ"] . "&amp;id=" .$zaznam[id] . "'>");
            echo ($zaznam["nazov"] . "</a> &nbsp;<i>[");

        if ($zaznam["hodnotenie"] == 0) echo ("Nehodnotená");
        else echo ($zaznam["hodnotenie"] . "/10");

            echo ("] - (" . $zaznam[pocet_obr] . " obr.)</i>&nbsp;</td>");
          echo ("</tr>");

        $cislo++;
      }

      echo ("</table>");
    }

    else {
      echo ("<big><b>&nbsp;&nbsp;V databáze nebol nájdený žiadny záznam podobný vyhľadávanému slovu!</b></big><br>");
    }
  }
*/

  // Display all messages from News
  if (isset($akcia) && $akcia == "show-all-news") {
    echo("<h1>Novinky - archív</h1>
<p align='right'><a href='index.php'>Späť na úvodnú stránku</a> &gt;&gt;</p>\n");
    $sql = "SELECT * FROM `news` WHERE `publish`='1' ORDER BY `date` DESC, `time` DESC";
    $r2 = mysql_query($sql);
    $NoN = mysql_num_rows($r2); // Number of News in database
    if ($NoN > 0) {
      while($news = mysql_fetch_array($r2)) {
        echo("<p class='date'>" . date_conv($news["date"]) . ", " . $news["time"] . "</p>\n");
        echo("<div class='message'>" . nl2br($news["message"]) . "</div>\n");
      }
      echo("<p align='right'><a href='index.php'>Späť na úvodnú stránku</a> &gt;&gt;</p>\n");
    }
    else echo("<h2 class='attention center'>V databáze sa nenachádzajú žiadne novinky!</h2>\n");
  }

  // Display WELCOME page
  if (!isset($item) && !isset($tl_fulltext) && !isset($akcia) && !isset($show_welcome)) {
    echo("<h1>Vítam vás na stránkach Mody.</h1>
<p>Tieto stránky by mali predstavovať pokračovanie mojich aktivít v GTA komunite. Stránky majú za úlohu podrobnejšie oboznámiť
svojich čitateľov o jednotlivých modoch do obľúbených hier. Primárne sa budem venovať hre GTA: San Andreas, ale občas si rád &quot;zaskočím&quot; aj do iných dielov tejto skvelej hernej série, prípadne rovno do inej hry. Pokiaľ to bude v mojich silách a bude sa to dať, rád vám všetky tieto
mody poskytnem na stiahnutie.</p>
<p>Dúfam, že sa vám moje stránky budú páčiť a že ich budete navštevovať.</p>
<p class='signature'>YoGI</p>\n");
    $sql = "SELECT * FROM `news` WHERE `publish`='1' ORDER BY `date` DESC, `time` DESC LIMIT 0, 1";
    $r2 = mysql_query($sql);
    $NoN = mysql_num_rows($r2); // Number of news
    if ($NoN > 0) {
      while($news = mysql_fetch_array($r2)) {
        echo("<p class='date'>" . date_conv($news["date"]) . ", " . $news["time"] . "</p>\n");
        echo("<div class='message'>" . nl2br($news["message"]) . "</div>\n");
      }
      echo ("<p align='right'><a href='index.php?action=show-all-news'>Zobraziť všetky novinky</a> &gt;&gt;</p>\n");
    }
    else echo ("<h2 class='attention center'>V databáze sa nenachádzajú žiadne novinky!</h2>\n");
    echo ("<p class='center'><img src='images/main/web_ikonka_velka.jpg' width='445' height='160' alt='www.gunsoft.sk/mody'></p>");
  }
?>
      </div>
      <div id="right-menu">
<?php
  if (!isset($item)) {
    // Include right menu
    if (file_exists("include/right-menu.php")) require_once("include/right-menu.php");
    else {
      $error = error_title("V adresári [include] chýba súbor <u>right-menu.php</u>!", "File <u>right-menu.php</u> is missing in directory [include]!");
      exit($error);
    }
  }
  else {
    // Load mod info, if it was selected
    if (file_exists("include/extra-info.php")) require_once("include/extra-info.php");
    else {
      $error = error_title("V adresári [include] chýba súbor <u>extra-info.php</u>!", "File <u>extra-info.php</u> is missing in directory [include]!");
      exit($error);
    }
  }
?>
      </div>
      <p id="pata">&copy; 2008 - 2012 <a href="http://www.gunsoft.sk/" target="_blank">Mário Herák - GUNSOFT</a></p>
    </div>
  </body>
</html>
<?php
  mysql_close($link); // Close connection with database
?>