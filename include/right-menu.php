<?php
/*
  (C) 2008 - 2011 Mário Herák - GUNSOFT
  Author: Mário Herák (herakm@gmail.com)
  Last update: 10.03.2011
*/

  // Display 10 newest mods
  echo("<h1>10 najnovších modov</h1>
  <ul class='rated'>\n");
  $sql = "SELECT `id`, `grp4`, `name` FROM `items` WHERE `publish`='1' ORDER BY `id` DESC LIMIT 10";
  $r = mysql_query($sql);
  while($items = mysql_fetch_array($r)) {
    $sql = "SELECT `name` FROM `groups_4` WHERE `id`='" . $items["grp4"] . "'";
    $r2 = mysql_query($sql);
    $grp4 = mysql_fetch_array($r2);
    $ToMenu = $grp4["name"] . " " . $items["name"];
    $ToURL = ClearURL($ToMenu);
    echo("<li><a href='index.php?item=$items[id]&amp;mod_name=$ToURL'>$ToMenu</a></li>\n");
  }
  echo("</ul>\n");
  // Display my 10 best mods
  echo("<h1>Moje NAJ mody</h1>
  <ul>\n");
  $sql = "SELECT `id` FROM `items` WHERE `publish`='1' AND `rating`='10'";
  $r = mysql_query($sql);
  $NoN = mysql_num_rows($r); // Number of items with 10 points - rated by my
  // If $NoN is 10 or less, select 10 items ordered by rating from the highest
  if ($NoN <= 10) $sql = "SELECT `id`, `grp4`, `name`, `rating` FROM `items` WHERE `publish`='1' ORDER BY `rating` DESC, `name` ASC LIMIT 10";
  // If $NoN is more than 10, select all items with rating 10 and order by name from A to Z
  if ($NoN > 10) $sql = "SELECT `id`, `grp4`, `name`, `rating` FROM `items` WHERE `publish`='1' AND `rating`='10' ORDER BY `name` ASC";
  $r = mysql_query($sql);
  while($items = mysql_fetch_array($r)) {
    $sql = "SELECT `name` FROM `groups_4` WHERE `id`='" . $items["grp4"] . "'";
    $r2 = mysql_query($sql);
    $grp4 = mysql_fetch_array($r2);
    $ToMenu = $grp4["name"] . " " . $items["name"];
    $ToURL = ClearURL($ToMenu);
    echo("<li>[" . $items["rating"] . "/10] <a href='index.php?item=$items[id]&amp;mod_name=$ToURL'>$ToMenu</a></li>\n");
  }
  echo("</ul>\n");
  // Display 10 best users mods
  echo("<h1>Vaše NAJ mody</h1>
  <ul>\n");
  $sql = "SELECT `id` FROM `items` WHERE `publish`='1' AND `users_rating`='10'";
  $r = mysql_query($sql);
  $NoI = mysql_num_rows($r); // Number of items rated 10 by users
  // If $NoI is 10 or less, select 10 items ordered by rating from highest
  if ($NoI <= 10) $sql = "SELECT `id`, `grp4`, `name`, `users_rating` FROM `items` WHERE `publish`='1' AND `users_rating` IS TRUE ORDER BY `users_rating` DESC, `name` ASC LIMIT 10";
  // If $NoI is more than 10, select all items with rating 10 and order by name from A to Z
  if ($NoI > 10) $sql = "SELECT `id`, `grp4`, `name`, `users_rating` FROM `items` WHERE `publish`='1' AND `users_rating`='10' ORDER BY `name` ASC";
  $r = mysql_query($sql);
  while($items = mysql_fetch_array($r)) {
    $sql = "SELECT `name` FROM `groups_4` WHERE `id`='" . $items["grp4"] . "'";
    $r2 = mysql_query($sql);
    $grp4 = mysql_fetch_array($r2);
    $ToMenu = $grp4["name"] . " " . $items["name"];
    $ToURL = ClearURL($ToMenu);
    echo("<li>[" . $items["users_rating"] . "/10] <a href='index.php?item=$items[id]&amp;mod_name=$ToURL'>$ToMenu</a></li>\n");
  }
  echo("</ul>\n");
  // Display 10 most downloaded mods
  echo("<h1>10 najsťahovanejších</h1>
  <ul>\n");
  $sql = "SELECT `id`, `grp4`, `name`, `times_dwnl` FROM `items` WHERE `publish`='1' ORDER BY `times_dwnl` DESC, `name` ASC LIMIT 10";
  $r = mysql_query($sql);
  while($items = mysql_fetch_array($r)) {
    $sql = "SELECT `name` FROM `groups_4` WHERE `id`='" . $items["grp4"] . "'";
    $r2 = mysql_query($sql);
    $grp4 = mysql_fetch_array($r2);
    $ToMenu = $grp4["name"] . " " . $items["name"];
    $ToURL = ClearURL($ToMenu);
    echo("<li>(" . $items["times_dwnl"] . "x) <a href='index.php?item=$items[id]&amp;mod_name=$ToURL'>$ToMenu</a></li>\n");
  }
  echo("</ul>\n");
  // Display users TOP reviews
  echo("<h1>Vaše NAJ články</h1>
  <ul>\n");
  $sql = "SELECT `item_id`, `users_rating` FROM `reviews` WHERE `publish`='1' AND `users_rating` IS TRUE ORDER BY `users_rating` DESC LIMIT 10";
  $r = mysql_query($sql);
  while($reviews = mysql_fetch_array($r)) {
    // Find id of item which belongs the review with id
    $sql = "SELECT `grp4`, `name` FROM `items` WHERE `id`='" . $reviews["item_id"] . "'";
    $r2 = mysql_query($sql);
    $items = mysql_fetch_array($r2);
    $name = $items["name"];
    // Find name of 4th group, it means Brand
    $sql = "SELECT `name` FROM `groups_4` WHERE `id`='" . $items["grp4"] . "'";
    $r3 = mysql_query($sql);
    $grp4 = mysql_fetch_array($r3);
    $ToMenu = $grp4["name"] . " " . $items["name"];
    $ToURL = ClearURL($ToMenu);
    echo("<li>[" . $reviews["users_rating"] . "/10] <a href='index.php?item=$reviews[item_id]&amp;mod_name=$ToURL'>$ToMenu</a></li>\n");
  }
  echo("</ul>\n");
  // Display 10 most viewed reviews
  echo("<h1>NAJčítanejšie články</h1>
  <ul>\n");
  $sql = "SELECT `viewed`, `item_id` FROM `reviews` WHERE `publish`='1' AND `viewed` IS TRUE ORDER BY `viewed` DESC LIMIT 10";
  $r = mysql_query($sql);
  while($reviews = mysql_fetch_array($r)) {
    // Find name of item and group 4 id according to "item_id" of selected review
    $sql = "SELECT `grp4`, `name` FROM `items` WHERE `id`='" . $reviews["item_id"] . "'";
    $r2 = mysql_query($sql);
    $items = mysql_fetch_array($r2);
    $name = $items["name"];
    // Find the name of 4th group, this means Brand
    $sql = "SELECT `name` FROM `groups_4` WHERE `id`='" . $items["grp4"] . "'";
    $r3 = mysql_query($sql);
    $grp4 = mysql_fetch_array($r3);
    $ToMenu = $grp4["name"] . " " . $items["name"];
    $ToURL = ClearURL($ToMenu);
    echo("<li>(" . $reviews["viewed"] . "x) <a href='index.php?item=$reviews[item_id]&amp;mod_name=$ToURL'>$ToMenu</a></li>\n");
  }
  echo("</ul>\n");
?>