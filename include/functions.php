<?php
/*
(C) 2008 - 2011 Mário Herák - GUNSOFT
Author: Mário Herák (herakm@gmail.com)
Last update: 04.02.2011
*/


// Clear string from unwanted characters (replace them with character "-")
function ClearURL($value) {
	// Replace space (" "), "%20" and "." for "-"
	// And at the end convert all characters to lower case
	$result = ereg_replace("[^a-z0-9._-]", "",
		str_replace(" ", "-",
			str_replace("%20", "-",
				str_replace(".", "-",
					strtolower($value)
				)
			)
		)
	);
	
	return $result;
}


// Prepare error message for displaying befor script stops
function error_title($err_title_sk, $err_title_en) {
	$error = "<html>
	<head>
		<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
		<meta name='author' content='Mário Herák - GUNSOFT, gunsoft@gunsoft.sk, www.gunsoft.sk, +421 910 129 492'>
		<meta name='copyright' content='(C) 2008 - 2011 Mário Herák GUNSOFT'>
		<meta name='robots' content='index,follow'>
		<meta name='description' content='Stránky venované detailnejším popisom modov do hier. Zamerané najmä na GTA: San Andreas a hernú sériu Grand Theft Auto.'>
		<meta name='keywords' content='GTA, Grand Theft Auto, San Andreas, GTA: SA, mody, GUNSOFT, Mário Herák'>
		
		<title>Mody | $err_title_sk / $err_title_en</title>
		
		<link rel='icon' type='image/ico' href='favicon.ico'>
		<link rel='stylesheet' type='text/css' href='css/reset.css'>
		<link rel='stylesheet' type='text/css' href='css/screen.css'>
	</head>
	
	
	<body class='error'>
		<h1 class='warning'>$err_title_sk</h1>
		
		<p>
			Ospravedlňujeme sa, ale naše webové stránky sú dočasne nedostupné. Skúste sa pripojiť neskôr.<br>
			Pre viac informácií nás prosím kontaktujte na tel. čísle: +421 910 129 492, Mário Herák, alebo e-mailom na <a href='mailto:herakm@gmail.com' target='_blank'>herakm@gmail.com</a>.<br>
			<br>
			Ďakujeme za pochopenie.
		</p>
		<hr>
		
		<h1 class='warning'>$err_title_en</h1>
		
		<p>
			Sorry, but our web pages are temporarily unavailable. Try it later.<br>
			For mofe information contact us on phone number: +421 910 129 492, Mário. Herák, or by e-mail <a href='mailto:herakm@gmail.com' target='_blank'>herakm@gmail.com</a>.<br>
			<br>
			Thank You for understanding.</p>
		<p class='signature'>Mário Herák - GUNSOFT</p>
	</body>
</html>";

return $error;
}


// Convert date in format "YYYY-MM-DD" (from database) to format "DD.MM.YYYY"
function date_conv($date) {
	$Year = substr($date, 0, 4);
	$Month = substr($date, 5, 2);
	$Day = substr($date, 8, 2);
	$New_Date = $Day . "." . $Month . "." . $Year;
	return $New_Date;
}


// Replace special signs in array $emoticons by smileys images
function add_emo($emoticons, $string) {
	foreach ($emoticons as $key => $array) {
		$repl = "<img src='images/emoticons/$key.gif' alt='$array[0]'>";
		$string = ereg_replace($array[1], $repl, $string);
	}
	
	return $string;
}


// Vyplni pole $f_table udajmi z tabulky $table
function FillTableArray($table, $column, $value) {
	$f_sql = "SELECT * FROM `" . $table . "` WHERE `" . $column . "`='" . $value . "'";
	$f_r = mysql_query($f_sql);
	$f_table = mysql_fetch_array($f_r);
	return $f_table;
}


/*
Vytvori nahladovy obrazok z urceneho obrazku; s danou sirkou; proporcionalne a s antialiasingom

$src (string) = cesta k zdrojovemu suboru
$thumbWidth (int) = sirka nahladoveho obrazku
*/
function createThumb($src, $thumbWidth) {
	$info = pathinfo($src);
	
	// Zisti adr. cestu k zdrojovemu suboru
	$src_dir = $info['dirname'];
	
	// Zisti meno a koncovku zdrojoveho suboru (spojeneho bodkou)
	$src_base = $info['basename'];
	
	// Zisti koncovku zdrojoveho suboru
	$src_ext = $info['extension'];
	
	// Zisti meno zdrojoveho suboru (bez koncovky)
	$src_file = $info['filename'];
	
	//echo ("\$src_dir = $src_dir<br>\$src_base = $src_base<br>\$src_ext = $src_ext<br>\$src_file = $src_file<br>");
	
	// load image and get image size
	if ($src_ext == "gif") {
		$img = imagecreatefromgif($src);
	}
	if ($src_ext == "jpg") {
		$img = imagecreatefromjpeg($src);
	}
	if ($src_ext == "png") {
		$img = imagecreatefrompng($src);
	}
	  
	// Get image width
	$width = imagesx($img);
	
	// Get image height
	$height = imagesy($img);
	
	// Ak je sirka alebo vyska obr. vacsia ako 128px, vypocita sa nova vyska nahl. obr.
	if ($width > 128 || $height > 128) {
		// calculate thumbnail size
		$new_width = $thumbWidth;
		$new_height = floor($height * ($thumbWidth / $width));
		
		// Ak je vypocitana vyska nahl. obr. vacsia ako 128px, vypocita sa nova sirka nahl. obr.
		if ($new_height > 128) {
			// calculate thumbnail size
			$new_height = $thumbWidth;
			$new_width = floor($width * ($thumbWidth / $height));
		}
	}
	else {
		// Inak sa vytvori nahl. obr. s rovnakymi rozmermi ako ma zdrojovy subor
		$new_width = $width;
		$new_height = $height;
	}
	
	// create a new temporary image
	$tmp_img = imagecreatetruecolor($new_width, $new_height);
	
	// copy and resize old image into new image
	imagecopyresampled($tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
	
	// Pripravi sa nazov suboru nahl. obr. spolu s cestou kam sa ulozi
	$name = $src_dir . "/thumbs/tn_" . $src_file . ".jpg";
	$thumb_name = "tn_" . $src_file . ".jpg";
	
	// save thumbnail into a file
	imagejpeg($tmp_img, $name);
	
	// vrati meno nahl. obr.
	return $thumb_name;
}
?>