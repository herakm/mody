<?php
/*
  (C) 2008 - 2011 Mário Herák - GUNSOFT
  Author: Mário Herák (herakm@gmail.com)
  Last update: 20.02.2011
*/
  if (isset($_GET['mod_name'])) $mod_name = $_GET['mod_name']; // Get mod name converted for URL
  if (isset($_POST["rate"])) $btn_rate = $_POST["rate"]; // Load value of btn. "Hodnotit", if pressed
  if (isset($_POST["hlasovat"])) $btn_vote = $_POST["hlasovat"]; // Load value of btn. "Hodnotit" if pressed

  // Load data from database according selected item id
  $sql = "SELECT * FROM `items` WHERE `id`='" . $item . "'";
  $r = mysql_query($sql);
  $items = mysql_fetch_array($r);

  $IFCH = substr($items["img_name"], 0, 1); // Get Image name First Character

  // Load name of Group 1 to which item belong
  $sql = "SELECT `name` FROM `groups_1` WHERE `id`='" . $items["grp1"] . "'";
  $r2 = mysql_query($sql);
  $groups_1 = mysql_fetch_array($r2);

  $Grp1_Name = $groups_1["name"]; // Select name of Group 1, where item belong

  // Load name of Group 2 to which item belong
  $sql = "SELECT `name` FROM `groups_2` WHERE `id`='" . $items["grp2"] . "'";
  $r2 = mysql_query($sql);
  $groups_2 = mysql_fetch_array($r2);

  $Grp2_Name = $groups_2["name"]; // Select name of Group 2, where item belong

  // Load name of Group 3 to which item belong
  $sql = "SELECT `name` FROM `groups_3` WHERE `id`='" . $items["grp3"] . "'";
  $r2 = mysql_query($sql);
  $groups_3 = mysql_fetch_array($r2);

  $Grp3_Name = $groups_3["name"]; // Select name of Group 3, where item belong

  // Load name and image name of Group 4 to which item belong
  $sql = "SELECT `name`, `img_name` FROM `groups_4` WHERE `id`='" . $items["grp4"] . "'";
  $r2 = mysql_query($sql);
  $groups_4 = mysql_fetch_array($r2);

  $Grp4_Name = $groups_4["name"]; // Select name of Group 4, where item belong
  $Grp4_img = $groups_4["img_name"]; // Select image name of Group 4, where item belong

  $Img_Title = $Grp4_Name . " " . $items["name"]; // Set image title text

  // Display bread crumbs
  echo("<p class='bread-crumbs'>
  <a href='index.php?grp1=$items[grp1]'>$Grp1_Name</a>
  \\
  <a href='index.php?grp1=$items[grp1]&amp;grp2=$items[grp2]'>$Grp2_Name</a>
  \\
  <a href='index.php?grp1=$items[grp1]&amp;grp2=$items[grp2]&amp;grp3=$items[grp3]'>$Grp3_Name</a>
  \\
  <a href='index.php?grp1=$items[grp1]&amp;grp2=$items[grp2]&amp;grp3=$items[grp3]&amp;grp4=$items[grp4]'>$Grp4_Name</a>
</p>\n");

  // Display mod logo and name
  echo ("<p class='logo center'>");
  // If exists group 4 image, display it
  if ($Grp4_img) {
    $Logo_Title = "logo " . $Grp4_Name;
    echo ("<img src='images/groups_4/" . $Grp4_img . ".jpg' alt='" . $Logo_Title . "'>&nbsp;");
  }
  // Display item name
  echo ($items["name"] . "</p>\n");

  // Display mod reviews
  $sql = "SELECT * FROM `reviews` WHERE `publish`='1' AND `item_id`='$item' ORDER BY `date` DESC, `time` DESC";
  $r2 = mysql_query($sql);
  $NoR = mysql_num_rows($r2); // Number of Reviews of selected mod
  //If there's at least one review, display all reviews belong to selected mod
  if ($NoR > 0) {
    while($reviews = mysql_fetch_array($r2)) {
      $RI = rand(1, $items["num_of_pics"]); // Randomly set image
      $Img_File = "images/mods/" . $IFCH . "/" . $items["img_name"] . "_" . $RI . ".jpg"; // Set image file name
      $Img_Alt = $Grp4_Name . " " . $items["name"] . " - náhľ. obr. č. " . $RI; // Set image alternative text

      // Display review with random image
      echo("<div class='review'>\n");
      if (file_exists($Img_File)) echo("<a rel='lightbox' href='$Img_File' title='$Img_Title'><img class='random' src='$Img_File' alt='$Img_Alt'></a>\n");
      echo($reviews["review"] . "\n</div>\n");

      $Rev_View = $reviews["viewed"]; // Get number of times the review was viewed

      // If user haven't voted,
      if (!isset($btn_vote) && !isset($btn_rate)) {
        $Rev_View++; // increase number of review's views with +1
        $sql = "UPDATE `reviews` SET `viewed`='$Rev_View' WHERE `id`='" . $reviews["id"] . "'";
        $res = mysql_query($sql);
      }

      $RUR = $reviews["users_rating"]; // Get users rating of the review

      // If user has pressed button "Hodnotit",
      if (isset($btn_rate)) {
        if (isset($_GET['rev_id'])) $rev_id = $_GET['rev_id'];
        $UR = $_POST["user-rating"]; // Get user rating
        $z = $UR + $RUR; // Set help variable $z as sum of user rating and users rating
        if ($RUR) $result = $z / 2; // If review was rated in database, divide user rating plus users rating by 2
        else $result = $UR; // If review wasn't rated yet, result is user rating
        $result = round($result); // Round result: 0-4 down, 5-9 up
        // Update users rating of review
        $sql = "UPDATE `reviews` SET `users_rating`='$result' WHERE `id`='" . $rev_id . "'";
        $res = mysql_query ($sql);
        $RUR = $result; // Set users rating of review as result
      }

      echo("<form class='users-rating' name='hodnotenie' method='post' action='index.php?rev_id=" . $reviews["id"] . "&amp;item=$item");
      if (isset($mod_name)) echo("&amp;mod_name=$mod_name");
      echo("'>
  Článok zobrazený <strong>$Rev_View</strong> krát.&nbsp;
  Vaše hodnotenie
  <select name='user-rating' size='1'>
    <option value=''");
      if (!$RUR) echo(" selected='selected'");
      echo(">- - - - -</option>\n");
      for($i = 1; $i < 11; $i++) {
        echo("<option value='$i'");
        if ($RUR == $i) echo(" selected='selected'"); // Mark users rating as selected
        echo(">" . $i . "/10</option>\n");
      }
      echo ("</select>
  <input type='submit' name='rate' value='Hodnotiť' class='form_tlacidlo'>&nbsp;
  <input class='form_tlacidlo' type='submit' value='Vytlačiť' onclick='window.print();'>
</form>\n");
    }
  }

  // If there are images of mod, display it
  if ($items["num_of_pics"] > 0) {
    echo ("<div class='mod-gallery'>
  <h3>Galéria</h3>\n");
    $IFCH = substr($items["img_name"], 0, 1); // Get Image name First Character
    for($i = 1; $i <= $items["num_of_pics"]; $i++) {
      $Img_File_Thumb = "images/mods/" . $IFCH . "/thumbnails/" . $items["img_name"] . "_" . $i . ".jpg"; // Set image thumb. file name
      $Img_File = "images/mods/" . $IFCH . "/" . $items["img_name"] . "_" . $i . ".jpg"; // Set image file name
      $Img_Alt = $Grp4_Name . " " . $items["name"] . " - náhľ. obr. č. " . $i; // Set image alternative text
      $Img_Title = $Grp4_Name . " " . $items["name"]; // Set image title text
      // If thumbnail and big file exists, display them
      if (file_exists($Img_File_Thumb) && file_exists($Img_File)) {
        $Img_Size = getimagesize($Img_File_Thumb); // Get thumbnail image size
        echo ("<a rel='lightbox[mod-gallery]' href='$Img_File' title='$Img_Title'>
  <img src='$Img_File_Thumb' alt='$Img_Alt' width='$Img_Size[0]' height='$Img_Size[1]' ></a>\n");
      }
    }
    echo ("</div>\n");
  }
?>