<?php
/*
  (C) 2008 - 2011 M�rio Her�k - GUNSOFT
  Author: M�rio Her�k (herakm@gmail.com)
  Last update: 30.01.2011
*/

  // Display Group 1 items
  $sql = "SELECT * FROM `groups_1` WHERE `publish`=1 ORDER BY `name` ASC";
  $r = mysql_query($sql);
  $grp1_count = mysql_num_rows($r); // Find the number of Group 1 items
  // If there's at least one item in Group 1, display menu
  if ($grp1_count > 0) {
    echo ("<ul class='grp1'>\n");
    while ($grp1 = mysql_fetch_array($r)) {
      echo ("<li class='grp1'>\n");
      $sql = "SELECT `id` FROM `groups_2` WHERE `publish`=1 AND `grp1`=" . $grp1['id'];
      $r2 = mysql_query($sql);
      $grp2_count = mysql_num_rows($r2); // Find the number of Group 2 items belong to Group 1 item
      if ($grp2_count > 0) echo ("<a href='index.php?grp1=" . $grp1["id"] . "'>"); // If there's at least one item belongs to Group 1 in Group 2, display hyper link
      echo ($grp1["name"]); // Display Group 1 item name
      if ($grp2_count > 0) echo ("</a>"); // If there's at least one item belongs to Group 1 in Group 2, display hyper link's end tag
      echo ("\n");
      if ($grp2_count > 0) { // If there's at least one item belongs to Group 1 in Group 2, display Group 2 items belongs to Group 1 item
        echo ("<ul class='grp2'>\n");
        $sql = "SELECT * FROM `groups_2` WHERE `grp1`=" . $grp1["id"] . " AND `publish`=1 ORDER BY `name` ASC";
        $r2 = mysql_query($sql);
        while ($grp2 = mysql_fetch_array($r2)) {
          echo ("<li>\n");
          $sql = "SELECT `id` FROM `groups_3` WHERE `publish`=1 AND `grp2`=" . $grp2["id"];
          $r3 = mysql_query($sql);
          $grp3_count = mysql_num_rows($r3); // Find the number of Group 3 items belongs to Group 2 item
          // If there's at least one item belongs to Group 2 in Group 3, display hyper link
          if ($grp3_count > 0) echo ("<a href='index.php?grp1=" . $grp1["id"] . "&amp;grp2=" . $grp2["id"] . "'>");
          echo ($grp2["name"]); // Display Group 2 item name
          if ($grp3_count > 0) echo ("</a>"); // If there's at least one item belongs to Group 1 in Group 2, display hyper link's end tag
          echo ("\n");
          if ($grp3_count > 0) { // If there's at least one item belongs to Group 2 in Group 3, display Group 3 items belongs to Group 2 item
            echo ("<ul class='grp3'>\n");
            $sql = "SELECT * FROM `groups_3` WHERE `grp2`=" . $grp2["id"] . " AND `publish`=1 ORDER BY `name` ASC";
            $r3 = mysql_query($sql);
            while ($grp3 = mysql_fetch_array($r3)) {
              echo ("<li>\n");
              $sql = "SELECT `id` FROM `groups_4` WHERE `publish`=1 AND `grp3`=" . $grp3["id"];
              $r4 = mysql_query($sql);
              $grp4_count = mysql_num_rows($r3); // Find the number of Group 4 items belong to Group 3 item
              // If there's at least one item belongs to Group 3 in Group 4, display hyper link
              if ($grp4_count > 0) echo ("<a href='index.php?grp1=" . $grp1["id"] . "&amp;grp2=" . $grp2["id"] . "&amp;grp3=" . $grp3["id"] . "'>");
              echo ($grp3["name"]);
              if ($grp4_count > 0) echo ("</a>"); // If there's at least one item belongs to Group 3 in Group 4, display hyper link's end tag
              echo ("\n");

/*
              if ($grp4_count > 0) { // If there's at least one item belongs to Group 3 in Group 4, display Group 4 items belongs to Group 3 item
                
              }
*/
              
              echo ("</li>\n");
            }
            echo ("</ul>\n");
          }
          echo ("</li>\n");
        }
        echo ("</ul>\n");
      }
      echo ("</li>\n");
    }
    echo ("</ul>\n");
  }
?>