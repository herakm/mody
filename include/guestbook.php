<?php
/*
(C) 2008 - 2012 Mário Herák - GUNSOFT
Author: Mário Herák (herakm@gmail.com)
*/


echo "<h1>Kniha hostí</h1>\n\n";


// If message was send by user:
if (isset($btn_send_message) && $btn_send_message) {
	$error = FALSE;
	
	/*
	Recaptcha check answer from user if it's valid
	//if (isset($_POST["recaptcha_challenge_field"]) && isset($_POST["recaptcha_response_field"])) $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
	*/
	
	if (!$user_nick) {
		echo "<h3 class='center warning'>Nezadali ste žiadnu <strong>prezývku</strong>!</h3>\n\n";
	}
	if (!$user_message) {
		echo "<h3 class='center warning'>Nevložili ste žiadny <strong>odkaz</strong>!</h3>\n\n";
	}
	if (!$validCheck) {
		echo "<h3 class='center warning'>Nevložili ste žiadnu <strong>kontrolnú odpoveď</strong>!</h3>\n\n";
	}
	if (isset($resp) && !$resp->is_valid) {
		echo "<h3 class='center warning'>Neopísali ste správne dve slová z <strong>ReCAPTCHA</strong>!</h3>\n\n";
	}
	
	if (!$user_nick || !$user_message || !$validCheck
/*      || (isset($resp) && !$resp->is_valid) */
		)
	{
		// Set if user forget to insert necessary data
		$error = TRUE;
	}
	
	// Kontrola pritomnosti cloveka
	if ($validCheck != $odpoved[$validCheckId]) {
		echo "<h3 class='center warning'>Neodpovedali ste správne na <strong>kontrolnú otázku</strong>!</h3>\n\n";
		$error = TRUE;
	}
	
	// If user insert necessary data to form, insert user message to database
	if (!$error) {
		$datum = date("Y-m-d");
		$cas = date("H:i:s");
		
		$sql = "INSERT INTO `guestbook` (`date`, `time`, `nick`, `email`, `message`, `publish`) VALUES ('$datum', '$cas', '$user_nick', '$email', '$user_message', '1')";
		$res = mysql_query($sql);
		
		// Nacita z tabulky "settings" email administratora
		$emailAddr = $settings['admin_email'];
		
		// Pripravi samotnu emailovu spravu
		$emailMessage = "<html>
	<head>
		<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
		<title>Nová správa v Knihe hostí od $user_nick | Mody</title>
	</head>
	
	<body>
		<p>
			Na stránkach <strong>Mody</strong> bola vložená užívateľom <strong>$user_nick</strong>";
		if ($email) {
			$emailMessage .= " [$email] ";
		}
		$emailMessage .= "nová správa v <strong>Knihe hostí</strong>:
		</p>
		
		<p>$user_message</p>
		
		<p>
			Správu si môžete prečítať priamo na stránkach kliknutím na <a href='http://www.mody.gunsoft.sk/index.php?action=guestbook' target='_blank' title='Prečítať správu'>tento odkaz</a>.
		</p>
	</body>
</html>";
		
		// Pripravi predmet emailovej spravy
		$emailSubject = "Nova sprava v Knihe hosti od uzivatela " . $user_nick . " | Mody";
		
		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-Type: text/html; charset=utf-8' . "\r\n";
		
		// Additional headers
		//$headers .= 'To: Mary <mary@example.com>, Kelly <kelly@example.com>' . "\r\n";
		$headers .= 'From: Webmaster - Mody <' . $emailAddr . '>' . "\r\n";
		//$headers .= 'Cc: birthdayarchive@example.com' . "\r\n";
		//$headers .= 'Bcc: birthdaycheck@example.com' . "\r\n";
		
		// Odosle administratorovi email
		mail ($emailAddr, $emailSubject, $emailMessage, $headers);
		
		// Empty user message after inserting to database
		$user_message = "";
		
		echo "<h3 class='center attention'>Ďakujem, Váš odkaz bol uložený.</h3>\n\n";
		
	}
}


echo "<form id='form-message' name='form-message' method='post' action='index.php?action=guestbook'>
	<table>
		<tr>
			<th colspan='2'>Odkaz:</th>
		</tr>
		<tr>
			<td class='left'>* Prezývka:</td>
			<td>
				<input id='usernick' class='text' type='text' name='user-nick'";
if (isset($user_nick)) {
	echo " value='", $user_nick, "'";
}
echo " maxlength='255'>
			</td>
		</tr>
		<tr>
			<td class='left'>E-mail:</td>
			<td>
				<input class='text' type='text' name='email'";
if (isset($email)) {
	echo " value='", $email, "'";
}
echo " maxlength='255'>
			</td>
		</tr>
		<tr>
			<td class='left top'>* Odkaz:</td>
			<td>
				<textarea id='usermessage' class='text' name='user-message' rows='5' cols='50'>";
if (isset($user_message) && $user_message) {
	echo $user_message;
}
echo "</textarea>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
				<ul>\n";

// Display emoticons
foreach ($Emoticons as $key => $array) {
	echo "<li>
	<button
		class='emoticons'
		type='submit'
		style='background: url(images/emoticons/", $key, ".gif) no-repeat scroll 0 0 transparent;'
		title='", $array[0], "'
		alt='", $array[0], "'
		name='emo'
		value='", $array[1], "'>", $array[1], "</button>
</li>\n";
}

echo "</ul>
			</td>
		</tr>\n";

// Urci nahodne cislo
$r = mt_rand(0, 9);

echo "<tr>
			<td class='left'>Kontrola:</td>
			<td>
				<strong>", $otazka[$r], "</strong><br>
				<input class='text' type='text' id='valid-check' name='valid-check'>
				<input type='hidden' name='valid-check-id' value='", $r, "'><br>
				<small>(Odpoveď prosím vyplňte slovom a malými písmenami.)</small>
			</td>
		</tr>\n";

/*
echo "<tr>
			<td>&nbsp;</td>
			<td class='top'>\n", recaptcha_get_html($publickey), "\n</td>
		</tr>\n";
*/

echo "<tr>
			<td colspan='2' class='center'>
				<input type='submit' name='send-message' value='Poslať odkaz'>
			</td>
		</tr>
	</table>
</form>\n";


// Display users messages
echo "<div class='users-messages'>\n";

$sql = "SELECT * FROM `guestbook` WHERE `publish`='1' ORDER BY `date` DESC, `time` DESC";
$r = mysql_query($sql);

// Select number of messages from guest book
$NoM = mysql_num_rows($r);

if (!$NoM) {
	echo "<h3 class='attention center'>V knihe hostí zatiaľ nie sú žiadne odkazy!</h3>\n";
}
else {
	while ($guestbook = mysql_fetch_array($r)) {
		// Replace secial characters by images of emoticons
		$Message = add_emo($Emoticons, $guestbook["message"]);
		
		// Replace hidden EOL (End Of Line) characters by HTML <br /> entity
		$Message = nl2br($Message);
		
		echo "<div class='user-message'>
	<p class='user-nick'>\n";
		
		if ($guestbook["email"]) {
			echo "<a href='mailto:$guestbook[email]?subject=Dotaz%20zo%20stranky%20Mody' target='_blank'>\n";
		}
		
		echo ($guestbook["nick"]);
		if ($guestbook["email"]) {
			echo("</a>");
		}
		echo "</p>\n";
		
		$yyyy = substr($guestbook["date"], 0, 4);
		$mm = substr($guestbook["date"], 5, 2);
		$dd = substr($guestbook["date"], 8, 2);
		
		echo "<p class='message-date'>$dd.$mm.$yyyy, $guestbook[time]</p>
	<p>\n" . $Message . "\n</p>
</div>\n";
	}
}

echo "</div>\n";
?>