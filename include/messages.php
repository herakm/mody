<?php
/*
	(C) 2008 - 2012 Mário Herák - GUNSOFT
	Author: Mário Herák (herakm@gmail.com)
*/

// If message was send by user:
if (isset($btn_send_message) && $btn_send_message) {
	$error = FALSE;
	$errMsg = "";
	
	/*
	// Recaptcha check answer from user if it's valid
	if (isset($_POST["recaptcha_challenge_field"]) && isset($_POST["recaptcha_response_field"])) $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
	*/
	
	if (!$user_nick) {
		$errMsg .= "<h3 class='center warning'>
	Nezadali ste žiadnu <strong>prezývku</strong>!
</h3>\n\n";
	}
	
	if (!$user_message) {
		$errMsg .= "<h3 class='center warning'>
	Nevložili ste žiadny <strong>odkaz</strong>!
</h3>\n\n";
	}
	
	if (!$validCheck) {
		$errMsg .= "<h3 class='center warning'>
	Nevložili ste žiadnu <strong>kontrolnú odpoveď</strong>!
</h3>\n\n";
	}
	
	if (isset($resp) && !$resp->is_valid) {
		$errMsg .= "<h3 class='center warning'>
	Neopísali ste správne dve slová z <strong>ReCAPTCHA</strong>!
</h3>\n\n";
	}
	
	if (!$user_nick
		|| !$user_message
		|| !$validCheck
/*		|| (isset($resp) && !$resp->is_valid) */
	) {
		// Set if user forget to insert necessary data
		$error = TRUE;
	}
	
	// Kontrola pritomnosti cloveka
	if ($validCheck != $odpoved[$validCheckId]) {
		$errMsg .= "<h3 class='center warning'>
	Neodpovedali ste správne na <strong>kontrolnú otázku</strong>!
</h3>\n\n";
		$error = TRUE;
	}
	
	// If user insert necessary data to form, insert user message to database
	if (!$error) {
		$datum = date("Y-m-d");
		$cas = date("H:i:s");
		
		// Kontrola uzivatelom vlozenych udajov:
		
		/*
		 * Ak je v PHP zapnuta funkcia Magic Quotes
		 * ( vklada / (lomitko) za ' (jedn. uv.), " (dvoj. uv.), \ (spatne lomitko) a NULL znaky ),
		 * odstrania sa / vlozene funkciou Magic Quotes:
		 * */
		if (get_magic_quotes_gpc()) {
			$user_nick = stripslashes($user_nick);
			$email = stripslashes($email);
			$user_message = stripslashes($user_message);
		}
		
		/*
		 * Osetri specialne znaky v retazci pre pouzitie s SQL:
		 *  prida spatne lomitka pred: \x00, \n, \r, \, ', " a \x1a
		 * */
		$user_nick =  mysql_real_escape_string($user_nick);
		$email = mysql_real_escape_string($email);
		$user_message = mysql_real_escape_string($user_message);
		
		$sql = "INSERT INTO `messages` (`item_id`, `date`, `time`, `nick`, `email`, `message`, `publish`)
				VALUES ('$item', '$datum', '$cas', '$user_nick', '$email', '$user_message', '1')";
		$res = mysql_query($sql);
		
		//Zisti vsetky info o modely pre ktory bola sprava napisana
		$query = "SELECT `grp1`, `grp2`, `grp3`, `grp4`, `name` FROM `items` WHERE `id`='" . $item . "'";
		$rows = mysql_query($query);
		$resultItems = mysql_fetch_array($rows);
		
		// Urci nazov modelu
		$modelName = $resultItems['name'];
		
		// Urci cestu k modelu
		$modelBreadcrumbs = "";
		
		$query = "SELECT `name` FROM `groups_1` WHERE `id`='" . $resultItems['grp1'] . "'";
		$rows = mysql_query($query);
		$resultGroups1 = mysql_fetch_array($rows);
		$modelBreadcrumbs .= $resultGroups1['name'];
		
		$modelBreadcrumbs .= " \ ";
		
		$query = "SELECT `name` FROM `groups_2` WHERE `id`='" . $resultItems['grp2'] . "'";
		$rows = mysql_query($query);
		$resultGroups2 = mysql_fetch_array($rows);
		$modelBreadcrumbs .= $resultGroups2['name'];
		
		$modelBreadcrumbs .= " \ ";
		
		$query = "SELECT `name` FROM `groups_3` WHERE `id`='" . $resultItems['grp3'] . "'";
		$rows = mysql_query($query);
		$resultGroups3 = mysql_fetch_array($rows);
		$modelBreadcrumbs .= $resultGroups3['name'];
		
		$modelBreadcrumbs .= " \ ";
		
		$query = "SELECT `name` FROM `groups_4` WHERE `id`='" . $resultItems['grp4'] . "'";
		$rows = mysql_query($query);
		$resultGroups4 = mysql_fetch_array($rows);
		$modelBreadcrumbs .= $resultGroups4['name'];
		$modelBrand = $resultGroups4['name'];;
		
		$modelBreadcrumbs .= " \ ";
		
		// Urci nazov modelu do url
		$ToURL = $modelBrand . " " . $modelName;
		$ToURL = ClearURL($ToURL);
		
		// Urci URL k modelu
		$url = "http://mody.gunsoft.sk?item=" . $item . "&amp;mod_name=" . $ToURL;
		
		// Nacita z tabulky "settings" email administratora
		$emailAddr = $settings['admin_email'];
// 		echo "email: ", $emailAddr;
		
		// Pripravi samotnu emailovu spravu
		$emailMessage = "<html>
	<head>
		<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
		<title>Nová správa pre model $modelName od $user_nick | Mody</title>
	</head>
	
	<body>
		<p>
			Na stránkach <strong>Mody</strong> bola vložená užívateľom <strong>$user_nick</strong>
			nová správa pre model:
		</p>
		
		<p align='center'>$modelBreadcrumbs<br><strong>$modelName</strong></p>
		
		<p>
			Správu si môžete prečítať kliknutím na <a href='$url' target='_blank' title='Prečítať správu'>tento odkaz</a>.
		</p>
	</body>
</html>";
		
		// Pripravi predmet emailovej spravy
		$emailSubject = "Nova sprava pre model " . $modelName . " od " . $user_nick . " | Mody";
		
		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-Type: text/html; charset=utf-8' . "\r\n";
		
		// Additional headers
		//$headers .= 'To: Mary <mary@example.com>, Kelly <kelly@example.com>' . "\r\n";
		$headers .= 'From: Webmaster - Mody <' . $emailAddr . '>' . "\r\n";
		//$headers .= 'Cc: birthdayarchive@example.com' . "\r\n";
		//$headers .= 'Bcc: birthdaycheck@example.com' . "\r\n";
		
		// Odosle administratorovi email
		mail ($emailAddr, $emailSubject, $emailMessage, $headers);
		
		// Empty user message after inserting to database
		$user_message = "";
	}
}


echo "<form id='form-message' name='form-message' method='post' action='index.php?";
if (isset($mod_name)) {
	echo "mod_name=", $mod_name, "&amp;";
}
echo "item=", $item, "#form-message'>
	<table>
		<tr>
			<th colspan='2'>Odkaz:</th>
		</tr>
		<tr>
			<td class='left'>* Prezývka:</td>
			<td>
				<input class='text' type='text' name='user-nick'";
if (isset($user_nick)) {
	echo " value='", $user_nick, "'";
}
echo " maxlength='255'>
			</td>
		</tr>
		<tr>
			<td class='left'>E-mail:</td>
			<td>
				<input class='text' type='text' name='email'";
if (isset($email)) {
	echo " value='", $email, "'";
}
echo " maxlength='255'>
			</td>
		</tr>
		<tr>
			<td class='left top'>* Odkaz:</td>
			<td>
				<textarea id='um' class='text' name='user-message' rows='5' cols='50'>";
if (isset($user_message)) {
	echo $user_message;
}
echo "</textarea>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
				<ul>\n";
// Display emoticons
foreach ($Emoticons as $key => $array) {
	echo "<li>
	<input class='emoticons' type='image' src='images/emoticons/", $key, ".gif'
	title='", $array[0], "' alt='", $array[0], "' name='emo' value='", $array[1], "'>
</li>\n";
}
echo "</ul>
			</td>
		</tr>\n";

// Urci nahodne cislo
$r = mt_rand(0, 9);

echo "<tr>
	<td class='left'>Kontrola:</td>
	<td>
		<strong>", $otazka[$r], "</strong><br>
		<input class='text' type='text' id='valid-check' name='valid-check'>
		<input type='hidden' name='valid-check-id' value='", $r, "'><br>
		<small>(Odpoveď prosím vyplňte slovom a malými písmenami.)</small>
	</td>
</tr>\n";

/*
echo "<tr>
	<td>&nbsp;</td>
	<td class='top'>\n", recaptcha_get_html($publickey), "\n</td>
</tr>\n";
*/

echo "<tr>
			<td colspan='2' class='center'>
				<input type='submit' name='send-message' value='Poslať odkaz'>
			</td>
		</tr>
	</table>
</form>\n\n";

// If message was send by user:
if (isset($btn_send_message) && $btn_send_message) {
	if (!$error) {
		echo "<h3 class='center attention'>Ďakujem, Váš odkaz bol uložený.</h3>\n\n";
	}
	else {
		echo $errMsg;
	}
}

// Display users messages
echo "<div class='users-messages'>\n";

$sql = "SELECT * FROM `messages` WHERE `item_id`='$item' AND `publish`='1' ORDER BY `date` DESC, `time` DESC";
$r = mysql_query($sql);

// Find number of messages belongs to selected item
$NoM = mysql_num_rows($r);

if (!$NoM) {
	echo "<h3 class='attention center'>K tomuto modu zatiaľ neexistujú žiadne odkazy!</h3>\n";
}
else {
	while($messages = mysql_fetch_array($r)) {
		// Replace secial characters by images of emoticons
		$Message = add_emo($Emoticons, $messages["message"]);
		
		// Replace hidden EOL (End Of Line) characters by HTML <br /> entity
		$Message = nl2br($Message);
		
		echo "<div class='user-message'>
	<p class='user-nick'>\n";
			if ($messages["email"]) {
				echo "<a href='mailto:$messages[email]?subject=Dotaz%20zo%20stranky%20Mody' target='_blank'>\n";
			}
			echo $messages["nick"];
			if ($messages["email"]) {
				echo "</a>";
			}
			echo "</p>\n";
			
			$yyyy = substr($messages["date"], 0, 4);
			$mm = substr($messages["date"], 5, 2);
			$dd = substr($messages["date"], 8, 2);
			echo "<p class='message-date'>", $dd, ".", $mm, ".", $yyyy,", ", $messages['time'], "</p>";
			
			echo "<p>\n", $Message, "\n</p>
</div>\n";
	}
}
echo "</div>\n";
?>