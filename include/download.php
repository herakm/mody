<?php
/*
  (C) 2008 - 2011 M�rio Her�k - GUNSOFT
  Author: M�rio Her�k (herakm@gmail.com)
  Last update: 06.02.2011
*/

  if (isset($_GET["file"])) {
    $MF = $_GET["file"]; // Get mod's filename

    // Include database connection
    if (file_exists("db-connect.php")) require("db-connect.php");
    else {
      $error = "V adres�ri [include] ch�ba s�bor <u>db-connect.php</u>! / File <u>db-connect.php</u> is missing in directory [include]!";
      exit($error);
    }

    // Load data from database according selected item id
    $sql = "SELECT `times_dwnl` FROM `items` WHERE `file_name`='$MF'";
    $r = mysql_query($sql);
    $items = mysql_fetch_array($r);

    $file = "../download/" . $MF; // Get mod's file name from URL

    $TD = $items["times_dwnl"]; // Get times of downloads of mod's file
    $TD++;

    // Update number of downloads
    $sql = "UPDATE `items` SET `times_dwnl`='$TD' WHERE `file_name`='$MF'";
    $r = mysql_query($sql);

    if (file_exists($file)) {
      header('Content-Description: File Transfer');
      header('Content-Type: application/octet-stream');
      header('Content-Disposition: attachment; filename='.basename($file));
      header('Content-Transfer-Encoding: binary');
      header('Expires: 0');
      header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
      header('Pragma: public');
      header('Content-Length: ' . filesize($file));
      ob_clean();
      flush();
      readfile($file);
      exit;
    }
  }
?>