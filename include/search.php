<?php
/*
  (C) 2008 - 2011 Mário Herák - GUNSOFT
  Author: Mário Herák (herakm@gmail.com)
  Last update: 10.03.2011
*/

  if (isset($_POST["search"])) $hladat = $_POST["search"]; // Get button state

  if (!isset($hladat)) {
    echo("<h1>Vyhľadávanie</h1>
<form class='search' name='vyhladavanie' action='index.php?action=search' method='post'>
  <table>
    <tr>
      <td class='right'><strong>Text:</strong></td>
      <td>
        <input id='search_text' type='text' name='hladat_text' size='25'>
      </td>
    </tr>
    <tr>
      <td colspan='2'>Hľadať v</td>
    </tr>
    <tr>
      <td class='right'><strong>Skupine:</strong></td>
      <td>
        <table>
          <tr>\n");
    for($i = 1; $i < 5; $i++) echo("<td class='center'>$i</td>\n");
    echo("</tr>
<tr>\n");
    for($i = 1; $i < 5; $i++) echo("<td><input type='checkbox' name='hladat_skup$i' checked='checked'></td>\n");
    echo("</tr>
        </table>
      </td>
    </tr>
    <tr>
      <td class='right'>Názve položky:</td>
      <td><input type='checkbox' name='hladat_nazov_polozky' checked='checked'></td>
    </tr>
    <tr>
      <td class='right'>Názve modelu:</td>
      <td><input type='checkbox' name='hladat_model' checked='checked'></td>
    </tr>
    <tr>
      <td class='right'>Menách autorov:</td>
      <td><input type='checkbox' name='hladat_autor' checked='checked'></td>
    </tr>
    <tr>
      <td class='right'>Plusoch:</td>
      <td><input type='checkbox' name='hladat_plusy' checked='checked'></td>
    </tr>
    <tr>
      <td class='right'>Mínusoch:</td>
      <td><input type='checkbox' name='hladat_minusy' checked='checked'></td>
    </tr>
    <tr>
      <td class='right'>Záveroch:</td>
      <td><input type='checkbox' name='hladat_zavery' checked='checked'></td>
    </tr>
    <tr>
      <td class='right'>Linkoch:</td>
      <td><input type='checkbox' name='hladat_linky' checked='checked'></td>
    </tr>
    <tr>
      <td class='right'>Zoradiť podľa:</td>
      <td>
        <select size='1' name='hladat_radit'>
          <option value=''>- - - - -</option>
          <option value='date'>Dátum</option>
          <option value='name'>Názov</option>
          <option value='rating'>Hodnotenie</option>
        </select>
      </td>
    </tr>
    <tr>
      <td class='right'>Poradie:</td>
      <td>
        <select size='1' name='hladat_poradie'>
          <option value=''>- - - - -</option>
          <option value='ASC'>A - Z</option>
          <option value='DESC'>Z - A</option>
        </select>
      </td>
    </tr>
    <tr>
      <td class='right'>Zobraziť obrázky:</td>
      <td><input type='checkbox' name='hladat_obrazky'></td>
    </tr>
    <tr>
      <td colspan='2' class='center'><input type='submit' name='search' value='Hladať'></td>
    </tr>
  </table>
</form>
<p class='rady'>
  Pre zobrazenie všetkých modov nevložte žiaden text.<br>
  Vyhľadávanie nerobí rozdiel medzi veľkými a malými písmenami!
</p>\n");
  }
  else {
    // Get filled data from form
    $hladat_text = $_POST["hladat_text"]; // Get search string
    //$hladat_text = bezdiakritiky ($_POST["hladat_text"]);
    //$hladat_text = iconv ("windows-1250", "utf-8", $hladat_text);
    // Check if search also in groups names
    if (isset($_POST["hladat_skup1"])) $hladat_skup1 = $_POST["hladat_skup1"];
    if (isset($_POST["hladat_skup2"])) $hladat_skup2 = $_POST["hladat_skup2"];
    if (isset($_POST["hladat_skup3"])) $hladat_skup3 = $_POST["hladat_skup3"];
    if (isset($_POST["hladat_skup4"])) $hladat_skup4 = $_POST["hladat_skup4"];
    if (isset($_POST["hladat_nazov_polozky"])) $hladat_nazov_polozky = $_POST["hladat_nazov_polozky"]; // Check if also search in name of item
    if (isset($_POST["hladat_model"])) $hladat_model = $_POST["hladat_model"]; // Check if also search in names of models
    if (isset($_POST["hladat_autor"])) $hladat_autor = $_POST["hladat_autor"]; // Check if also search in autors names
    if (isset($_POST["hladat_plusy"])) $hladat_plusy = $_POST["hladat_plusy"]; // Check if also search in pluses
    if (isset($_POST["hladat_minusy"])) $hladat_minusy = $_POST["hladat_minusy"]; // Check if also search in minuses
    if (isset($_POST["hladat_zavery"])) $hladat_zavery = $_POST["hladat_zavery"]; // Check if also search in results
    if (isset($_POST["hladat_linky"])) $hladat_linky = $_POST["hladat_linky"]; // Check if also search in links
    if (isset($_POST["hladat_radit"])) $hladat_radit = $_POST["hladat_radit"]; // Check if order results
    if (isset($_POST["hladat_poradie"])) $hladat_poradie = $_POST["hladat_poradie"]; // Check results order by
    if (isset($_POST["hladat_obrazky"])) $hladat_obrazky = $_POST["hladat_obrazky"]; // Check if also display mods images

    // Create the search string for MySQL
    $sql = "SELECT * FROM `items` WHERE `publish`='1'";

    // If search also in group 1 names
    if ($hladat_skup1) {
      $sql3 = "SELECT `id` FROM `groups_1` WHERE `publish`='1' AND `name` LIKE '%$hladat_text%'";
      $r3 = mysql_query($sql3);
      $groups_1 = mysql_fetch_array($r3);
      if (mysql_num_rows($r3)) {
        $sql .= " AND `grp1`='$groups_1[id]'";
        $hl_sk1 = TRUE;
      }
    }

    // If search also in group 2 names
    if ($hladat_skup2) {
      $sql3 = "SELECT `id` FROM `groups_2` WHERE `publish`='1' AND `name` LIKE '%$hladat_text%'";
      $r3 = mysql_query($sql3);
      $groups_2 = mysql_fetch_array($r3);
      if (mysql_num_rows($r3)) {
        if ($hl_sk1) $sql .= " OR ";
        else $sql .= " AND ";
        $sql .= "`grp2`='$groups_2[id]'";
        $hl_sk2 = TRUE;
      }
    }

    // If search also in group 3 names
    if ($hladat_skup3) {
      $sql3 = "SELECT `id` FROM `groups_3` WHERE `publish`='1' AND `name` LIKE '%$hladat_text%'";
      $r3 = mysql_query($sql3);
      $groups_3 = mysql_fetch_array($r3);
      if (mysql_num_rows($r3)) {
        if ($hl_sk1 || $hl_sk2) $sql .= " OR ";
        else $sql .= " AND ";
        $sql .= "`grp3`='$groups_3[id]'";
        $hl_sk3 = TRUE;
      }
    }

    // If search also in group 4 names
    if ($hladat_skup4) {
      $sql3 = "SELECT `id` FROM `groups_4` WHERE `publish`='1' AND `name` LIKE '%$hladat_text%'";
      $r3 = mysql_query($sql3);
      $groups_4 = mysql_fetch_array($r3);
      if (mysql_num_rows($r3)) {
        if (isset($hl_sk1) || isset($hl_sk2) || isset($hl_sk3)) $sql .= " OR ";
        else $sql .= " AND ";
        $sql .= "`grp4`='$groups_4[id]'";
        $hl_sk4 = TRUE;
      }
    }

    // If also search in item names
    if ($hladat_nazov_polozky) {
      if (isset($hl_sk1) || isset($hl_sk2) || isset($hl_sk3) || isset($hl_sk4)) $sql .= " OR ";
      else $sql .= " AND "; 
      $sql .= "`name` LIKE '%$hladat_text%'";
    }

    // If also search in models names
    if ($hladat_model) {
      $sql3 = "SELECT `id` FROM `models` WHERE `publish`='1' AND `name` LIKE '%$hladat_text%'";
      $r3 = mysql_query($sql3);
      $models = mysql_fetch_array($r3);
      if (mysql_num_rows($r3)) {
        if ($hl_sk1 || $hl_sk2 || $hl_sk3 || $hl_sk4 || $hladat_nazov_polozky) $sql .= " OR ";
        else $sql .= " AND ";
        $sql .= "`model_id`='$models[id]'";
        $hl_mod = TRUE;
      }
    }

    // If search also in autors names
    if ($hladat_autor) {
      if (isset($hl_sk1) || isset($hl_sk2) || isset($hl_sk3) || isset($hl_sk4) || isset($hladat_nazov_polozky) || isset($hl_mod)) $sql .= " OR ";
      else $sql .= " AND ";
      $sql .= "`author` LIKE '%$hladat_text%'";
    }

    // If also search in pluses
    if ($hladat_plusy) {
      if (isset($hl_sk1) || isset($hl_sk2) || isset($hl_sk3) || isset($hl_sk4) || isset($hladat_nazov_polozky) || isset($hl_mod) || isset($hladat_autor)) $sql .= " OR ";
      else $sql .= " AND ";
      $sql .= "`plus` LIKE '%$hladat_text%'";
    }

    // If also search in minuses
    if ($hladat_minusy) {
      if (isset($hl_sk1) || isset($hl_sk2) || isset($hl_sk3) || isset($hl_sk4) || isset($hladat_nazov_polozky) || isset($hl_mod) || isset($hladat_autor) || isset($hladat_plusy)) $sql .= " OR ";
      else $sql .= " AND ";
      $sql .= "`minus` LIKE '%$hladat_text%'";
    }

    // If search also in results
    if ($hladat_zavery) {
      if (isset($hl_sk1) || isset($hl_sk2) || isset($hl_sk3) || isset($hl_sk4) || isset($hladat_nazov_polozky) || isset($hl_mod) || isset($hladat_autor) || isset($hladat_plusy) || isset($hladat_minusy)) $sql .= " OR ";
      else $sql .= " AND ";
      $sql .= "`result` LIKE '%$hladat_text%'";
    }

    // If search also in links
    if ($hladat_linky) {
      if (isset($hl_sk1) || isset($hl_sk2) || isset($hl_sk3) || isset($hl_sk4) || isset($hladat_nazov_polozky) || isset($hl_mod) || isset($hladat_autor) || isset($hladat_plusy) || isset($hladat_minusy) || isset($hladat_zavery)) $sql .= " OR ";
      else $sql .= " AND ";
      $sql .= "`links` LIKE '%$hladat_text%'";
    }

    if ($hladat_radit) $sql .= " ORDER BY `$hladat_radit`";

    if ($hladat_radit && $hladat_poradie) $sql .= " " . $hladat_poradie;

    $r = mysql_query($sql);
    $NoI = mysql_num_rows($r); // Get number of items by selected query

    echo("<h1>Výsledky vyhľadávania</h1>\n");

    // If there is at least one item meets selected query, show items
    if ($NoI) {
      $i = 1; // Help variable used to display items order

      echo("<p class='center'>V databáze bolo nájdených <strong>$NoI</strong> záznamov vyhovujúcich Vašim podmienkam!</p>
<table class='search-results'>\n");

      while($items = mysql_fetch_array($r)) {
        $sql = "SELECT `name` FROM `groups_1` WHERE `id`='$items[grp1]'";
        $r2 = mysql_query($sql);
        $groups1 = mysql_fetch_array($r2);
        $sk1 = $groups1["name"]; // Select name of group 1 where item belongs

        $sql = "SELECT `name` FROM `groups_2` WHERE `id`='$items[grp2]'";
        $r2 = mysql_query($sql);
        $groups2 = mysql_fetch_array($r2);
        $sk2 = $groups2["name"]; // Select name of group 2 where item belongs

        $sql = "SELECT `name` FROM `groups_3` WHERE `id`='$items[grp3]'";
        $r2 = mysql_query($sql);
        $groups3 = mysql_fetch_array($r2);
        $sk3 = $groups3["name"]; // Select name of group 3 where item belongs

        $sql = "SELECT `name` FROM `groups_4` WHERE `id`='$items[grp4]'";
        $r2 = mysql_query($sql);
        $groups4 = mysql_fetch_array($r2);
        $sk4 = $groups4["name"]; // Select name of group 4 where item belongs

        $nazov = $items["name"]; // Set item name
        $ToMenu = $sk4 . " " . $items["name"];
        $ToURL = ClearURL($ToMenu);
        $ItemLink = "index.php?item=$items[id]&amp;mod_name=$ToURL"; // Set link to item

        // Start displaying items and display item Nr.
        echo("<tr");
        if ($i % 2) echo(" class='darker-row'");
/*
        The modulo operator (%) divides the value of ($i) by 2 and returns the remainder, so the result is always 0 or 1.
        PHP treats 0 as false, and 1 as true, so when the result is 1, class="darker-row" is inserted in the <tr> tag.
*/
        echo(">
  <td class='right'>$i</td>\n");

        // Display random item image
        if (isset($hladat_obrazky) && $items["img_name"]) {
          $IFCH = substr($items["img_name"], 0, 1); // Get Image name First Character
          $RI = rand(1, $items["num_of_pics"]); // Randomly set image
          $Img_File = "images/mods/" . $IFCH . "/thumbnails/" . $items["img_name"] . "_" . $RI . ".jpg"; // Set image file name
          $Img_Alt = $sk4 . " " . $items["name"] . " - náhľ. obr. č. " . $RI; // Set image alternative text
          echo("<td>
  <a href='$ItemLink'>
    <img src='$Img_File' alt='$Img_Alt'></a>
</td>\n");
        }
        else echo("<td>&nbsp;</td>\n"); // Or display nothing if item image is missing

        // Display group 4 name (where item belongs) and item name
        echo("<td>
  <a href='$ItemLink' title='$sk1 / $sk2 / $sk3'>
    $ToMenu</a>
</td>\n");

        // Display item rating and/or autor
        if ($items["rating"] || $items["author"]) {
          echo("<td class='center'>");
          if ($items["rating"]) echo("[$items[rating]/10]");
          if ($items["rating"] && $items["author"]) echo(" ");
          if ($items["author"]) echo(htmlspecialchars($items["author"]));
          echo("</td>\n");
        }

        echo("</tr>\n");
        $i++;
      }
      echo("</table>\n");
    }
    else echo("<p class='attention center'>V databáze nie je žiadny záznam vyhovujúci Vašim podmienkam!</p>\n");

    echo ("<p class='right'><a href='index.php?action=search'>Hladať znovu</a> &gt;&gt;</p>\n");
  }
?>