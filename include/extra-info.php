<?php
/*
  (C) 2008 - 2011 Mário Herák - GUNSOFT
  Author: Mário Herák (herakm@gmail.com)
  Last update: 06.02.2011
*/

  if (isset($_POST["vote"])) $btn_vote = $_POST["vote"]; // Get status of button "Hlasovat"

  echo("<table cellpadding='0' cellspacing='0'>\n");

  // Display what selected model replace in game
  if ($items["model_id"]) {
    $sql = "SELECT * FROM `models` WHERE `id`='$items[model_id]'";
    $r2 = mysql_query($sql);
    $models = mysql_fetch_array($r2);
    $Model_Name = $models["name"] . " (" . $models["model_nr"] . ")";
    echo("<tr><th>Nahrádza:</th></tr>\n");
    if ($models["img"]) {
      $Img_Filename = "images/models/" . $models["img"] . ".jpg";
      $Img_Filename_Thumb = "images/models/thumbnails/" . $models["img"] . ".jpg";
      $Img_Size_Thumb = getimagesize("images/models/thumbnails/" . $models["img"] . ".jpg"); // Get thumbnail size
      echo("<tr>
  <td class='center'>
    <a rel='lightbox' href='$Img_Filename' title='$Model_Name'>
      <img src='$Img_Filename_Thumb' alt='$Model_Name'></a>
  </td>
</tr>\n");
    }
    echo("<tr><td class='center'>$Model_Name</td></tr>\n");
  }

  // Display mod pluses
  if ($items["plus"]) {
    $Mod_Pluses = nl2br($items["plus"]);
    echo("<tr><th>Plusy:</th></tr>
<tr>
  <td>\n$Mod_Pluses\n</td>
</tr>\n");
  }

  // Display mod minuses
  if ($items["minus"]) {
    $Mod_Minuses = nl2br($items["minus"]);
    echo("<tr><th>Mínusy:</th></tr>
<tr>
  <td>\n$Mod_Minuses\n</td>
</tr>\n");
  }

  // Display mod result
  if ($items["result"]) {
    $Mod_Result = nl2br($items["result"]);
    echo("<tr><th>Záver:</th></tr>
<tr>
  <td class='justify'>\n$Mod_Result\n</td>
</tr>\n");
  }

  // Display mod rating
  if ($items["rating"]) echo("<tr><th>Hodnotenie:</th></tr>
<tr>
  <td class='center'><strong>$items[rating]</strong>/10</td>
</tr>\n");

  // Users rating
  $MUR = $items["users_rating"]; // Get mod's users rating from database
  // If user has pressed "Hlasovat" button:
  if (isset($btn_vote)) {
    $UR = $_POST["user-rating"]; // Get user rating
    $z = $UR + $MUR; // Set help variable $z as sum of user rating and users rating
    if ($MUR) $result = $z / 2; // If mod was rated in database, divide user rating plus users rating by 2
    else $result = $UR; // If mod wasn't rated yet, result is user rating
    $result = round($result); // Round result: 0-4 down, 5-9 up
    // Update users rating of mod
    $sql = "UPDATE `items` SET `users_rating`='$result' WHERE `id`='$item'";
    $res = mysql_query($sql);
    $MUR = $result; // Set users rating of mod as result
  }
  echo ("<tr><th>Vaše hodnotenie:</th></tr>
<tr>
  <td class='center'>
    <form name='hlasovanie' method='post' action='index.php?item=$item");
  if (isset($mod_name)) echo("&amp;mod_name=$mod_name");
  echo("'>
<select name='user-rating' size='1'>
  <option value=''");
  if (!$MUR) echo(" selected='selected'");
  echo(">- - - - -</option>\n");
  for($i = 1; $i < 11; $i++) {
    echo("<option value='$i'");
    if (isset($MUR) && $MUR == $i) echo(" selected='selected'"); // Select users rating
    echo (">$i/10</option>\n");
  }
  echo ("</select>
      <input type='submit' name='vote' value='Hlasovať'>
    </form>
  </td>
</tr>\n");

  // Display autor name of mod
  if ($items["author"]) echo("<tr><th>Autor:</th></tr>
<tr>
  <td class='center'>$items[author]</td>
</tr>\n");

  // Display file to download of mod
  if ($items["file_name"]) {
    $MFS = floor(@filesize("download/" . $items["file_name"]) / 1024); // Get mod's filename size in kB
    echo ("<tr><th>Súbor na stiahnutie:</th></tr>
<tr>
  <td class='center'>\n");
    $alt_text = $items["file_name"] . " [" . $MFS . " kB], stiahnuté " . $items["times_dwnl"] . " krát";
    echo ("<a href='include/download.php?file=" . $items["file_name"] . "' target='_blank'>
    <img src='images/main/disketa.gif' width='26' height='32' alt='$items[file_name]' title='$alt_text'></a>
  </td>
</tr>\n");
  }

  // Display links of mod
  if ($items["links"]) {
    $Mod_Links = nl2br($items["links"]);
    echo ("<tr><th>Linky:</th></tr>
<tr>
  <td class='links'>\n$Mod_Links\n</td>
</tr>\n");
  }

  echo ("</table>\n");
?>